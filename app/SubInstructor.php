<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubInstructor extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sub_instructor_id', 'instructor_id'];

    // protected $appends = ['primary_id'];

    protected $hidden = ['id'];

    // public function getPrimaryIdAttribute() {

    //     return $this->id;
    // }
 
 	public function instructorDetails() {

 		return $this->belongsTo(Instructor::class,'sub_instructor_id');
 	} 

 	public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['status'] = APPROVED;

            $model->attributes['unique_id'] = uniqid();
            
        });

        static::created(function($model) {});

    } 
}
