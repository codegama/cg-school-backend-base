<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomUser extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['instructor_id', 'user_id', 'room_id'];

    protected $appends = ['room_user_id'];

    protected $hidden = ['id'];

    public function getRoomUserIdAttribute() {

        return $this->id;
    }

    public function userDetails() {

    	return $this->belongsTo(User::class,'user_id');
    }
}
