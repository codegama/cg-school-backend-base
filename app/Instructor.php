<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Setting;

class Instructor extends Model
{
    protected $fillable = ['email', 'password'];

    protected $hidden = ['id'];

    protected $appends = ['instructor_id', 'instructor_unique_id'];

    public function getInstructorIdAttribute() {

        return $this->id;
    }

    public function getInstructorUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function instructorRooms() {

    	return $this->hasMany(Room::class,'instructor_id');
    }

    public function subInstructors() {

    	return $this->hasMany(SubInstructor::class,'sub_instructor_id');
    }

    public function subInstructorDetails() {

        return $this->hasMany(SubInstructor::class,'instructor_id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProfileResponse($query) {

        return $query->select('instructors.*');
    
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSubInstructorResponse($query) {

        return $query->select(
                'instructors.id',
                'instructors.unique_id',
                'instructors.unique_id as sub_instructor_unique_id', 
                'instructors.name', 
                'instructors.first_name', 
                'instructors.last_name', 
                'instructors.email', 
                'instructors.mobile', 
                'instructors.picture', 
                'instructors.id as sub_instructor_id'
            );
    
    }
      
    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['first_name'] = $model->attributes['last_name'] = $model->attributes['name'];

            $model->attributes['is_verified'] = USER_EMAIL_VERIFIED;

            if (Setting::get('is_account_email_verification') == YES && envfile('MAIL_USERNAME') && envfile('MAIL_PASSWORD')) { 

                if($model->attributes['login_by'] == 'manual') {

                    $model->generateEmailCode();

                }

            }

            $model->attributes['payment_mode'] = COD;

            $model->attributes['unique_id'] = uniqid();

            $model->attributes['token'] = \Helper::generate_token();

            $model->attributes['token_expiry'] = \Helper::generate_token_expiry();

            $model->attributes['status'] = INSTRUCTOR_APPROVED;

            if(in_array($model->attributes['login_by'], ['facebook', 'google', 'apple', 'linkedin', 'instagram'] )) {
                
                $model->attributes['password'] = \Hash::make($model->attributes['social_unique_id']);
            }

        });

        static::created(function($model) {

            $model->attributes['email_notification_status'] = $model->attributes['push_notification_status'] = YES;

            $model->attributes['username'] = $model->attributes['unique_id'] = "INS"."-".$model->attributes['id'].routefreestring($model->attributes['name']);

            $model->save();
        
        });

        static::updating(function($model) {

            $model->attributes['username'] = $model->attributes['unique_id'] = "INS"."-".$model->attributes['id'].routefreestring($model->attributes['name']);

            $model->attributes['first_name'] = $model->attributes['last_name'] = $model->attributes['name'];

        });

        static::deleting(function ($model){

            $model->instructorRooms()->delete();

            $model->subInstructors()->delete();

        });

    }
}
