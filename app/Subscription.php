<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{ 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'plan', 'amount', 'no_of_class', 'no_of_users_each_class'];

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['subscription_id', 'subscription_unique_id', 'plan_formatted', 'subscription_amount_formatted', 'no_of_class_formatted', 'no_of_users_each_class_formatted', 'currency'];

    public function getSubscriptionIdAttribute() {

        return $this->id;
    }

    public function getSubscriptionUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getPlanFormattedAttribute() {

        return formatted_plan($this->plan, $this->plan_type);
    }

    public function getSubscriptionAmountFormattedAttribute() {

        return formatted_amount($this->amount);
    }

    public function getNoOfClassFormattedAttribute() {

        return no_of_class_formatted($this->no_of_class);
    }

    public function getNoOfUsersEachClassFormattedAttribute() {

        return no_of_users_each_class_formatted($this->no_of_users_each_class);
    }

    public function getCurrencyAttribute() {

        return \Setting::get('currency' , '$');
    }

    public function subscriptionPayments() {

    	return $this->hasMany(SubscriptionPayment::class,'subscription_id');
    }

    /**
     * Scope a query to only include approved records.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        return $query->where('subscriptions.status', APPROVED);
    
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "SI-"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "SI-"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

        static::deleting(function ($model){

           $model->subscriptionPayments()->delete();

        });
    }
}
