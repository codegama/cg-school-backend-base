<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstructorCard extends Model
{
    protected $fillable = ['customer_id', 'instructor_id'];

    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['instructor_card_id', 'instructor_card_unique_id'];

    public function getInstructorCardIdAttribute() {

        return $this->id;
    }

    public function getInstructorCardUniqueIdAttribute() {

        return $this->unique_id;
    }
}
