<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'instructor_id'];

    protected $appends = ['room_id', 'room_unique_id'];

    protected $hidden = ['id'];

    public function getRoomIdAttribute() {

        return $this->id;
    }

    public function getRoomUniqueIdAttribute() {

        return $this->id;
    }

    public function instructorDetails() {

    	return $this->belongsTo(Instructor::class, 'instructor_id');
    }

    public function roomUserDetails() {

    	return $this->hasMany(RoomUser::class, 'room_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['status'] = APPROVED;

            $model->attributes['unique_id'] = "RID"."-".uniqid();
            
        });

        static::created(function($model) {

        	$title = routefreestring($model->attributes['title']);

            $model->attributes['unique_id'] = "RID"."-".$model->attributes['id']."-".$title;

            $model->save();
        });

    } 
}
