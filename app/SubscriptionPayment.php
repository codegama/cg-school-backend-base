<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPayment extends Model
{
    protected $hidden = ['id', 'unique_id'];

    protected $appends = ['subscription_payment_id', 'plan_formatted', 'subscription_amount_formatted', 'no_of_class_formatted', 'no_of_users_each_class_formatted', 'currency', 'status_formatted'];

    public function getSubscriptionPaymentIdAttribute() {

        return $this->id;
    }

    public function getPlanFormattedAttribute() {

        return formatted_plan($this->plan, $this->plan_type);
    }

    public function getSubscriptionAmountFormattedAttribute() {

        return formatted_amount($this->amount);
    }

    public function getNoOfClassFormattedAttribute() {

        return no_of_class_formatted($this->no_of_class);
    }

    public function getNoOfUsersEachClassFormattedAttribute() {

        return no_of_users_each_class_formatted($this->no_of_users_each_class);
    }

    public function getCurrencyAttribute() {

        return \Setting::get('currency' , '$');
    }

    public function getStatusFormattedAttribute() {

        return $this->status == PAID ? tr('paid') : tr('unpaid');
    }

    /**
     * Get the subscription details 
     */
	public function subscriptionDetails() {

		return $this->belongsTo(Subscription::class,'subscription_id');

	}

	/**
     * Get the user details 
     */
	public function instructorDetails() {

		return $this->belongsTo(Instructor::class,'instructor_id');

	}

		/**
     * Scope a query to basic subscription details
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeBaseResponse($query) {

    	return $query;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "SP-"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "SP-"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
