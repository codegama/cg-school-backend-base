<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    //
    public function roomDetails(){

    	return $this->belongsTo(Room::class,'room_id');
    }

    public function instructorDetails(){

    	return $this->belongsTo(Instructor::class,'instructor_id');
    }

}
