<?php

namespace App\Http\Controllers\InstructorApi;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\Instructor;

use App\Jobs\SendEmailJob;

use App\Repositories\PaymentRepository as PaymentRepo;

class AccountApiController extends Controller
{
 	protected $loginUser;

    protected $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = Instructor::ProfileResponse()->find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method register()
     *
     * @uses Registered user can register through manual or social login
     * 
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param Form data
     *
     * @return Json response with user details
     */
    public function register(Request $request) {

        try {

            DB::beginTransaction();

            $rules = 
                [
                    'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS.','.DEVICE_WEB,
                    'device_token' => 'required',
                    'login_by' => 'required|in:manual,facebook,google,apple,linkedin,instagram',
                ];

            Helper::custom_validator($request->all(), $rules);

            $allowed_social_logins = ['facebook','google','apple', 'linkedin', 'instagram'];

            if(in_array($request->login_by, $allowed_social_logins)) {

                // validate social registration fields

                $rules = [
                    'social_unique_id' => 'required',
                    'name' => 'required|max:255|min:2',
                    'email' => 'required|email|max:255',
                    'mobile' => 'digits_between:6,13',
                    'picture' => '',
                    'gender' => 'in:male,female,others',
                ];

                Helper::custom_validator($request->all(), $rules);

            } else {

                $rules = [
                        'name' => 'required|max:255',
                        'email' => 'required|email|max:255|min:2',
                        'password' => 'required|min:6',
                        'picture' => 'mimes:jpeg,jpg,bmp,png',
                    ];

                Helper::custom_validator($request->all(), $rules);

                // validate email existence

                $rules = ['email' => 'unique:instructors,email'];

                Helper::custom_validator($request->all(), $rules);

            }

            $instructor_details = Instructor::where('email' , $request->email)->first();

            $send_email = NO;

            // Creating the user

            if(!$instructor_details) {

                $instructor_details = new Instructor;

                register_mobile($request->device_type);

                $send_email = YES;

                $instructor_details->picture = asset('placeholder.jpeg');

                $instructor_details->registration_steps = 1;

            } else {

                if(in_array($instructor_details->status, [INSTRUCTOR_PENDING , INSTRUCTOR_DECLINED])) {

                    throw new Exception(api_error(1000), 1000);
                
                }

            }

            $instructor_details->name = $request->name ?? "";

            $instructor_details->email = $request->email ?? "";

            $instructor_details->mobile = $request->mobile ?? "";

            if($request->has('password')) {

                $instructor_details->password = Hash::make($request->password ?: "123456");

            }

            $instructor_details->gender = $request->has('gender') ? $request->gender : "male";

            $check_device_exist = Instructor::where('device_token', $request->device_token)->first();

            if($check_device_exist) {

                $check_device_exist->device_token = "";

                $check_device_exist->save();
            }

            $instructor_details->device_token = $request->device_token ?: "";

            $instructor_details->device_type = $request->device_type ?: DEVICE_WEB;

            $instructor_details->login_by = $request->login_by ?: 'manual';

            $instructor_details->social_unique_id = $request->social_unique_id ?: '';

            // Upload picture

            if($request->login_by == 'manual') {

                if($request->hasFile('picture')) {

                    $instructor_details->picture = Helper::storage_upload_file($request->file('picture') , PROFILE_PATH_INSTRUCTOR);

                }

            } else {

                $instructor_details->picture = $request->picture ?: $instructor_details->picture;

            }   

            if($instructor_details->save()) {

                $instructor_details->save();

                // Send welcome email to the new user:

                if($send_email) {

                    if($instructor_details->login_by == 'manual') {

                        $instructor_details->password = $request->password;
                        
                        $email_data['subject'] = tr('welcome_title').' '.Setting::get('site_name');

                        $email_data['page'] = "emails.instructors.welcome";

                        $email_data['data'] = $instructor_details;

                        $email_data['email'] = $instructor_details->email;

                        $this->dispatch(new SendEmailJob($email_data));

                    }

                }

                if(in_array($instructor_details->status , [INSTRUCTOR_DECLINED , INSTRUCTOR_PENDING])) {
                
                    $response = ['success' => false , 'error' => api_error(1000) , 'error_code' => 1000];

                    DB::commit();

                    return response()->json($response, 200);
               
                }

                if($instructor_details->is_verified == INSTRUCTOR_EMAIL_VERIFIED) {

                	$data['instructor_details'] = Instructor::ProfileResponse()->find($instructor_details->id);

                    $response = ['success' => true, 'data' => $data];

                } else {

                    $response = ['success' => false, 'error' => api_error(1001), 'error_code'=>1001];

                    DB::commit();

                    return response()->json($response, 200);

                }

            } else {

                throw new Exception(api_error(103), 103);

            }

            DB::commit();

            return response()->json($response, 200);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method login()
     *
     * @uses Registered user can login using their email & password
     * 
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Email & Password
     *
     * @return Json response with user details
     */
    public function login(Request $request) {

        try {

            DB::beginTransaction();

            $basic_validator = Validator::make($request->all(),
                [
                    'device_token' => 'required',
                    'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS.','.DEVICE_WEB,
                    'login_by' => 'required|in:manual,facebook,google,apple,linkedin,instagram',
                ]
            );

            if($basic_validator->fails()){

                $error = implode(',', $basic_validator->messages()->all());

                throw new Exception($error , 101);

            }

            /** Validate manual login fields */

            $manual_validator = Validator::make($request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required',
                ]
            );

            if($manual_validator->fails()) {

                $error = implode(',', $manual_validator->messages()->all());

            	throw new Exception($error , 101);

            }

            $instructor_details = Instructor::where('email', '=', $request->email)->first();

            $is_email_verified = YES;

            // Check the user details 

            if(!$instructor_details) {

            	throw new Exception(api_error(1002), 1002);

            }

            // check the user approved status

            if($instructor_details->status != USER_APPROVED) {

            	throw new Exception(api_error(1000), 1000);

            }

            if(Setting::get('is_account_email_verification') == YES && !$instructor_details->is_verified) {

                Helper::check_email_verification("" , $instructor_details->id, $error);

                $is_email_verified = NO;

            }

            if(!$is_email_verified) {

    			throw new Exception(api_error(1001), 1001);
            }

            if(Hash::check($request->password, $instructor_details->password)) {

                // Generate new tokens
                
                $instructor_details->token = Helper::generate_token();

                $instructor_details->token_expiry = Helper::generate_token_expiry();
                
                // Save device details

                $check_device_exist = Instructor::where('device_token', $request->device_token)->first();

                if($check_device_exist) {

                    $check_device_exist->device_token = "";
                    
                    $check_device_exist->save();
                }

                $instructor_details->device_token = $request->device_token ?? $instructor_details->device_token;

                $instructor_details->device_type = $request->device_type ?? $instructor_details->device_type;

                $instructor_details->login_by = $request->login_by ?? $instructor_details->login_by;

                $instructor_details->save();

                $data['instructor_details'] = Instructor::ProfileResponse()->find($instructor_details->id);
				
				DB::commit();

            	return $this->sendResponse(api_success(101), 101, $data);

            } else {

				throw new Exception(api_error(102), 102);

            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method forgot_password()
     *
     * @uses If the user forgot his/her password he can hange it over here
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Email id
     *
     * @return send mail to the valid user
     */
    
    public function forgot_password(Request $request) {

        try {

            DB::beginTransaction();

            // Check email configuration and email notification enabled by admin

            if(Setting::get('is_email_notification') != YES ) {

                throw new Exception(api_error(106), 106);
                
            }
            
            $rules = ['email' => 'required|email|exists:instructors,email']; 

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $instructor_details = Instructor::where('email' , $request->email)->first();

            if(!$instructor_details) {

                throw new Exception(api_error(1002), 1002);
            }

            if($instructor_details->login_by != 'manual') {

                throw new Exception(api_error(118), 118);
                
            }

            // check email verification

            if($instructor_details->is_verified == INSTRUCTOR_EMAIL_NOT_VERIFIED) {

                throw new Exception(api_error(1001), 1001);
            }

            // Check the user approve status

            if(in_array($instructor_details->status , [INSTRUCTOR_DECLINED , INSTRUCTOR_PENDING])) {
                throw new Exception(api_error(1000), 1000);
            }

            $new_password = Helper::generate_password();

            $instructor_details->password = Hash::make($new_password);

            $email_data['subject'] = tr('instructor_forgot_email_title' , Setting::get('site_name'));

            $email_data['email']  = $instructor_details->email;

            $email_data['password'] = $new_password;

            $email_data['page'] = "emails.instructors.forgot-password";

            $this->dispatch(new SendEmailJob($email_data));

            if(!$instructor_details->save()) {

                throw new Exception(api_error(103));

            }

            DB::commit();

            return $this->sendResponse(api_success(102), $success_code = 102, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    }

    /**
     * @method change_password()
     *
     * @uses To change the password of the user
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Password & confirm Password
     *
     * @return json response of the user
     */
    public function change_password(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required|min:6',
            ]; 

            Helper::custom_validator($request->all(), $rules, $custom_errors =[]);

            $instructor_details = Instructor::find($request->id);

            if(!$instructor_details) {

                throw new Exception(api_error(1002), 1002);
            }

            if($instructor_details->login_by != "manual") {

                throw new Exception(api_error(118), 118);
                
            }

            if(Hash::check($request->old_password,$instructor_details->password)) {

                $instructor_details->password = Hash::make($request->password);
                
                if($instructor_details->save()) {

                    DB::commit();

                    $email_data['subject'] = tr('change_password_email_title' , Setting::get('site_name'));

                    $email_data['email']  = $instructor_details->email;

                    $email_data['page'] = "emails.instructors.change-password";

                    $email_data['subject'] = tr('instructor_change_password_title' , Setting::get('site_name'));

                    $this->dispatch(new SendEmailJob($email_data));

                    return $this->sendResponse(api_success(104), $success_code = 104, $data = []);
                
                } else {

                    throw new Exception(api_error(103), 103);   
                }

            } else {

                throw new Exception(api_error(108) , 108);
            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /** 
     * @method profile()
     *
     * @uses To display the user details based on user  id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - User Id
     *
     * @return json response with user details
     */

    public function profile(Request $request) {

        try {

            $instructor_details = Instructor::where('id' , $request->id)->ProfileResponse()->first();

            if(!$instructor_details) { 

                throw new Exception(api_error(1002) , 1002);
            }

            $data['instructor_details'] = $instructor_details;

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }
 
    /**
     * @method update_profile()
     *
     * @uses To update the user details
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param objecct $request : User details
     *
     * @return json response with user details
     */
    public function update_profile(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
            		'name' => 'required|max:255',
                    'email' => 'email|unique:instructors,email,'.$request->id.'|max:255',
                    'mobile' => 'digits_between:6,13',
                    'picture' => 'nullable|mimes:jpeg,bmp,png',
                    'gender' => 'nullable|in:male,female,others',
                    'device_token' => '',
            ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end
            
            $instructor_details = Instructor::find($request->id);

            if(!$instructor_details) { 

                throw new Exception(api_error(1002) , 1002);
            }

            $instructor_details->name = $request->name ?? $instructor_details->name;
            
            if($request->has('email')) {

                $instructor_details->email = $request->email;
            }

            $instructor_details->mobile = $request->mobile ?: $instructor_details->mobile;

            $instructor_details->gender = $request->gender ?: $instructor_details->gender;

            $instructor_details->address = $request->address ?: $instructor_details->address;

            // Upload picture
            if($request->hasFile('picture') != "") {

                Helper::storage_delete_file($instructor_details->picture, PROFILE_PATH_INSTRUCTOR); // Delete the old pic

                $instructor_details->picture = Helper::storage_upload_file($request->file('picture') , PROFILE_PATH_INSTRUCTOR);

            }

            if($instructor_details->save()) {

                $data['instructor_details'] = Instructor::ProfileResponse()->find($instructor_details->id);

                DB::commit();

                return $this->sendResponse($message = api_success(111), $success_code = 111, $data);

            } else {    

        		throw new Exception(api_error(103), 103);
            }

        } catch (Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method delete_account()
     * 
     * @uses Delete user account based on user id
     *
     * @created Vithya R 
     *
     * @updated Vithya R
     *
     * @param object $request - Password and user id
     *
     * @return json with boolean output
     */

    public function delete_account(Request $request) {

        try {

            DB::beginTransaction();

            $request->request->add([ 
                'login_by' => $this->loginUser ? $this->loginUser->login_by : "manual",
            ]);

            // Validation start

            $rules = ['password' => 'required_if:login_by,manual'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $instructor_details = Instructor::find($request->id);

            if(!$instructor_details) {

            	throw new Exception(api_error(1002), 1002);
                
            }

            // The password is not required when the user is login from social. If manual means the password is required

            if($instructor_details->login_by == 'manual') {

                if(!Hash::check($request->password, $instructor_details->password)) {
         
                    throw new Exception(api_error(104), 104); 
                }
            
            }

            if($instructor_details->delete()) {

                DB::commit();

                return $this->sendResponse(api_success(103), $success_code = 103, $data = []);

            } else {

            	throw new Exception(api_error(119), 119);
            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

	}

    /**
     * @method logout()
     *
     * @uses Logout the user
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param 
     * 
     * @return
     */
    public function logout(Request $request) {

        return $this->sendResponse(api_success(106), 106);

    }

    /**
     * @method cards_list()
     *
     * @uses get the user payment mode and cards list
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return
     */

    public function cards_list(Request $request) {

        try {

            $instructor_cards = \App\InstructorCard::where('instructor_id' , $request->id)->get();

            $card_payment_mode = $payment_modes = [];

            $card_payment_mode['name'] = "Card";

            $card_payment_mode['payment_mode'] = "card";

            $card_payment_mode['is_default'] = 1;

            array_push($payment_modes , $card_payment_mode);

            $data['payment_modes'] = $payment_modes;   

            $data['cards'] = $instructor_cards ? $instructor_cards : []; 

            return $this->sendResponse($message = "", $success_code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }
    
    /**
     * @method cards_add()
     *
     * @uses used to add card to the user
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param card_token
     * 
     * @return JSON Response
     */
    public function cards_add(Request $request) {

        try {

            if(Setting::get('stripe_secret_key')) {

                \Stripe\Stripe::setApiKey(Setting::get('stripe_secret_key'));

            } else {

                throw new Exception(api_error(121), 121);

            }

            // Validation start

            $rules = ['card_token' => 'required'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end
            
            $instructor_details = Instructor::find($request->id);

            if(!$instructor_details) {

                throw new Exception(api_error(1002), 1002);
                
            }

            DB::beginTransaction();

            // Get the key from settings table
            
            $customer = \Stripe\Customer::create([
                    "card" => $request->card_token,
                    "email" => $instructor_details->email,
                    "description" => "Customer for ".Setting::get('site_name'),
                ]);

            if($customer) {

                $customer_id = $customer->id;

                $card_details = new \App\InstructorCard;

                $card_details->instructor_id = $request->id;

                $card_details->customer_id = $customer_id;

                $card_details->card_token = $customer->sources->data ? $customer->sources->data[0]->id : "";

                $card_details->card_type = $customer->sources->data ? $customer->sources->data[0]->brand : "";

                $card_details->last_four = $customer->sources->data[0]->last4 ? $customer->sources->data[0]->last4 : "";

                $card_details->card_holder_name = $request->card_holder_name ?: $this->loginUser->name;

                // Check is any default is available

                $check_card_details = \App\InstructorCard::where('instructor_id',$request->id)->count();

                $card_details->is_default = $check_card_details ? NO : YES;

                if($card_details->save()) {

                    if($instructor_details) {

                        $instructor_details->card_id = $check_card_details ? $instructor_details->card_id : $card_details->id;

                        $instructor_details->save();
                    }

                    $data = \App\InstructorCard::where('id' , $card_details->id)->first();

                    DB::commit();

                    return $this->sendResponse(api_success(105), 105, $data);

                } else {

                    throw new Exception(api_error(114), 114);
                    
                }
           
            } else {

                throw new Exception(api_error(121) , 121);
                
            }

        } catch(Stripe_CardError | Stripe_InvalidRequestError | Stripe_AuthenticationError | Stripe_ApiConnectionError | Stripe_Error $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode() ?: 101);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode() ?: 101);
        }

    }

    /**
     * @method cards_delete()
     *
     * @uses delete the selected card
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer card_id
     * 
     * @return JSON Response
     */

    public function cards_delete(Request $request) {

        try {

            DB::beginTransaction();

            // validation start

            $rules = [
                    'card_id' => 'required|integer|exists:instructor_cards,id,instructor_id,'.$request->id,
                    ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // validation end

            $instructor_details = Instructor::find($request->id);

            if(!$instructor_details) {

                throw new Exception(api_error(1002), 1002);
            }

            \App\InstructorCard::where('id', $request->card_id)->delete();

            if($instructor_details->payment_mode = CARD) {

                // Check he added any other card

                if($check_card = \App\InstructorCard::where('instructor_id' , $request->id)->first()) {

                    $check_card->is_default =  DEFAULT_TRUE;

                    $instructor_details->card_id = $check_card->id;

                    $check_card->save();

                } else { 

                    $instructor_details->payment_mode = COD;

                    $instructor_details->card_id = DEFAULT_FALSE;
                
                }
           
            }

            // Check the deleting card and default card are same

            if($instructor_details->card_id == $request->card_id) {

                $instructor_details->card_id = DEFAULT_FALSE;

                $instructor_details->save();
            }
            
            $instructor_details->save();
                
            DB::commit();

            return $this->sendResponse(api_success(109), 109, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method cards_default()
     *
     * @uses update the selected card as default
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return JSON Response
     */
    public function cards_default(Request $request) {

        try {

            DB::beginTransaction();

            // validation start

            $rules = [
                    'card_id' => 'required|integer|exists:instructor_cards,id,instructor_id,'.$request->id,
                    ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // validation end

            $instructor_details = Instructor::find($request->id);

            if(!$instructor_details) {

                throw new Exception(api_error(1002), 1002);
            }
        
            $old_default_cards = \App\InstructorCard::where('instructor_id' , $request->id)->where('is_default', YES)->update(['is_default' => NO]);

            $instructor_cards = \App\InstructorCard::where('id' , $request->card_id)->update(['is_default' => YES]);

            $instructor_details->card_id = $request->card_id;

            $instructor_details->save();

            DB::commit();

            return $this->sendResponse(api_success(108), 108);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }
    
    } 

    /**
     * @method payment_mode_default()
     *
     * @uses update the selected card as default
     *
     * @created Vithya R
     *
     * @updated Vithya R
     *
     * @param integer id
     * 
     * @return JSON Response
     */
    public function payment_mode_default(Request $request) {

        Log::info("payment_mode_default");

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), [

                'payment_mode' => 'required',

            ]);

            if($validator->fails()) {

                $error = implode(',',$validator->messages()->all());

                throw new Exception($error, 101);

            }

            $instructor_details = Instructor::find($request->id);

            $instructor_details->payment_mode = $request->payment_mode ?: CARD;

            $instructor_details->save();           

            DB::commit();

            return $this->sendResponse($message = "Mode updated", $code = 200, $data = ['payment_mode' => $request->payment_mode]);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    } 

    /**
     * @method subscriptions_index()
     *
     * @uses To display all the subscription plans
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function subscriptions_index(Request $request) {

        try {

            $subscriptions = \App\Subscription::Approved()->orderBy('amount', 'asc')->get();

            $data['subscriptions'] = $subscriptions ?? [];

            $data['total'] = $subscriptions->count() ?? 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method subscriptions_view()
     *
     * @uses get the selected subscription details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param integer $subscription_id
     *
     * @return JSON Response
     */
    public function subscriptions_view(Request $request) {

        try {

            $rules = ['subscription_id' => 'required|exists:subscriptions,id',$request->subscription_id];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            $subscription_details = Subscription::BaseResponse()->where('subscriptions.status' , APPROVED)->where('subscriptions.id', $request->subscription_id)->first();

            if(!$subscription_details) {
                throw new Exception(api_error(135), 135);   
            }

            return $this->sendResponse($message = '' , $code = '', $subscription_details);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method subscriptions_history()
     *
     * @uses get the selected subscription details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param integer $subscription_id
     *
     * @return JSON Response
     */
    public function subscriptions_history(Request $request) {

        try {

            $base_query = $subscription_query = \App\SubscriptionPayment::BaseResponse()->where('instructor_id' , $request->id);

            $subscription_payments = $subscription_query->skip($this->skip)->take($this->take)->orderBy('subscription_payments.id', 'desc')->get();

            foreach ($subscription_payments as $key => $value) {

                $value->expiry_date = common_date($value->expiry_date, $this->timezone ?? '', 'd M Y');
            
            }

            $data['subscription_payments'] = $subscription_payments;

            $data['total'] = $base_query->count();

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method subscriptions_current_plan()
     *
     * @uses get the selected subscription details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param integer $subscription_id
     *
     * @return JSON Response
     */
    public function subscriptions_current_plan(Request $request) {

        try {

            $subscription_payment_details = \App\SubscriptionPayment::BaseResponse()->where('instructor_id' , $request->id)->where('is_current_subscription', YES)->first();

            if(!$subscription_payment_details) {

                throw new Exception(api_error(134), 134);
                
            }

            $subscription_payment_details->expiry_date = common_date($subscription_payment_details->expiry_date, $this->timezone ?? '', 'd M Y');
            
            return $this->sendResponse($message = '' , $code = '', $subscription_payment_details);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /** 
     * @method subscriptions_payment_by_stripe()
     *
     * @uses pay for subscription using stripe
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function subscriptions_payment_by_stripe(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['subscription_id' => 'required|exists:subscriptions,id'];

            $custom_errors = ['subscription_id' => api_error(151)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);
            
            // Validation end

           // Check the subscription is available

            $subscription_details = \App\Subscription::where('id',  $request->subscription_id)->Approved()->first();

            if(!$subscription_details) {

                throw new Exception(api_error(161), 161);
                
            }

            $request->request->add(['payment_mode' => CARD]);

            $total = $user_pay_amount = $subscription_details->amount ?? 0.00;

            $request->request->add([
                'total' => $total, 
                'user_pay_amount' => $user_pay_amount,
                'paid_amount' => $user_pay_amount,
            ]);

            if($user_pay_amount > 0) {

                // Check the user have the cards

                $card_details = \App\InstructorCard::where('instructor_id', $request->id)->where('is_default', YES)->first();

                // If the user doesn't have cards means the payment will switch to COD

                if(!$card_details) {

                    throw new Exception(api_error(133), 133); 

                }

                $request->request->add(['customer_id' => $card_details->customer_id]);
                
                $card_payment_response = PaymentRepo::subscriptions_payment_by_stripe($request, $subscription_details)->getData();

                if($card_payment_response->success == false) {

                    throw new Exception($card_payment_response->error, $card_payment_response->error_code);
                    
                }

                $card_payment_data = $card_payment_response->data;

                $request->request->add(['paid_amount' => $card_payment_data->paid_amount, 'payment_id' => $card_payment_data->payment_id, 'paid_status' => $card_payment_data->paid_status]);

            }

            $payment_response = PaymentRepo::subscriptions_payment_save($request, $subscription_details)->getData();

            if($payment_response->success) {
                
                DB::commit();

                return $this->sendResponse(api_success(123), 123, $payment_response->data);

            } else {

                throw new Exception($payment_response->error, $payment_response->error_code);
                
            }
        
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

    /** 
     * @method subscriptions_payment_by_paypal()
     *
     * @uses pay for subscription using paypal
     *
     * @created Vidhya R
     *
     * @updated Vidhya R
     *
     * @param
     * 
     * @return JSON response
     *
     */

    public function subscriptions_payment_by_paypal(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'subscription_id' => 'required|exists:subscriptions,id',
                    'payment_id' => 'required',
                    ];

            $custom_errors = ['subscription_id' => api_error(151)];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);
            
            // Validation end

           // Check the subscription is available

            $subscription_details = Subscription::where('id',  $request->subscription_id)
                                    ->Approved()
                                    ->first();

            if(!$subscription_details) {

                throw new Exception(api_error(161), 161);
                
            }

            $request->request->add(['payment_mode' => PAYPAL]);

            $total = $user_pay_amount = $subscription_details->amount ?? 0.00;

            $request->request->add([
                'total' => $total, 
                'user_pay_amount' => $user_pay_amount,
                'paid_amount' => $user_pay_amount,
            ]);

            $payment_response = PaymentRepo::subscriptions_payment_save($request, $subscription_details)->getData();

            if($payment_response->success) {
                
                DB::commit();

                $code = 111;

                return $this->sendResponse(api_success($code), $code, $payment_response->data);

            } else {

                throw new Exception($payment_response->error, $payment_response->error_code);
                
            }
        
        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }

    }

}
