<?php

namespace App\Http\Controllers\InstructorApi;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\User;

class SubInstructorApiController extends Controller
{
    protected $loginUser;

    protected $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::CommonResponse()->find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method sub_instructors_index()
     *
     * @uses sub instructors list based on the logged in instructor
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function sub_instructors_index(Request $request) {

        try {

        	$base_query = $sub_instructor_query = \App\SubInstructor::where('instructor_id', $request->id);

        	$sub_instructor_ids = $sub_instructor_query->skip($this->skip)->take($this->take)->pluck('sub_instructor_id');

            $instructors = $sub_instructor_ids ? \App\Instructor::whereIn('instructors.id', $sub_instructor_ids)->SubInstructorResponse()->get() : [];

            $data['instructors'] = $instructors;

            $data['total'] = $base_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method sub_instructors_view()
     *
     * @uses sub instructors view based on the logged in instructor
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function sub_instructors_view(Request $request) {

        try {

            $data['instructor'] = \App\Instructor::where('instructors.id', $request->sub_instructor_id)->SubInstructorResponse()->first();

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method sub_instructors_add()
     *
     * @uses sub instructors view based on the logged in instructor
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function sub_instructors_add(Request $request) {

        try {

        	DB::beginTransaction();

        	// Validation start

            $rules = ['sub_instructor_id' => 'required|exists:instructors,id'];

            $custom_errors = ['sub_instructor_id' => api_error(129)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            if($request->sub_instructor_id == $request->id) {
                throw new Exception(api_error(135), 135);
            }

            $sub_instructor = \App\SubInstructor::updateOrCreate(['sub_instructor_id' => $request->sub_instructor_id, 'instructor_id' => $request->id], $request->all());

        	$sub_instructor->save();

           	DB::commit();

           	$data['sub_instructor'] = $sub_instructor;

            return $this->sendResponse($message = api_success(117) , $code = 117, $data);

        } catch(Exception $e) {

        	DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method sub_instructors_remove()
     *
     * @uses sub instructors remove
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function sub_instructors_remove(Request $request) {

        try {

        	DB::beginTransaction();

        	// Validation start

            $rules = ['sub_instructor_id' => 'required|exists:instructors,id'];

            $custom_errors = ['sub_instructor_id' => api_error(129)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $sub_instructor = \App\SubInstructor::where('sub_instructor_id', $request->sub_instructor_id)->where('instructor_id', $request->id)->delete();

            //  @todo releated details

           	DB::commit();

            return $this->sendResponse($message = api_success(118) , $code = 118, $data = []);

        } catch(Exception $e) {

        	DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method sub_instructors_search()
     *
     * @uses sub instructors search
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function sub_instructors_search(Request $request) {

        try {

        	// Validation start

            $rules = ['search_key' => 'required|min:2'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $search_key = $request->search_key;

            $sub_instructor_query = $base_query = \App\Instructor::where('instructors.id', '!=', $request->id)->SubInstructorResponse()
            				->leftJoin('sub_instructors', 'sub_instructors.sub_instructor_id', '=', 'instructors.id')
                            ->where(function($query) use($search_key) {

                                $query->orWhere('name','LIKE','%'.$search_key.'%');
                                
                                $query->orWhere('email','LIKE','%'.$search_key.'%');
                                
                                $query->orWhere('mobile','LIKE','%'.$search_key.'%');
                            });

		    $sub_instructors = $sub_instructor_query->skip($this->skip)
		                    ->take($this->take)
		                    ->get();

		    $data['sub_instructors'] = $sub_instructors;

		    $data['total'] = $base_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

}
