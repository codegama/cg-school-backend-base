<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User, App\Instructor;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

class ApplicationController extends Controller
{
    /**
     * @method static_pages_api()
     *
     * @uses used to get the pages
     *
     * @created Vidhya R 
     *
     * @edited Vidhya R
     *
     * @param - 
     *
     * @return JSON Response
     */

    public function static_pages_api(Request $request) {

        if($request->page_type) {

            $static_page = \App\StaticPage::where('type' , $request->page_type)
                                ->where('status' , APPROVED)
                                ->CommonResponse()
                                ->first();

            $data = $static_page;

        } else {

            $static_pages = \App\StaticPage::where('status' , APPROVED)
                                ->CommonResponse()
                                ->orderBy('title', 'asc')
                                ->get();

            $data = $static_pages ? $static_pages->toArray(): [];

        }

        return $this->sendResponse($message = "", $code = "", $data);

    }
    /**
     * @method email_verify()
     *
     * @uses To verify the email from user and provider.  
     *
     * @created Bhawya
     *
     * @updated Bhawya
     *
     * @param -
     *
     * @return JSON RESPONSE
     */

    public function email_verify(Request $request) {

        if($request->user_id) {

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_error',tr('user_details_not_found'));
            } 

            if($user_details->is_verified == USER_EMAIL_VERIFIED) {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_success' ,tr('verify_success'));
            }

            $response = Helper::check_email_verification($request->verification_code , $user_details->id, $error, USER);
            
            if($response) {

                $user_details->is_verified = USER_EMAIL_VERIFIED;       

                $user_details->save();

            } else {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_error' , $error);
            }

        } else {

            $instructor_details = Instructor::find($request->instructor_id);

            if(!$instructor_details) {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_error' , tr('instructor_details_not_found'));
            }

            if($instructor_details->is_verified) {
                
                return redirect()->away(Setting::get('frontend_url'))->with('flash_success' ,tr('verify_success'));
            }

            $response = Helper::check_email_verification($request->verification_code , $instructor_details->id, $error, INSTRUCTOR);

            if($response) {

                $instructor_details->is_verified = INSTRUCTOR_EMAIL_VERIFIED;
                
                $instructor_details->save();

            } else {

                return redirect()->away(Setting::get('frontend_url'))->with('flash_error' , $error);
            }

        }

        return redirect()->away(Setting::get('frontend_url'));
    
    }
}
