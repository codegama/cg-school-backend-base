<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

use App\Admin, App\User;

use App\Settings, App\StaticPage;

use App\Jobs\SendEmailJob;

use Carbon\Carbon;

use App\Instructor, App\Subscription, App\SubscriptionPayment, App\Room, App\Meeting, App\SubInstructor,App\RoomUser,App\MeetingUser;

class AdminController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {

        $this->middleware('auth:admin');

        $this->skip = $request->skip ?: 0;
       
        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

    }

    /**
     * @method index()
     *
     * @uses Show the application dashboard.
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function index() {
        
        $data = new \stdClass;

        $data->total_users = User::count();

        $data->total_instructors = Instructor::count();

        $data->total_revenue = SubscriptionPayment::where('status', PAID)->sum('amount');
        
        $data->today_revenue = SubscriptionPayment::whereDate('paid_date',today())->sum('amount');

        $today_subscriptions  = SubscriptionPayment::whereDate('paid_date', today())->where('status', PAID)->take(5)->get();

        $data->on_live_meetings = Meeting::where('status',APPROVED)->orderBy('id' , 'desc')->where('meeting_type',MEETING_TYPE_LIVE)->skip($this->skip)->take($this->take)->get();
        
        $data->today_scheduled_meetings = Meeting::where('status',APPROVED)->orderBy('id' , 'desc')->whereDate('schedule_time',today())->skip($this->skip)->take($this->take)->get();

        $data->total_meetings = Meeting::where('status',APPROVED)->count();
        
        $data->scheduled_meetings = Meeting::where('status',APPROVED)->whereDate('schedule_time',today())->count();

        $data->todays_meeting = Meeting::where('status',APPROVED)->whereDate('created_at',today())->count();

        $data->completed_meetings = Meeting::where('status',COMPLETED)->count();

        $data->analytics = last_6_months_data();

        return view('admin.dashboard')
                    ->with('page' , 'dashboard')
                    ->with('data', $data)
                    ->with('today_subscriptions',$today_subscriptions);
    
    }

     /**
     * @method users_index()
     *
     * @uses To list out users details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function users_index(Request $request) {

        $base_query = User::orderBy('created_at','desc');

        if($request->search_key) {

            $base_query = $base_query
                    ->orWhere('name','LIKE','%'.$request->search_key.'%')
                    ->orWhere('email','LIKE','%'.$request->search_key.'%')
                    ->orWhere('mobile','LIKE','%'.$request->search_key.'%');
        }

        if($request->status) {

            switch ($request->status) {

                case SORT_BY_APPROVED:
                    $base_query = $base_query->where('status',APPROVED);
                    break;

                case SORT_BY_DECLINED:
                    $base_query = $base_query->where('status',DECLINED);
                    break;

                case SORT_BY_EMAIL_VERIFIED:
                    $base_query = $base_query->where('is_verified',USER_EMAIL_VERIFIED);
                    break;
                
                default:
                    $base_query = $base_query->where('is_verified',USER_EMAIL_NOT_VERIFIED);
                    break;
            }
        }

        $users = $base_query->paginate(10);

        return view('admin.users.index')
                    ->with('main_page','users-crud')
                    ->with('page','users')
                    ->with('sub_page' , 'users-view')
                    ->with('users' , $users);
    }

    /**
     * @method users_create()
     *
     * @uses To create user details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function users_create() {

        $user_details = new User;

        return view('admin.users.create')
                    ->with('main_page','users-crud')
                    ->with('page' , 'users')
                    ->with('sub_page','users-create')
                    ->with('user_details', $user_details);           
    }

    /**
     * @method users_edit()
     *
     * @uses To display and update user details based on the user id
     *
     * @created Anjana
     *
     * @updated Anjana
     *
     * @param object $request - User Id
     * 
     * @return redirect view page 
     *
     */
    public function users_edit(Request $request) {

        try {

            $user_details = User::find($request->user_id);

            if(!$user_details) { 

                throw new Exception(tr('user_not_found'), 101);
            }

            return view('admin.users.edit')
                    ->with('main_page','users-crud')
                    ->with('page' , 'users')
                    ->with('sub_page','users-view')
                    ->with('user_details' , $user_details); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method users_save()
     *
     * @uses To save the users details of new/existing user object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - User Form Data
     *
     * @return success message
     *
     */
    public function users_save(Request $request) {

        try {

            DB::begintransaction();

            $rules = [
                'name' => 'required|max:191|unique:users,name',
                'email' => $request->user_id ? 'required|email|max:191|unique:users,email,'.$request->user_id.',id' : 'required|email|max:191|unique:users,email,NULL,id',
                'password' => $request->user_id ? "" : 'required|min:6',
                'mobile' => $request->mobile ? 'digits_between:6,13' : '',
                'picture' => 'mimes:jpg,png,jpeg',
                'user_id' => 'exists:users,id'
            ];

            Helper::custom_validator($request->all(),$rules);

            $user_details = $request->user_id ? User::find($request->user_id) : new User;

            $is_new_user = NO;

            if($user_details->id) {

                $message = tr('user_updated_success'); 

            } else {

                $is_new_user = YES;

                $user_details->password = ($request->password) ? \Hash::make($request->password) : null;

                $message = tr('user_created_success');

                $user_details->email_verified_at = date('Y-m-d H:i:s');

                $user_details->picture = asset('placeholder.jpeg');

                $user_details->is_verified = USER_EMAIL_VERIFIED;

            }

            $user_details->name = $request->name ?: $user_details->name;

            $user_details->email = $request->email ?: $user_details->email;

            $user_details->mobile = $request->mobile ?: '';

            $user_details->login_by = $request->login_by ?: 'manual';

            // Upload picture
            
            if($request->hasFile('picture')) {

                if($request->user_id) {

                    Helper::storage_delete_file($user_details->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $user_details->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

            if($user_details->save()) {

                if($is_new_user == YES) {

                    /**
                     * @todo Welcome mail notification
                     */

                    $user_details->is_verified = USER_EMAIL_VERIFIED;

                    $user_details->save();

                }

                DB::commit(); 

                return redirect(route('admin.users.view', ['user_id' => $user_details->id]))->with('flash_success', $message);

            } 

            throw new Exception(tr('user_save_failed'));
            
        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        } 

    }

    /**
     * @method users_view()
     *
     * @uses view the users details based on users id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - User Id
     * 
     * @return View page
     *
     */
    public function users_view(Request $request) {
       
        try {
      
            $user_details = User::find($request->user_id);

            if(!$user_details) { 

                throw new Exception(tr('user_not_found'), 101);                
            }

            return view('admin.users.view')
                        ->with('main_page','users-crud')
                        ->with('page', 'users') 
                        ->with('sub_page','users-view') 
                        ->with('user_details' , $user_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method users_delete()
     *
     * @uses delete the user details based on user id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - User Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function users_delete(Request $request) {

        try {

            DB::begintransaction();

            $user_details = User::find($request->user_id);
            
            if(!$user_details) {

                throw new Exception(tr('user_not_found'), 101);                
            }

            if($user_details->delete()) {

                DB::commit();

                return redirect()->route('admin.users.index')->with('flash_success',tr('user_deleted_success'));   

            } 
            
            throw new Exception(tr('user_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method users_status
     *
     * @uses To update user status as DECLINED/APPROVED based on users id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - User Id
     * 
     * @return response success/failure message
     *
     **/
    public function users_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('user_not_found'), 101);
                
            }

            $user_details->status = $user_details->status ? DECLINED : APPROVED ;

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->status ? tr('user_approve_success') : tr('user_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method users_verify_status()
     *
     * @uses verify the user
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - User Id
     *
     * @return redirect back page with status of the user verification
     */
    public function users_verify_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('user_details_not_found'), 101);
                
            }

            $user_details->is_verified = $user_details->is_verified ? USER_EMAIL_NOT_VERIFIED : USER_EMAIL_VERIFIED;

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->is_verified ? tr('user_verify_success') : tr('user_unverify_success');

                return redirect()->route('admin.users.index')->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_verify_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method instructors_index()
     *
     * @uses To list out instructor details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function instructors_index(Request $request) {

        $base_query = Instructor::orderBy('created_at','desc');

        if($request->search_key) {

            $base_query = $base_query
                    ->orWhere('name','LIKE','%'.$request->search_key.'%')
                    ->orWhere('email','LIKE','%'.$request->search_key.'%')
                    ->orWhere('mobile','LIKE','%'.$request->search_key.'%');
        }

        if($request->status) {

            switch ($request->status) {

                case SORT_BY_APPROVED:
                    $base_query = $base_query->where('status',APPROVED);
                    break;

                case SORT_BY_DECLINED:
                    $base_query = $base_query->where('status',DECLINED);
                    break;

                case SORT_BY_EMAIL_VERIFIED:
                    $base_query = $base_query->where('is_verified',INSTRUCTOR_EMAIL_VERIFIED);
                    break;
                
                default:
                    $base_query = $base_query->where('is_verified',INSTRUCTOR_EMAIL_NOT_VERIFIED);
                    break;
            }
        }


        $instructors = $base_query->paginate(10);
 
        return view('admin.instructors.index')
                    ->with('main_page','instructors-crud')
                    ->with('page','instructors')
                    ->with('sub_page' , 'instructors-view')
                    ->with('instructors' , $instructors);
    }

    /**
     * @method instructors_create()
     *
     * @uses To create instructors details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function instructors_create() {

        $instructor_details = new Instructor;

        $subscriptions = Subscription::where('status',APPROVED)->get();

        return view('admin.instructors.create')
                    ->with('main_page','instructors-crud')
                    ->with('page' , 'instructors')
                    ->with('sub_page','instructors-create')
                    ->with('instructor_details', $instructor_details);           
    }

    /**
     * @method instructors_edit()
     *
     * @uses To display and update instructors details based on the instructor id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Instructor Id
     * 
     * @return redirect view page 
     *
     */
    public function instructors_edit(Request $request) {

        try {

            $instructor_details = Instructor::find($request->instructor_id);

            if(!$instructor_details) { 

                throw new Exception(tr('instructor_not_found'), 101);
            }

            return view('admin.instructors.edit')
                    ->with('main_page','instructors-crud')
                    ->with('page' , 'instructors')
                    ->with('sub_page','instructors-view')
                    ->with('instructor_details' , $instructor_details); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.instructors.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method instructors_save()
     *
     * @uses To save the instructors details of new/existing instructor object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - Instructor Form Data
     *
     * @return success message
     *
     */
    public function instructors_save(Request $request) {

        try {

            DB::begintransaction();
            
            $rules = [
                'name' => 'required|max:191',
                'email' => $request->instructor_id ? 'required|email|max:191|unique:instructors,email,'.$request->instructor_id.',id' : 'required|email|max:191|unique:instructors,email,NULL,id',
                'password' => $request->instructor_id ? "" : 'required|min:6',
                'mobile' => $request->mobile ? 'digits_between:6,13' : '',
                'picture' => 'mimes:jpg,png,jpeg',
                'instructor_id' => 'exists:instructors,id'
            ];

            Helper::custom_validator($request->all(),$rules);

            $instructor_details = $request->instructor_id ? Instructor::find($request->instructor_id) : new Instructor;

            $is_new_instructor = NO;

            if($instructor_details->id) {

                $message = tr('instructor_updated_success'); 

            } else {

                $is_new_instructor = YES;

                $instructor_details->password = ($request->password) ? \Hash::make($request->password) : null;

                $message = tr('instructor_created_success');

                $instructor_details->email_verified_at = date('Y-m-d H:i:s');

                $instructor_details->picture = asset('placeholder.jpeg');

                $instructor_details->is_verified = INSTRUCTOR_EMAIL_VERIFIED;

            }

            $instructor_details->name = $request->name ?: $instructor_details->name;

            $instructor_details->email = $request->email ?: $instructor_details->email;

            $instructor_details->mobile = $request->mobile ?: '';

            $instructor_details->login_by = $request->login_by ?: 'manual';

            $instructor_details->token = Helper::generate_token();

            $instructor_details->token_expiry = Helper::generate_token_expiry();

            // Upload picture
            
            if($request->hasFile('picture')) {

                if($request->instruction_id) {

                    Helper::storage_delete_file($instructor_details->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $instructor_details->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

            if($instructor_details->save()) {

                if($is_new_instructor == YES) {

                    /**
                     * @todo Welcome mail notification
                     */

                    $instructor_details->is_verified = USER_EMAIL_VERIFIED;

                    $instructor_details->save();

                }

                DB::commit(); 

                return redirect(route('admin.instructors.view', ['instructor_id' => $instructor_details->id]))->with('flash_success', $message);

            } 

            throw new Exception(tr('instructor_save_failed'));
            
        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        } 

    }

    /**
     * @method instructors_view()
     *
     * @uses view the instructors details based on instructors id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - Instructor Id
     * 
     * @return View page
     *
     */
    public function instructors_view(Request $request) {
       
        try {
           
            $instructor_details = Instructor::find($request->instructor_id);

            if(!$instructor_details) { 

                throw new Exception(tr('instructor_not_found'), 101);                
            }

            return view('admin.instructors.view')
                        ->with('main_page','instructors-crud')
                        ->with('page', 'instructors') 
                        ->with('sub_page','instructors-view') 
                        ->with('instructor_details' , $instructor_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method instructors_delete()
     *
     * @uses delete the instructor details based on instructor id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - Instructor Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function instructors_delete(Request $request) {

        try {

            DB::begintransaction();

            $instructor_details = Instructor::find($request->instructor_id);
            
            if(!$instructor_details) {

                throw new Exception(tr('instructor_not_found'), 101);                
            }

            if($instructor_details->delete()) {

                DB::commit();

                return redirect()->route('admin.instructors.index')->with('flash_success',tr('instructor_deleted_success'));   

            } 
            
            throw new Exception(tr('instructor_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method instructors_status
     *
     * @uses To update instructor status as DECLINED/APPROVED based on instructors id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Instructor Id
     * 
     * @return response success/failure message
     *
     **/
    public function instructors_status(Request $request) {

        try {

            DB::beginTransaction();

            $instructor_details = Instructor::find($request->instructor_id);

            if(!$instructor_details) {

                throw new Exception(tr('instructor_not_found'), 101);
                
            }

            $instructor_details->status = $instructor_details->status ? DECLINED : APPROVED ;

            if($instructor_details->save()) {

                DB::commit();

                $message = $instructor_details->status ? tr('instructor_approve_success') : tr('instructor_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('instructor_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.instructors.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method instructors_verify_status()
     *
     * @uses verify the instructor
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - Instructor Id
     *
     * @return redirect back page with status of the instructor verification
     */
    public function instructors_verify_status(Request $request) {

        try {

            DB::beginTransaction();

            $instructor_details = Instructor::find($request->instructor_id);

            if(!$instructor_details) {

                throw new Exception(tr('instructor_details_not_found'), 101);
                
            }

            $instructor_details->is_verified = $instructor_details->is_verified ? INSTRUCTOR_EMAIL_NOT_VERIFIED : INSTRUCTOR_EMAIL_VERIFIED;

            if($instructor_details->save()) {

                DB::commit();

                $message = $instructor_details->is_verified ? tr('instructor_verify_success') : tr('instructor_unverify_success');

                return redirect()->route('admin.instructors.index')->with('flash_success', $message);
            }
            
            throw new Exception(tr('instructor_verify_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.instructors.index')->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method sub_instructors_create()
     *
     * @uses Display the view for sub instructor
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - Sub Instructor Id
     *
     * @return redirect back page with status of the instructor verification
     */
    public function sub_instructors_create(Request $request) {

        try{

            $instructor_details = Instructor::where('id',$request->instructor_id)->first();

            if(!$instructor_details) {

                throw new Exception(tr('instructor_not_found'), 101);
            }

            $base_query = Instructor::where('status',APPROVED)->where('id','!=',$request->instructor_id);

            $current_sub_instructors = SubInstructor::where('instructor_id',$request->instructor_id)->get();

            if($current_sub_instructors) {

                foreach ($current_sub_instructors as $key => $current_sub_instructor_details) {
                     
                    $base_query = $base_query->where('id','!=',$current_sub_instructor_details->sub_instructor_id);

                }

            } 

            $instructors = $base_query->get();
           
            $sub_instructors = SubInstructor::where('instructor_id',$request->instructor_id)->paginate(5);

            $sub_instructor_count = SubInstructor::where('instructor_id',$request->instructor_id)->count();
            
            return view('admin.instructors.sub_instructors.create')
                    ->with('main_page','instructors-crud')
                    ->with('page','instructors')
                    ->with('sub_page','instructors-view')
                    ->with('instructor_details',$instructor_details)
                    ->with('instructors',$instructors)
                    ->with('sub_instructors',$sub_instructors)
                    ->with('search_key',$request->instructor_id)
                    ->with('sub_instructor_count',$sub_instructor_count);


        } catch(Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method sub_instructors_save()
     *
     * @uses which is used to save the Sub Instructors
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - Sub Instructor Id
     *
     * @return redirect back page with status of the instructor verification
     */
    public function sub_instructors_save(Request $request) {

        try {

            DB::begintransaction();
            
            $rules = [
                'sub_instructor_ids' => 'required',
            ];

            Helper::custom_validator($request->all(),$rules);


            foreach ($request->sub_instructor_ids as $sub_instructor_id)
            {

                $check_sub_instructor = SubInstructor::where('instructor_id',$request->instructor_id)->where('sub_instructor_id',$sub_instructor_id)->first();

                if($check_sub_instructor) {

                    throw new Exception(tr('sub_instructor_already_present'), 1);
                }

                $sub_instructor_details = new SubInstructor;
                
                $sub_instructor_details->sub_instructor_id = $sub_instructor_id;

                $sub_instructor_details->instructor_id = $request->instructor_id;

                $sub_instructor_details->save();

                DB::commit();
            }

            return redirect()->back()->with('flash_success',tr('instructor_assigned_successfully'));

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error',$e->getMessage());
        }
    
    }

    /**
     * @method sub_instructors_delete()
     *
     * @uses Used to display the sub instructors based on id
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - Sub Instructor Id
     *
     * @return redirect back page with status of the instructor verification
     */
    public function sub_instructors_delete(Request $request) {

        try {

            DB::begintransaction();

            $sub_instructor_details = SubInstructor::find($request->sub_instructor_id);
            
            if(!$sub_instructor_details) {

                throw new Exception(tr('sub_instructor_not_found'), 101);                
            }

            if($sub_instructor_details->delete()) {

                DB::commit();

                return redirect()->back()->with('flash_success',tr('sub_instructor_deleted_success'));   

            } 
            
            throw new Exception(tr('sub_instructor_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }     
    
    }

    /**
     * @method instructors_subscriptions_index()
     *
     * @uses Used to display all subscription available for all users
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - subscripiton id
     *
     * @return redirect back page with status of the instructor verification
     */
    public function instructors_subscriptions_index(Request $request) 
    {
        $subscriptions = Subscription::where('status',APPROVED)->get();

        $instructor_details = Instructor::where('id',$request->instructor_id)->first();

        $subscripton_payment_history = SubscriptionPayment::where('instructor_id',$request->instructor_id)->paginate(10);
       
        return view('admin.instructors.subscriptions.index')
                    ->with('main_page','instructors-crud')
                    ->with('page','instructors')
                    ->with('sub_page','instructors-view')
                    ->with('subscriptions',$subscriptions)
                    ->with('instructor_details',$instructor_details)
                    ->with('subscripton_payment_history',$subscripton_payment_history);
    }

    /**
     * @method instructors_subscriptions_payments()
     *
     * @uses Used to subscribe perticulor subscription 
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - subscripiton id
     *
     * @return redirect back page with status of the instructor verification
     */
    public function instructors_subscriptions_payments(Request $request) 
    {
        try {
           
            DB::begintransaction();
            
            $rules = [
                'subscription_id' =>'required',
                'instructor_id' => 'required'
            ];

            Helper::custom_validator($request->all(),$rules);

            $subscription_details = Subscription::where('id',$request->subscription_id)->first();

            $check_subscription = SubscriptionPayment::where('subscription_id',$subscription_details->id)->where('instructor_id',$request->instructor_id)->first();

            if($check_subscription) {

                throw new Exception(tr('you_are_already_member_of_this_subscription'), 101);
                
            }

            $subscription_payment_details = new SubscriptionPayment;

            $subscription_payment_details->subscription_id = $subscription_details->id;

            $subscription_payment_details->instructor_id = $request->instructor_id;

            $subscription_payment_details->payment_id = uniqid();

            $subscription_payment_details->amount = $subscription_details->amount;

            $subscription_payment_details->is_current_subscription = YES;

            $subscription_payment_details->paid_date = date('Y-m-d H:i:s');

            $subscription_payment_details->expiry_date = getExpiryDate($subscription_details->plan,$subscription_details->plan_type);

            $subscription_payment_details->no_of_class = $subscription_details->no_of_class;

            $subscription_payment_details->no_of_users_each_class = $subscription_details->no_of_users_each_class;

            $subscription_payment_details->plan = $subscription_details->plan;

            $subscription_payment_details->plan_type = $subscription_details->plan_type;

            $subscription_payment_details->cancel_reason = '';

            if($subscription_payment_details->save()) {

                DB::commit();

                return redirect()->back()->with('flash_success',tr('subscription_payment_success'));
                
            }

            throw new Exception(tr('subscription_payment_failed'), 101);

        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        } 
    }

    /**
     * @method instructors_subscriptions_payments_delete()
     *
     * @uses Used to delete the payment record based on payment Id
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - subsctiption payment Id
     *
     * @return 
     */
    public function instructors_subscriptions_payments_delete(Request $request) {

        try {

            DB::begintransaction();

            $subscripton_payment_details = SubscriptionPayment::find($request->subscription_payment_id);
            
            if(!$subscripton_payment_details) {

                throw new Exception(tr('subscription_payment_details_not_found'), 101);                
            }

            if($subscripton_payment_details->delete()) {

                DB::commit();

                return redirect()->back()->with('flash_success',tr('subscription_payment_deleted_success'));   

            } 
            
            throw new Exception(tr('subscription_payment_failed_to_delete'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }     
    
    }

    /**
     * @method rooms_index()
     *
     * @uses To list out rooms details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function rooms_index(Request $request) {

        $base_query = Room::orderBy('rooms.created_at','DESC');

        $main_page = 'rooms-crud';

        $sub_page = 'rooms-view';

        $page = 'rooms';

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query = $base_query
            
                    ->orWhereHas('instructorDetails', function($q) use ($search_key) {

                        return $q->Where('instructors.name','LIKE','%'.$search_key.'%');

                    })->orWhereHas('instructorDetails', function($q) use ($search_key) {

                        return $q->Where('instructors.mobile','LIKE','%'.$search_key.'%');

                    })->orWhere('rooms.title','LIKE','%'.$search_key.'%');
        }

        if($request->instructor_id) {

            $base_query = $base_query->where('instructor_id',$request->instructor_id);

            $main_page = 'instructors-crud';

            $sub_page = 'instructors-view';

            $page = 'instructors';
        }

        $rooms = $base_query->paginate(10);

        return view('admin.rooms.index')
                    ->with('main_page',$main_page)
                    ->with('page',$page)
                    ->with('sub_page' ,$sub_page)
                    ->with('rooms' , $rooms);
    }

    /**
     * @method rooms_create()
     *
     * @uses To create room details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function rooms_create() {

        $room_details = new Room;

        $instructors = Instructor::Where('status',APPROVED)->get();

        return view('admin.rooms.create')
                    ->with('main_page','rooms-crud')
                    ->with('page' , 'rooms')
                    ->with('sub_page','rooms-create')
                    ->with('room_details', $room_details)
                    ->with('instructors',$instructors);           
    }

    /**
     * @method rooms_edit()
     *
     * @uses To display and update rooms details based on the room id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Room Id
     * 
     * @return redirect view page 
     *
     */
    public function rooms_edit(Request $request) {

        try {

            $room_details = Room::find($request->room_id);

            $instructors = Instructor::where('status',APPROVED)->get();

            foreach ($instructors as $key => $instructor_details) {

                $instructor_details->is_selected = NO;
   
                if($room_details->instructor_id == $instructor_details->id) {
  
                    $instructor_details->is_selected = YES;
                }
            }

            if(!$room_details) { 

                throw new Exception(tr('room_not_found'), 101);
            }

            return view('admin.rooms.edit')
                    ->with('main_page','rooms-crud')
                    ->with('page' , 'rooms')
                    ->with('sub_page','rooms-view')
                    ->with('room_details' , $room_details)
                    ->with('instructors',$instructors); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.rooms.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method rooms_save()
     *
     * @uses To save the rooms details of new/existing room object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - room Form Data
     *
     * @return success message
     *
     */
    public function rooms_save(Request $request) {

        try {

            DB::begintransaction();

            $rules = [
                'title'  => 'required|max:255',
                'description' => 'max:255',
                'picture' => 'mimes:jpg,png,jpeg',
                'instructor_id' => 'required|exists:instructors,id',
            
            ];

            Helper::custom_validator($request->all(),$rules);

            $room_details = $request->room_id ? Room::find($request->room_id) : new Room;

            if(!$room_details) {

                throw new Exception(tr('room_not_found'), 101);
            }

            $room_details->status = APPROVED;

            $room_details->title = $request->title;

            $room_details->instructor_id = $request->instructor_id;

            $room_details->unique_id =  uniqid();

            $room_details->description = $request->description ?: "";

            if($request->hasFile('picture')) {

                if($request->room_id) {

                    Helper::storage_delete_file($room_details->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $room_details->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

           
            if( $room_details->save() ) {

                DB::commit();

                $message = $request->room_id ? tr('room_update_success')  : tr('room_create_success');

                return redirect()->route('admin.rooms.view', ['room_id' => $room_details->id])->with('flash_success', $message);
            } 

            throw new Exception(tr('room_saved_error') , 101);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        } 

    }

    /**
     * @method rooms_view()
     *
     * @uses view the rooms details based on rooms id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - Room Id
     * 
     * @return View page
     *
     */
    public function rooms_view(Request $request) {
       
        try {
      
            $room_details = Room::find($request->room_id);
            
            if(!$room_details) { 

                throw new Exception(tr('room_not_found'), 101);                
            }

            return view('admin.rooms.view')
                        ->with('main_page','rooms-crud')
                        ->with('page', 'rooms') 
                        ->with('sub_page','rooms-view') 
                        ->with('room_details' , $room_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method rooms_delete()
     *
     * @uses delete the rooms details based on room id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - Room Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function rooms_delete(Request $request) {

        try {

            DB::begintransaction();

            $room_details = Room::find($request->room_id);
            
            if(!$room_details) {

                throw new Exception(tr('room_not_found'), 101);                
            }

            if($room_details->delete()) {

                DB::commit();

                return redirect()->route('admin.rooms.index')->with('flash_success',tr('room_deleted_success'));   

            } 
            
            throw new Exception(tr('room_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method room_status
     *
     * @uses To update room status as DECLINED/APPROVED based on room id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Room Id
     * 
     * @return response success/failure message
     *
     **/
    public function rooms_status(Request $request) {

        try {

            DB::beginTransaction();

            $room_details = Room::find($request->room_id);

            if(!$room_details) {

                throw new Exception(tr('room_not_found'), 101);
                
            }

            $room_details->status = $room_details->status ? DECLINED : APPROVED ;

            if($room_details->save()) {

                DB::commit();

                $message = $room_details->status ? tr('room_approve_success') : tr('room_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('room_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.rooms.index')->with('flash_error', $e->getMessage());

        }

    }

     /**
     * @method rooms_user_create()
     *
     * @uses display the view page for creating the room users
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - Room Id
     *
     * @return 
     */
    public function rooms_user_create(Request $request) {

    try {

            $room_details = Room::where('id',$request->room_id)->first();

            if(!$room_details) {

                throw new Exception(tr('room_not_found'), 101);
            }
        
            $room_users = RoomUser::where('room_id',$request->room_id)->paginate(8);

            $base_query = User::where('status',APPROVED);

            $current_room_users = RoomUser::where('room_id',$request->room_id)->get();

            if($current_room_users) {

                foreach ($current_room_users as $key => $current_room_user_details) {
                     
                    $base_query = $base_query->where('id','!=',$current_room_user_details->user_id);

                }

            } 

            $users = $base_query->get();
            
            return view('admin.rooms.user.create')
                    ->with('main_page','rooms-crud')
                    ->with('page','rooms')
                    ->with('sub_page','rooms-view')
                    ->with('room_details',$room_details)
                    ->with('room_users',$room_users)
                    ->with('users',$users)
                    ->with('search_key',$request->room_id);


        } catch(Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());

        }
    
    }


    /**
     * @method rooms_user_save()
     *
     * @uses used to store the room users details to database
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - Room Id
     *
     * @return 
     */
    public function rooms_user_save(Request $request) {

        try {

            DB::begintransaction();
            
            $rules = [
                'user_ids' => 'required',
                'instructor_id' => 'required',
            ];

            Helper::custom_validator($request->all(),$rules);

            foreach ($request->user_ids as $user_id)
            {

                $check_room_user = RoomUser::where('room_id',$request->room_id)->where('user_id',$user_id)->first();

                if($check_room_user) {

                    throw new Exception(tr('sub_instructor_already_present'), 1);
                }

                $room_user_details = new RoomUser;
                
                $room_user_details->user_id = $user_id;

                $room_user_details->instructor_id = $request->instructor_id;

                $room_user_details->room_id = $request->room_id;

                $room_user_details->save();

                DB::commit();
            }

            return redirect()->back()->with('flash_success',tr('room_user_assigned_successfully'));

        } catch(Exception $e) {

            return redirect()->back()->with('flash_error',$e->getMessage());
        }
    
    }

     /**
     * @method rooms_user_delete()
     *
     * @uses Used to display the room user based on id
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - Room User Id
     *
     * @return 
     */
    public function rooms_user_delete(Request $request) {

        try {

            DB::begintransaction();

            $room_user_details = RoomUser::find($request->rooms_user_id);
            
            if(!$room_user_details) {

                throw new Exception(tr('rooms_user_details_not_found'), 101);                
            }

            if($room_user_details->delete()) {

                DB::commit();

                return redirect()->back()->with('flash_success',tr('rooms_user_deleted_success'));   

            } 
            
            throw new Exception(tr('rooms_user_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }     
    
    }


    /**
     * @method meetings_index()
     *
     * @uses To list out meetings details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function meetings_index(Request $request) {

        $base_query = Meeting::orderBy('meetings.id','desc');

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query =  $base_query

                    ->orWhereHas('instructorDetails', function($q) use ($search_key) {

                        return $q->Where('instructors.name','LIKE','%'.$search_key.'%');

                    })->orWhereHas('instructorDetails', function($q) use ($search_key) {

                        return $q->Where('instructors.mobile','LIKE','%'.$search_key.'%');

                    })->orWhere('meetings.title','LIKE','%'.$search_key.'%');
                        
        }
         
        $meetings = $base_query->paginate(10);

        return view('admin.meetings.index')
                    ->with('main_page','meetings-crud')
                    ->with('page','meetings')
                    ->with('sub_page' , 'meetings-view')
                    ->with('meetings' , $meetings);
    }

    /**
     * @method meetings_create()
     *
     * @uses To create meetings details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function meetings_create() {

        $meeting_details = new Meeting;

        $rooms = Room::where('status',APPROVED)->get();

        $users = User::where('status',APPROVED)->get();

        $instructors = Instructor::where('status',APPROVED)->get();

        return view('admin.meetings.create')
                    ->with('main_page','meetings-crud')
                    ->with('page' , 'meetings')
                    ->with('sub_page','meetings-create')
                    ->with('meeting_details', $meeting_details)
                    ->with('rooms',$rooms)
                    ->with('instructors',$instructors)
                    ->with('users',$users);           
    }

    /**
     * @method meetings_edit()
     *
     * @uses To display and update meetings details based on the meeting id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Room Id
     * 
     * @return redirect view page 
     *
     */
    public function meetings_edit(Request $request) {

        try {

            $meeting_details = Meeting::find($request->meeting_id);

            $instructors = Instructor::where('status',APPROVED)->get();

            foreach ($instructors as $key => $instructor_details) {

                $instructor_details->is_selected = NO;

                if($meeting_details->instructor_id == $instructor_details->id){
                    
                    $instructor_details->is_selected = YES;
                }

            }

            $rooms = Room::where('status',APPROVED)->get();

            foreach ($rooms as $key => $room_details) {

                $room_details->is_selected = NO;
   
                if($meeting_details->room_id == $room_details->id) {
  
                    $room_details->is_selected = YES;
                }
            }

            if(!$meeting_details) { 

                throw new Exception(tr('meeting_not_found'), 101);
            }

            return view('admin.meetings.edit')
                    ->with('main_page','meetings-crud')
                    ->with('page' , 'meetings')
                    ->with('sub_page','meetings-view')
                    ->with('meeting_details' , $meeting_details)
                    ->with('rooms',$rooms)
                    ->with('instructors',$instructors); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.meetings.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method meetings_save()
     *
     * @uses To save the meetings details of new/existing meeting object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - meeting Form Data
     *
     * @return success message
     *
     */
    public function meetings_save(Request $request) {

        try {
           
            DB::begintransaction();

            $rules = [
                'title'  => 'required|max:255',
                'description' => 'max:255',
                'picture' => 'mimes:jpg,png,jpeg',
                'instructor_id' => 'required|exists:instructors,id',
                'room_id' => 'required|exists:rooms,id',
                'schedule_time' => 'required',
                'start_time' =>'required',
                'end_time' => 'required',
            
            ];

            Helper::custom_validator($request->all(),$rules);

            $meeting_details = $request->meeting_id ? Meeting::find($request->meeting_id) : new Meeting;

            if(!$meeting_details) {

                throw new Exception(tr('meeting_not_found'), 101);
            }

            $meeting_details->unique_id =  uniqid();

            $meeting_details->created_by = ADMIN;

            $meeting_details->status = APPROVED;

            $meeting_details->title = $request->title;

            $meeting_details->instructor_id = $request->instructor_id;

            $meeting_details->room_id = $request->room_id;

            $meeting_details->start_time = convertTimeToUTCzone(date('Y-m-d H:i:s', strtotime($request->start_time)),Auth::guard('admin')->user()->timezone);

            $meeting_details->end_time = convertTimeToUTCzone(date('Y-m-d H:i:s', strtotime($request->end_time)),Auth::guard('admin')->user()->timezone);

            $meeting_details->schedule_time = convertTimeToUTCzone(date('Y-m-d H:i:s', strtotime($request->schedule_time)),Auth::guard('admin')->user()->timezone);

            $meeting_details->description = $request->description ?: "";

            if($request->hasFile('picture')) {

                if($request->meeting_id) {

                    Helper::storage_delete_file($meeting_details->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $meeting_details->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

           
            if( $meeting_details->save() ) {

                $meeting_user_details = $request->meeting_user_id ? MeetingUser::find($request->meeting_user_id) : new MeetingUser;
                

                DB::commit();

                $message = $request->meeting_id ? tr('meeting_update_success')  : tr('meeting_create_success');

                return redirect()->route('admin.meetings.view', ['meeting_id' => $meeting_details->id])->with('flash_success', $message);
            } 

            throw new Exception(tr('meeting_saved_error') , 101);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        } 

    }

    /**
     * @method meetings_view()
     *
     * @uses view the meetings details based on meetings id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - meeting Id
     * 
     * @return View page
     *
     */
    public function meetings_view(Request $request) {
       
        try {
    
            $meeting_details = Meeting::find($request->meeting_id);
            
            if(!$meeting_details) { 

                throw new Exception(tr('meeting_not_found'), 101);                
            }

            return view('admin.meetings.view')
                        ->with('main_page','meetings-crud')
                        ->with('page', 'meetings') 
                        ->with('sub_page','meetings-view') 
                        ->with('meeting_details' , $meeting_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method meetings_delete()
     *
     * @uses delete the meetings details based on meeting id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - Meeting Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function meetings_delete(Request $request) {

        try {

            DB::begintransaction();

            $meeting_details = Meeting::find($request->meeting_id);
            
            if(!$meeting_details) {

                throw new Exception(tr('meeting_not_found'), 101);                
            }

            if($meeting_details->delete()) {

                DB::commit();

                return redirect()->route('admin.meetings.index')->with('flash_success',tr('meeting_deleted_success'));   

            } 
            
            throw new Exception(tr('meeting_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method meeting_status
     *
     * @uses To update meeting status as DECLINED/APPROVED based on meeting id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Meeting Id
     * 
     * @return response success/failure message
     *
     **/
    public function meetings_status(Request $request) {

        try {

            DB::beginTransaction();

            $meeting_details = Meeting::find($request->meeting_id);

            if(!$meeting_details) {

                throw new Exception(tr('meeting_not_found'), 101);
                
            }

            $meeting_details->status = $meeting_details->status ? DECLINED : APPROVED ;

            if($meeting_details->save()) {

                DB::commit();

                $message = $meeting_details->status ? tr('meeting_approve_success') : tr('meeting_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('meeting_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.meetings.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method subscriptions_index()
     *
     * @uses To list out subscription details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function subscriptions_index() {

        $subscriptions = Subscription::orderBy('updated_at','desc')->paginate(10);

        return view('admin.subscriptions.index')
                    ->with('main_page','subscriptions-crud')
                    ->with('page','subscriptions')
                    ->with('sub_page' , 'subscriptions-view')
                    ->with('subscriptions' , $subscriptions);
    }

    /**
     * @method subscriptions_create()
     *
     * @uses To create subscriptions details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function subscriptions_create() {

        $subscription_details = new Subscription;

        $subscription_plan_types = [PLAN_TYPE_MONTH,PLAN_TYPE_YEAR,PLAN_TYPE_WEEK,PLAN_TYPE_DAY];

        return view('admin.subscriptions.create')
                    ->with('main_page','subscriptions-crud')
                    ->with('page' , 'subscriptions')
                    ->with('sub_page','subscriptions-create')
                    ->with('subscription_details', $subscription_details)
                    ->with('subscription_plan_types',$subscription_plan_types);           
    }

    /**
     * @method subscriptions_edit()
     *
     * @uses To display and update subscriptions details based on the instructor id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Subscription Id
     * 
     * @return redirect view page 
     *
     */
    public function subscriptions_edit(Request $request) {

        try {

            $subscription_details = Subscription::find($request->subscription_id);

            if(!$subscription_details) { 

                throw new Exception(tr('subscrprion_not_found'), 101);
            }

            $subscription_plan_types = [PLAN_TYPE_MONTH,PLAN_TYPE_YEAR,PLAN_TYPE_WEEK,PLAN_TYPE_DAY];
           
            return view('admin.subscriptions.edit')
                    ->with('main_page','subscriptions-crud')
                    ->with('page' , 'subscriptions')
                    ->with('sub_page','subscriptions-view')
                    ->with('subscription_details' , $subscription_details)
                    ->with('subscription_plan_types',$subscription_plan_types); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.subscriptions.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method subscriptions_save()
     *
     * @uses To save the subscriptions details of new/existing subscription object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - Subscrition Form Data
     *
     * @return success message
     *
     */
    public function subscriptions_save(Request $request) {

        try {

            DB::begintransaction();

            $rules = [
                'title'  => 'required|max:255',
                'description' => 'max:255',
                'amount' => 'required|numeric|min:0|max:10000000',
                'no_of_class' => 'required|numeric|min:1',
                'no_of_users_each_class' => 'required|numeric|min:1',
                'plan' => 'required',
                'plan_type' => 'required',
            
            ];

            Helper::custom_validator($request->all(),$rules);

            $subscription_details = $request->subscription_id ? Subscription::find($request->subscription_id) : new Subscription;

            if(!$subscription_details) {

                throw new Exception(tr('subscription_not_found'), 101);
            }

            $subscription_details->status = APPROVED;

            $subscription_details->title = $request->title;

            $subscription_details->description = $request->description ?: "";

            $subscription_details->plan = $request->plan;

            $subscription_details->plan_type = $request->plan_type;

            $subscription_details->amount = $request->amount;

            $subscription_details->no_of_class = $request->no_of_class;

            $subscription_details->no_of_users_each_class = $request->no_of_users_each_class;

            $subscription_details->is_free = $request->is_free == YES ? YES :NO;
        
            $subscription_details->is_popular  = $request->is_popular == YES ? YES :NO;

            if( $subscription_details->save() ) {

                DB::commit();

                $message = $request->subscription_id ? tr('subscription_update_success')  : tr('subscription_create_success');

                return redirect()->route('admin.subscriptions.view', ['subscription_id' => $subscription_details->id])->with('flash_success', $message);
            } 

            throw new Exception(tr('subscription_saved_error') , 101);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        } 

    }

    /**
     * @method subscriptions_view()
     *
     * @uses view the subscriptions details based on subscriptions id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - Subscription Id
     * 
     * @return View page
     *
     */
    public function subscriptions_view(Request $request) {
       
        try {
      
            $subscription_details = Subscription::find($request->subscription_id);
            
            if(!$subscription_details) { 

                throw new Exception(tr('subscription_not_found'), 101);                
            }

            return view('admin.subscriptions.view')
                        ->with('main_page','subscriptions-crud')
                        ->with('page', 'subscriptions') 
                        ->with('sub_page','subscriptions-view') 
                        ->with('subscription_details' , $subscription_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method subscriptions_delete()
     *
     * @uses delete the subscription details based on subscription id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - Subscription Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function subscriptions_delete(Request $request) {

        try {

            DB::begintransaction();

            $subscription_details = Subscription::find($request->subscription_id);
            
            if(!$subscription_details) {

                throw new Exception(tr('subscription_not_found'), 101);                
            }

            if($subscription_details->delete()) {

                DB::commit();

                return redirect()->route('admin.subscriptions.index')->with('flash_success',tr('subscription_deleted_success'));   

            } 
            
            throw new Exception(tr('subscription_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method subscriptions_status
     *
     * @uses To update subscription status as DECLINED/APPROVED based on subscriptions id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Subscription Id
     * 
     * @return response success/failure message
     *
     **/
    public function subscriptions_status(Request $request) {

        try {

            DB::beginTransaction();

            $subscription_details = Subscription::find($request->subscription_id);

            if(!$subscription_details) {

                throw new Exception(tr('subscription_not_found'), 101);
                
            }

            $subscription_details->status = $subscription_details->status ? DECLINED : APPROVED ;

            if($subscription_details->save()) {

                DB::commit();

                $message = $subscription_details->status ? tr('subscription_approve_success') : tr('subscription_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('subscription_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.subscriptions.index')->with('flash_error', $e->getMessage());

        }

    }

     /**
     * @method subscription_payments_index()
     *
     * @uses To create subscriptions details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function subscription_payments_index(Request $request) {

        $base_query = SubscriptionPayment::orderBy('subscription_payments.id','desc');

        if($request->today_revenue) {

            $base_query = $base_query->whereDate('subscription_payments.created_at',today());
        }

        if ($request->subscription_id) {

            $base_query = $base_query->where('subscription_payments.subscription_id',$request->subscription_id);
            
        }
        
        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query =  $base_query
                            ->orWhereHas('instructorDetails', function($q) use ($search_key) {

                                return $q->where('instructors.name','LIKE','%'.$search_key.'%');

                            })->orWhereHas('subscriptionDetails', function($q) use ($search_key) {

                                return $q->where('subscriptions.title','LIKE','%'.$search_key.'%');
                            })
                            ->orWhere('subscription_payments.payment_id','LIKE','%'.$search_key.'%');

        }

        $subscription_payments = $base_query->paginate(10);

        return view('admin.payments.subscription_payments.index')
                    ->with('main_page','payments-crud')
                    ->with('page' , 'payments')
                    ->with('sub_page','subscription-payments')
                    ->with('subscription_payments', $subscription_payments);           
    }

    /**
     * @method subscription_payments_view()
     *
     * @uses display the secified subscription details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function subscription_payments_view(Request $request) {

        try {

            $subscription_payment_details = SubscriptionPayment::where('subscription_payments.id', $request->subscription_payment_id)->first();

            if(!$subscription_payment_details) { 

                throw new Exception(tr('subscription_payment_not_found'), 101);                
            }

            return view('admin.payments.subscription_payments.view')
                    ->with('main_page','payments-crud')
                    ->with('page' , 'payments')
                    ->with('sub_page','subscription-payments')
                    ->with('subscription_payment_details', $subscription_payment_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
                   
    }
    /**
     * @method revenue_dashboard()
     *
     * @uses Display revenue details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function revenue_dashboard() {

        $data['total_subscribers'] = SubscriptionPayment::count('instructor_id');
        
        $data['today_subscribers'] = SubscriptionPayment::whereDate('created_at',today())->count();

        $data['total_earnings'] = SubscriptionPayment::sum('amount');

        $data['today_earnings'] = SubscriptionPayment::whereDate('created_at',today())->sum('amount');

        $data = (object) $data;

        $data->analytics = last_x_days_revenue(7);

        return view('admin.payments.revenue_dashboard')
                ->with('main_page','payments-crud')
                ->with('page', 'payments')
                ->with('sub_page' ,'revenues-dashboard')
                ->with('data', $data);

    }

    

    /**
     * @method settings()
     *
     * @uses  Used to display the setting page
     *
     * @created Akshata
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function settings() {

        $env_values = EnvEditorHelper::getEnvValues();

        return view('admin.settings.settings')
                ->with('env_values',$env_values)
                ->with('page' , 'settings');
    }
    
    /**
     * @method settings_save()
     * 
     * @uses to update settings details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param (request) setting details
     *
     * @return success/error message
     */
    public function settings_save(Request $request) {
       
        try {
            
            DB::beginTransaction();
            
            $rules =  
                [
                    'site_logo' => 'mimes:jpeg,jpg,bmp,png',
                    'site_icon' => 'mimes:jpeg,jpg,bmp,png',
                ];

            $custom_errors = 
                [
                    'mimes' => tr('image_error')
                ];

            Helper::custom_validator($request->all(),$rules,$custom_errors);

            foreach( $request->toArray() as $key => $value) {

                if($key != '_token') {

                    $check_settings = Settings::where('key' ,'=', $key)->count();

                    if( $check_settings == 0 ) {

                        throw new Exception( $key.tr('settings_key_not_found'), 101);
                    }
                    
                    if( $request->hasFile($key) ) {
                                            
                        $file = Settings::where('key' ,'=', $key)->first();
                       
                        Helper::storage_delete_file($file->value, FILE_PATH_SITE);

                        $file_path = Helper::storage_upload_file($request->file($key) , FILE_PATH_SITE);    

                        $result = Settings::where('key' ,'=', $key)->update(['value' => $file_path]); 

                        if( $result == TRUE ) {
                     
                            DB::commit();
                   
                        } else {

                            throw new Exception(tr('settings_save_error'), 101);
                        } 
                   
                    } else {

                        if(isset($value)) {

                            $result = Settings::where('key' ,'=', $key)->update(['value' => $value]);

                        } else {

                            $result = Settings::where('key' ,'=', $key)->update(['value' => '']);
                        } 
                        
                        if( $result == TRUE ) {
                         
                            DB::commit();
                       
                        } else {

                            throw new Exception(tr('settings_save_error'), 101);
                        } 

                    }  
 
                }
            }

            Helper::settings_generate_json();

            return back()->with('flash_success', tr('settings_update_success'));
            
        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());
        
        }
    }

    /**
     * @method env_settings_save()
     *
     * @uses To update the email details for .env file
     *
     * @created Akshata
     *
     * @updated
     *
     * @param Form data
     *
     * @return view page
     */

    public function env_settings_save(Request $request) {

        try {

            $env_values = EnvEditorHelper::getEnvValues();

            $env_settings = ['MAIL_DRIVER' , 'MAIL_HOST' , 'MAIL_PORT' , 'MAIL_USERNAME' , 'MAIL_PASSWORD' , 'MAIL_ENCRYPTION' , 'MAILGUN_DOMAIN' , 'MAILGUN_SECRET' , 'FCM_SERVER_KEY', 'FCM_SENDER_ID' , 'FCM_PROTOCOL'];

            if($env_values) {

                foreach ($env_values as $key => $data) {

                    if($request->$key) { 

                        \Enveditor::set($key, $request->$key);

                    }
                }
            }

            $message = tr('settings_update_success');

            return redirect()->route('clear-cache')->with('flash_success', $message);  

        } catch(Exception $e) {

            return back()->withInput()->with('flash_error' , $e->getMessage());

        }  

    }

    /**
     * @method profile()
     *
     * @uses  Used to display the logged in admin details
     *
     * @created Akshata
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function profile() {

        return view('admin.account.profile')
                ->with('page', 'profile');
    }


    /**
     * @method profile_save()
     *
     * @uses To update the admin details
     *
     * @created Akshata
     *
     * @updated
     *
     * @param -
     *
     * @return view page 
     */

    public function profile_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules = 
                [
                    'name' => 'max:191',
                    'email' => $request->admin_id ? 'email|max:191|unique:admins,email,'.$request->admin_id : 'email|max:191|unique:admins,email,NULL',
                    'admin_id' => 'required|exists:admins,id',
                    'picture' => 'mimes:jpeg,jpg,png'
                ];
            
            Helper::custom_validator($request->all(),$rules);
            
            $admin_details = Admin::find($request->admin_id);

            if(!$admin_details) {

                Auth::guard('admin')->logout();

                throw new Exception(tr('admin_details_not_found'), 101);
            }
        
            $admin_details->name = $request->name ?: $admin_details->name;

            $admin_details->email = $request->email ?: $admin_details->email;

            if($request->hasFile('picture') ) {
                
                Helper::storage_delete_file($admin_details->picture, PROFILE_PATH_ADMIN); 
                
                $admin_details->picture = Helper::storage_upload_file($request->file('picture'), PROFILE_PATH_ADMIN);
            }
            
            $admin_details->remember_token = Helper::generate_token();

            $admin_details->save();

            DB::commit();

            return redirect()->route('admin.profile')->with('flash_success', tr('admin_profile_success'));


        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error' , $e->getMessage());

        }    
    
    }

    /**
     * @method change_password()
     *
     * @uses To change the admin password
     *
     * @created Akshata
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function change_password(Request $request) {

        try {

            DB::begintransaction();

            $rules = 
            [              
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required',
            ];
            
            Helper::custom_validator($request->all(),$rules);

            $admin_details = Admin::find(Auth::guard('admin')->user()->id);

            if(!$admin_details) {

                Auth::guard('admin')->logout();
                              
                throw new Exception(tr('admin_details_not_found'), 101);

            }

            if(Hash::check($request->old_password,$admin_details->password)) {

                $admin_details->password = Hash::make($request->password);

                $admin_details->save();

                DB::commit();

                Auth::guard('admin')->logout();

                return redirect()->route('admin.login')->with('flash_success', tr('password_change_success'));
                
            } else {

                throw new Exception(tr('password_mismatch'));
            }

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error' , $e->getMessage());

        }    
    
    }
}
