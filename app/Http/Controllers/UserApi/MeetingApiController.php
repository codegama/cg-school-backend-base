<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\User;

class MeetingApiController extends Controller
{
    protected $loginUser;

    protected $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::CommonResponse()->find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method rooms_index()
     *
     * @uses list rooms created by logged user
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function rooms_index(Request $request) {

        try {

            $base_query = $rooms_query = \App\Room::where('instructor_id', $request->id);

            $rooms = $rooms_query->skip($this->skip)->take($this->take)->get();

            $data['rooms'] = $rooms;

            $data['total'] => $base_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method rooms_view()
     *
     * @uses room view based on the logged in instructor
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function rooms_view(Request $request) {

        try {

            $data['room_details'] = \App\Room::where('rooms.id', $request->room_id)->CommonReponse()->first();

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method rooms_save()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function rooms_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'room_id' => 'nullable|exists:rooms,id'
                    'title' => 'required|max:255',
                    'picture' => 'nullable|mimes:jpeg,jpg,png',
                ];

            $custom_errors = ['room_id' => api_error(130)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $room_details = \App\Room::updateOrCreate(['instructor_id' => $request->id, 'id' => $request->room_id], $request->all());

            $room_details->save();

            DB::commit();

            $data['room_details'] = $room_details;

            return $this->sendResponse($message = api_success(119) , $code = 119, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method rooms_delete()
     *
     * @uses sub instructors remove
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function rooms_delete(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['room_id' => 'required|exists:rooms,id'];

            $custom_errors = ['room_id' => api_error(130)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $room_details = \App\Room::where('rooms.id', $request->room_id)->where('instructor_id', $request->id)->delete();

            //  @todo releated details

            DB::commit();

            return $this->sendResponse($message = api_success(120) , $code = 120, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method rooms_search()
     *
     * @uses sub instructors search
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function rooms_search(Request $request) {

        try {

            // Validation start

            $rules = ['search_key' => 'required|min:2'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $rooms_query = $base_query = \App\Room::where('title','LIKE','%'.$request->search_key.'%')

            $rooms = $rooms_query->skip($this->skip)
                            ->skip($this->take)
                            ->get();

            $data['rooms'] = $rooms;

            $data['total'] = $base_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
    
    /**
     * @method room_users_index()
     *
     * @uses list users under selected room
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function room_users_index(Request $request) {

        try {

            // Validation start

            $rules = ['room_id' => 'required|exists:rooms,id'];

            $custom_errors = ['room_id' => api_error(130)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $base_query = $room_user_query = \App\RoomUser::where('room_id', $request->room_id);

            $room_users = $room_user_query->skip($this->skip)->take($this->take)->get();

            $data['room_users'] = $room_users;

            $data['total'] => $base_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method room_users_view()
     *
     * @uses room view based on the logged in instructor
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function room_users_view(Request $request) {

        try {

            // Validation start

            $rules = ['room_user_id' => 'required|exists:room_users,id'];

            $custom_errors = ['room_user_id' => api_error(131)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $data['room_user_details'] = \App\RoomUser::where('room_users.id', $request->room_user_id)->CommonReponse()->first();

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method room_users_add()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function room_users_add(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'room_id' => 'nullable|exists:rooms,id'
                    'user_id' => 'required|exists:users,id'
                ];

            $custom_errors = [
                    'room_id' => api_error(130),
                    'user_id' => api_error(131),
                ];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $room_user_details = \App\RoomUser::updateOrCreate(['instructor_id' => $request->id, 'room_id' => $request->room_id, 'user_id' => $request->user_id], $request->all());

            $room_user_details->save();

            DB::commit();

            $data['room_user_details'] = $room_user_details;

            return $this->sendResponse($message = api_success(121) , $code = 121, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method room_users_remove()
     *
     * @uses room users remove
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function room_users_remove(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['room_user_id' => 'required|exists:room_users,id'];

            $custom_errors = ['user_id' => api_error(131)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $room_details = \App\RoomUser::where('room_users.id', $request->room_user_id)->where('instructor_id', $request->id)->delete();

            //  @todo releated details

            DB::commit();

            return $this->sendResponse($message = api_success(122) , $code = 122, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method room_users_search()
     *
     * @uses sub instructors search
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function room_users_search(Request $request) {

        try {

            // Validation start

            $rules = ['search_key' => 'required|min:2', 'room_id' => 'required|exists:rooms,id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $room_user_ids = \App\RoomUser::where('instructor_id', $request->id)->where('room_id', $request->room_id)->list('user_id');

            $user_query = $base_query = \App\User::whereNotIn('users.id', '!=', $room_user_ids)
                            ->orWhere('name','LIKE','%'.$request->search_key.'%')
                            ->orWhere('email','LIKE','%'.$request->search_key.'%')
                            ->orWhere('mobile','LIKE','%'.$request->search_key.'%');

            $users = $user_query->skip($this->skip)
                            ->skip($this->take)
                            ->get();

            $data['users'] = $users;

            $data['total'] = $base_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_index()
     *
     * @uses list rooms created by logged user
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_index(Request $request) {

        try {

            $base_query = $rooms_query = \App\Room::where('instructor_id', $request->id);

            $rooms = $rooms_query->skip($this->skip)->take($this->take)->get();

            $data['rooms'] = $rooms;

            $data['total'] => $base_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_view()
     *
     * @uses room view based on the logged in instructor
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_view(Request $request) {

        try {

            $data['room_details'] = \App\Room::where('rooms.id', $request->room_id)->CommonReponse()->first();

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
    
    /**
     * @method meetings_save()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'room_id' => 'nullable|exists:rooms,id'
                    'title' => 'required|max:255',
                    'picture' => 'nullable|mimes:jpeg,jpg,png',
                ];

            $custom_errors = ['room_id' => api_error(130)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $room_details = \App\Room::updateOrCreate(['instructor_id' => $request->id, 'id' => $request->room_id], $request->all());

            $room_details->save();

            DB::commit();

            $data['room_details'] = $room_details;

            return $this->sendResponse($message = api_success(119) , $code = 119, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_delete()
     *
     * @uses sub instructors remove
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_delete(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['room_id' => 'required|exists:rooms,id'];

            $custom_errors = ['room_id' => api_error(130)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $room_details = \App\Room::where('rooms.id', $request->room_id)->where('instructor_id', $request->id)->delete();

            //  @todo releated details

            DB::commit();

            return $this->sendResponse($message = api_success(120) , $code = 120, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }


    /**
     * @method meetings_search()
     *
     * @uses meetings search
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_search(Request $request) {

        try {

            // Validation start

            $rules = ['search_key' => 'required|min:2'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $rooms_query = $base_query = \App\Room::where('title','LIKE','%'.$request->search_key.'%')

            $rooms = $rooms_query->skip($this->skip)
                            ->skip($this->take)
                            ->get();

            $data['rooms'] = $rooms;

            $data['total'] = $base_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_users_index()
     *
     * @uses list users under selected meeting
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_users_index(Request $request) {

        try {

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,id'];

            $custom_errors = ['meeting_id' => api_error(132)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $base_query = $meeting_user = \App\MeetingUser::where('meeting_id', $request->meeting_id);

            $meeting_users = $meeting_user->skip($this->skip)->take($this->take)->get();

            $data['meeting_users'] = $meeting_users;

            $data['total'] => $base_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_users_search()
     *
     * @uses meeting users search based on the key
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_users_search(Request $request) {

        try {

            // Validation start

            $rules = [
                        'search_key' => 'required|min:2', 
                        'meeting_id' => 'required|exists:meetings,id'
                    ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_user_ids = \App\MeetingUser::where('meeting_id', $request->meeting_id)->list('user_id');

            $user_query = $base_query = \App\User::whereNotIn('users.id', '!=', $meeting_user_ids)
                            ->orWhere('name','LIKE','%'.$request->search_key.'%')
                            ->orWhere('email','LIKE','%'.$request->search_key.'%')
                            ->orWhere('mobile','LIKE','%'.$request->search_key.'%');

            $users = $user_query->skip($this->skip)
                            ->skip($this->take)
                            ->get();

            $data['users'] = $users;

            $data['total'] = $base_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
}
