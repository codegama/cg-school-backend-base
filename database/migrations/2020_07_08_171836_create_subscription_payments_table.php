<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id')->default(uniqid());
            $table->integer('subscription_id');
            $table->integer('instructor_id');
            $table->string('payment_id')->default("");
            $table->float('amount')->default(0.00);
            $table->string('payment_mode')->default(COD);
            $table->integer('is_current_subscription')->default(0);
            $table->datetime('expiry_date')->nullable();
            $table->datetime('paid_date')->nullable();
            $table->tinyInteger('status')->default(PAID);
            $table->tinyInteger('is_cancelled')->default(0);
            $table->text('cancel_reason')->nullable("");
            $table->integer('no_of_class')->default(2);
            $table->integer('no_of_users_each_class')->default(1);
            $table->integer('plan')->default(1);
            $table->string('plan_type')->default(PLAN_TYPE_MONTH);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_payments');
    }
}
