@extends('layouts.admin') 

@section('content-header', tr('payments'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.users.index')}}">{{tr('payments')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('revenue_dashboard') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="col-lg-12">
		
	<div class="row">

	    <div class="col-lg-3 col-6">
	        
	        <div class="small-box bg-pink">
	            <div class="inner">
	                <h3>{{$data->total_subscribers}}</h3>

	                <p>{{tr('total_subscribers')}}</p>
	            </div>
	            <div class="icon">
	                <i class="ion ion-android-people"></i>
	            </div>
	        </div>

	    </div>

	    <div class="col-lg-3 col-6">
	        
	        <div class="small-box bg-teal">
	            <div class="inner">
	                <h3>{{$data->today_subscribers}}</h3>

	                <p>{{tr('today_subscribers')}}</p>
	            </div>
	            <div class="icon">
	                <i class="ion ion-person-add"></i>
	            </div>
	        </div>

	    </div>

	    <div class="col-lg-3 col-6">

	        <div class="small-box bg-warning">
	            <div class="inner">
	                <h3>{{formatted_amount($data->total_earnings)}}</h3>

	                <p>{{tr('total_earnings')}}</p>
	            </div>
	            <div class="icon">
	                <i class="ion ion-cash"></i>
	            </div>
	        </div>

	    </div>

	    <div class="col-lg-3 col-6">
	        
	        <div class="small-box bg-gray">
	            <div class="inner">
	                <h3>{{formatted_amount($data->today_earnings)}}</h3>

	                <p>{{tr('today_earnings')}}</p>
	            </div>
	            <div class="icon">
	                <i class="ion ion-cash"></i>
	            </div>
	        </div>

	    </div>
	    
	</div>

</div>

<div class="col-lg-12">

    <div class="card">

	   <div class="card-header border-0">

	        <div class="d-flex justify-content-between">
	           <h3 class="card-title">{{tr('last_week_revenues')}}</h3>
	        </div>

	   </div>

	   <div class="card-body">
	      
	        <div class="position-relative mb-4">
	           <canvas id="visitors-chart" height="200"></canvas>
	        </div>

	        <div class="d-flex flex-row justify-content-end">
	            <span class="mr-2">
	               <i class="fas fa-square text-primary"></i> This Week
	            </span>
	        </div>

	   </div>
       
	</div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

$(function () {
  'use strict'

  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode      = 'index'
  var intersect = true

  var $visitorsChart = $('#visitors-chart')
  var visitorsChart  = new Chart($visitorsChart, {
    data   : {
      labels:[<?php foreach ($data->analytics->last_x_days_revenues as $key => $value) 		{
                    echo '"'.$value->date.'"'.',';
                } 
             ?>],
      datasets: [{
        type                : 'line',
        data                : [<?php 
                                foreach ($data->analytics->last_x_days_revenues as $value) {
                                    echo $value->total_earnings.',';
                                }

                                ?>],
        backgroundColor     : 'transparent',
        borderColor         : '#007bff',
        pointBorderColor    : '#007bff',
        pointBackgroundColor: '#007bff',
        fill                : false
      },
        {
          type                : 'line',
          // data                : [60, 80, 70, 67, 80, 77, 100],
          backgroundColor     : 'tansparent',
          borderColor         : '#ced4da',
          pointBorderColor    : '#ced4da',
          pointBackgroundColor: '#ced4da',
          fill                : false
          
        }]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            suggestedMax: 100
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: true
          },
          ticks    : ticksStyle
        }]
      }
    }
  })
})
</script>

@endsection