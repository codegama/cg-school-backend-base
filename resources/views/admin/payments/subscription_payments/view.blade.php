@extends('layouts.admin') 

@section('content-header', tr('payments'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="#">{{tr('payments')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('subscription_payments') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('subscription_payments')}} - <a href="{{route('admin.instructors.view',['instructor_id' => $subscription_payment_details->instructor_id])}}">{{$subscription_payment_details->instructorDetails->name ?? "-"}}</a></h4>

            </div>

            <div class="card-body">

            	<div class="row">

            		<div class="col-md-6">
					    
					    <div class="card card-widget widget-user-2">

					        <div class="card-footer p-0">

					            <ul class="nav flex-column">

					            	<li class="nav-item">
					                    <div class="nav-link">{{tr('subscription')}}<span class="float-right"><a href="{{route('admin.subscriptions.view',['subscription_id' => $subscription_payment_details->subscription_id])}}">{{$subscription_payment_details->subscriptionDetails->title ?: "-"}}</a></span> </div>
					                </li>

					            	<li class="nav-item">
					                    <div class="nav-link">{{tr('subscriber_name')}} <span class="float-right"><a href="{{route('admin.instructors.view',['instructor_id' => $subscription_payment_details->instructor_id])}}">{{$subscription_payment_details->instructorDetails->name ?? "-"}}</a></span> </div>
					                </li>

					            	<li class="nav-item">
					                    <div class="nav-link">{{tr('email')}} <span class="float-right">{{$subscription_payment_details->instructorDetails->email ?? "-"}}</span> </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('is_current_subscription')}} 
					                    	@if($subscription_payment_details->is_current_subscription == YES)
					                    		<span class="float-right badge bg-primary">{{tr('yes')}}</span>
					                    	@else
					                    	 	<span class="float-right badge bg-danger">{{tr('no')}}</span>
					                    	@endif
					                    </div>
					                </li>
					                <li class="nav-item">
					                    <div class="nav-link">{{tr('expiry_date')}}<span class="float-right">{{common_date($subscription_payment_details->expiry_date,Auth::guard('admin')->user()->timezone)}}</span> </div>
					                </li>
					                
					                <li class="nav-item">
					                    <div class="nav-link">{{tr('is_cancelled')}} 
					                    	@if($subscription_payment_details->is_cancelled == YES)
					                    		<span class="float-right badge bg-primary">{{tr('yes')}}</span>
					                    	@else
					                    	 	<span class="float-right badge bg-danger">{{tr('no')}}</span>
					                    	@endif
					                    </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('cancel_reason')}}<span class="float-right">{{$subscription_payment_details->cancel_reason ?: "-"}}</span> </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('status')}}
					                    	@if($subscription_payment_details->status == APPROVED)
					                    		<span class="float-right badge bg-success text-uppercase">{{tr('approved')}}</span>
					                    	@else
					                    	 	<span class="float-right badge bg-danger text-uppercase">{{tr('declined')}}</span>
					                    	@endif 
					                    </div>
					                </li>

					            </ul>

					        </div>

					    </div>
					   
					</div>

					<div class="col-md-6">
					    
					    <div class="card card-widget widget-user-2">

					        <div class="card-footer p-0">

					            <ul class="nav flex-column">

					            	<li class="nav-item">
					                    <div  class="nav-link">{{tr('payment_id')}}<span class="float-right text-uppercase">{{$subscription_payment_details->payment_id}}</span> </div>
					                </li>

					                <li class="nav-item">
					                   <div class="nav-link">{{tr('amount')}}<span class="float-right text-uppercase">{{formatted_amount($subscription_payment_details->amount)}}</span> </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link"> {{tr('payment_mode')}} <span class="float-right badge bg-secondary text-uppercase">{{$subscription_payment_details->payment_mode}}</span></div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('created_at')}}<span class="float-right">{{common_date($subscription_payment_details->created_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('updated_at')}}<span class="float-right">{{common_date($subscription_payment_details->updated_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					            </ul>

					        </div>

					    </div>
					   
					</div>

		        </div>

            </div>
            
        </div>
        
    </div>
    
</div>

@endsection