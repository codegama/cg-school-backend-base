<div class="row">

    <div class="col-6">

        <form class="search-button" action="{{route('admin.subscription_payments.index')}}" method="GET" role="search">
            <!-- {{csrf_field()}} -->
            <div class="input-group">
                <input type="text" class="form-control" name="search_key"
                    placeholder="{{tr('subscription_payments_search_placeholder')}}" required> 

                    <span class="input-group-btn">
                    &nbsp

                    <button type="submit" class="btn btn-default">
                       <i class="fa fa-search" aria-hidden="true"></i>
                    </button>

                    <a class="btn btn-default" href="{{route('admin.subscription_payments.index')}}"><i class="fa fa-eraser" aria-hidden="true"></i>
                    </a>

                </span>
            </div>
        
        </form>

    </div>

    
</div>

<div class="col-6">
    @if(Request::has('search_key'))
        <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
    @endif
</div>
