@extends('layouts.admin') 

@section('content-header', tr('payments'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="#">{{tr('payments')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('subscription_payments') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('subscription_payments')}}
                </h4>

            </div>

            <div class="card-body">

                @include('admin.payments.subscription_payments._search')

                <table id="table_order" class="table table-bordered table-hover">

                    <thead>

                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('subscription')}}</th>
                            <th>{{tr('payment_id')}}</th>
                            <th>{{tr('username')}}</th>
                            <th>{{tr('amount')}}</th>
                            <th>{{tr('payment_mode')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($subscription_payments as $i => $subscription_payment_details)

                            <tr>
                                <td>{{$i+$subscription_payments->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.subscriptions.view' , ['subscription_id' => $subscription_payment_details->subscription_id])}}"> {{ $subscription_payment_details->subscriptionDetails->title ?? "-"}}
                                    </a>
                                </td>

                                <td> {{ $subscription_payment_details->payment_id }} </td>

                                <td>  
                                    <a href="{{route('admin.instructors.view' , ['instructor_id' => $subscription_payment_details->instructor_id])}}"> {{ $subscription_payment_details->instructorDetails->name ?? "-" }} 
                                    </a>
                                </td>

                                <td> 
                                    {{ formatted_amount($subscription_payment_details->amount) }} 
                                </td>

                                <td> 
                                    <span  class="badge bg-info">{{ $subscription_payment_details->payment_mode  }} </span>
                                </td>

                                <td>

                                    @if($subscription_payment_details->status == USER_APPROVED)

                                        <span class="badge bg-success">{{ tr('paid') }} </span>

                                    @else

                                        <span class="badge bg-danger">{{ tr('unpaid') }} </span>

                                    @endif

                                </td>

                                <td>     

                                    <a class="btn btn-info" href="{{ route('admin.subscription_payments.view', ['subscription_payment_id' => $subscription_payment_details->subscription_payment_id]) }}">
                                    {{tr('view')}}</a>

                                </td>

                            </tr>

                        @endforeach
                        
                    </tbody>
                   
                </table>

                <div class="pull-right">{{ $subscription_payments->appends(Request::all())->links() }}</div>

            </div>
            
        </div>
        
    </div>
    
</div>

@endsection