@extends('layouts.admin')

@section('content-header', tr('instructors'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.instructors.index') }}">{{tr('instructors')}}</a></li>
    
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{ tr('edit_instructor') }}</span>
    </li>
           
@endsection 

@section('content')

	@include('admin.instructors._form')

@endsection