@extends('layouts.admin')

@section('content-header', tr('instructors'))

@section('bread-crumb')

    <li class="breadcrumb-item">
    	<a href="{{ route('admin.instructors.index') }}">{{tr('instructors')}}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{tr('subscription_history')}}</span>
    </li>
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('subscription_history')}}
                    - <a href="{{route('admin.instructors.view' , ['instructor_id' => $instructor_details->instructor_id ?? 0])}}">{{$instructor_details->name}}</a>
                </h4>

            </div>

            <div class="card-body">

                <table id="table_order" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('payment_id')}}</th>
                            <th>{{tr('subscription')}}</th>
                            <th>{{tr('plan')}}</th>
                            <th>{{tr('amount')}}</th> 
                            <th>{{tr('expiry_date')}}</th>        
                            <th>{{tr('no_of_class')}}</th> 
                            <th>{{tr('no_of_users_each_class')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach($subscripton_payment_history as $i => $payment_details)
                              
                            <tr>
                                <td>{{$i+$subscripton_payment_history->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.subscription_payments.view' , ['subscription_payment_id' => $payment_details->id ?? 0])}}">{{$payment_details->payment_id}}</a>
                                </td>

                                <td>
                                    <a href="{{route('admin.subscriptions.view' , ['subscription_id' => $payment_details->subscriptionDetails->id ?? 0])}}"> {{ $payment_details->subscriptionDetails->title ?? '-' }}
                                    </a>
                                </td>

                                <td> {{$payment_details->plan_formatted}} </td>

                                <td>{{$payment_details->subscription_amount_formatted}}</td>

                                <td class="text-danger">{{common_date($payment_details->expiry_date,Auth::guard('admin')->user()->timezone)}}</td>
                                <td>
                                    {{ $payment_details->subscriptionDetails->no_of_class ?? "-" }}
                                </td>

                                <td>
                                    {{ $payment_details->subscriptionDetails->no_of_users_each_class ?? "-" }}
                                </td>

                                <td>     
                                    @if(Setting::get('is_demo_control_enabled') == NO)
                                  
                                        <a class="btn btn-danger" href="{{route('admin.instructors.subscriptions_payments_delete', ['subscription_payment_id' => $payment_details->id])}}" 
                                        onclick="return confirm(&quot;{{tr('subscription_history_delete_confirmation')}}&quot;);">
                                            {{tr('delete')}}
                                        </a>
                                    @endif

                                </td>

                            </tr>

                        @endforeach

                    </tbody>
                   
                </table>

                <div class="pull-right">{{ $subscripton_payment_history->links() }}</div>
                
            </div>
            
        </div>
        
    </div>
   
</div>


<div class="row">

	@foreach($subscriptions as $subscription_details)

		<div class="col-md-3">   

			<div class="card card-danger card-outline">

			   <div class="card-body box-profile">

			      	<h3 class="profile-username text-center text-uppercase">{{$subscription_details->title}}</h3>

			      	<p class="text-muted text-center">{{$subscription_details->plan_formatted}}</p>
			      
				      	<ul class="list-group list-group-unbordered mb-3">

				           	<li class="list-group-item">

				                <b>{{tr('no_of_class')}}</b>

                                <a class="float-right">
                                    {{$subscription_details->no_of_class}}
                                </a>

					        </li>

                            <li class="list-group-item">
                                <b>{{tr('no_of_users_each_class')}}</b> <a class="float-right">{{$subscription_details->no_of_users_each_class}}</a>
                            </li>

					        <li class="list-group-item">
					            <b>{{tr('amount')}}</b> <a class="float-right">{{$subscription_details->subscription_amount_formatted}}</a>
					        </li>
					        
					    </ul>
					<form action="{{route('admin.instructors.subscriptions_payments')}}" method="POST">
						@csrf
						<input type="hidden" name="subscription_id" value="{{$subscription_details->id}}">

						<input type="hidden" name="instructor_id" value="{{Request::get('instructor_id')}}">

						<button class="btn btn-danger btn-block" type="submit" onclick="return confirm('{{tr('do_you_want_to_subscribe_this_plan')}}')">{{tr('subscribe')}}</button>

					</form>

			   </div>

			</div>
	           
	    </div>

    @endforeach

</div>

@endsection