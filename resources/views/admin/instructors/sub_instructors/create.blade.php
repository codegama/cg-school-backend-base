@extends('layouts.admin')

@section('content-header', tr('instructors'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.instructors.index')}}">{{tr('instructors')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('sub_instructors') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

	<div class="col-md-12">

		<div class="card card-navy card-outline">

			<div class="card-header">

				<h4 class="text-uppercase">

                   {{tr('sub_instructors')}} - ({{$sub_instructor_count ?? 0}})

                   <a href="{{route('admin.instructors.view',['instructor_id' => $instructor_details->id])}}" class="pull-right"><span class="text-uppercase">{{tr('instructor')}}</span> - <span class="badge badge-info"> {{$instructor_details->name}}</span></a>

                </h4>

			</div>

			<div class="card-body">

				<div class="row">

					<div class="col-md-6">

						<div class="card-body card-comments">

							@if($sub_instructors->isEmpty())

								<div class="text-center m-5">
					          		<h2 class="text-muted">
					          			<i class="fa fa-inbox"></i>
					          		</h2>
					          		<p>{{tr('sub_instructor_not_found')}}</p> 
					          	</div>
						   	@endif
							
							@foreach($sub_instructors as $sub_instructor_details)

							    <div class="card-comment">

							        <img class="img-circle img-sm" src="{{$sub_instructor_details->instructorDetails->picture ?? '-' }}" alt="User Image" />

							        <div class="comment-text">

							            <span class="username">
							                <a href="{{route('admin.instructors.view',['instructor_id' =>$sub_instructor_details->instructorDetails->id ?? 0])}}">{{$sub_instructor_details->instructorDetails->name ?? '-'}}</a>
							               
							                <a href="{{route('admin.sub_instructors.delete',['sub_instructor_id' => $sub_instructor_details->id])}}" class="badge badge-danger icon-color" onclick="return confirm('are you sure?')"><i class="fas fa-trash"></i></a>
							       
							            </span>
							            
							            {{$sub_instructor_details->instructorDetails->email ?? "-"}}

							            <span class="pull-right">
							            {{tr('mobile')}} - 
							            {{$sub_instructor_details->instructorDetails->mobile ?? "-"}}</span>


							        </div>
							       
							    </div>

						    @endforeach

						    <div class="pull-right">{{ $sub_instructors->appends(['instructor_id' => $search_key ?? ""])->links()}}</div>
						   		
						</div>
					</div>

					<div class="col-md-6">

						<div class="card">

							<div class="card card-navy">

								<div class="card-header">
									{{tr('assign_subinstructor')}}
								</div>
								
							</div>

							<form class="forms-sample" action="{{ Setting::get('is_demo_control_enabled') == NO ? route('admin.sub_instructors.save') : '#'}}" method="POST" enctype="multipart/form-data" role="form">
			                @csrf

			                	<input type="hidden" name="instructor_id" value="{{$instructor_details->id}}">

				                <div class="card-body">

				                    <div class="row">

				                        <div class="form-group col-md-12">

				                            <label for="title">{{ tr('select_subinstructor') }} <span class="admin-required">*</span> </label>

				                            <select class="form-control js-example-basic-multiple select2" id="instructor_id" name="sub_instructor_ids[]" multiple="multiple">

				                            @foreach($instructors as $instructor_details)
				                                <option value="{{$instructor_details->id}}"@if($instructor_details->is_selected == YES) selected @endif>
				                                    {{$instructor_details->name}}
				                                </option>
				                            @endforeach

				                            </select>

				                        </div>
				                     
				                    </div>

				                </div>

				                <div class="card-footer">
				  
				                    @if(Setting::get('is_demo_control_enabled') == NO )

				                    <button type="submit" class="btn btn-success mr-2">{{ tr('submit') }}</button>

				                    @else

				                    <button type="button" class="btn btn-success mr-2" disabled>{{ tr('submit') }}</button>

				                    @endif
				                </div>
			            	</form>
		            	</div>

					</div>

				</div>

			</div>
			
		</div>

	</div>

</div>
@endsection