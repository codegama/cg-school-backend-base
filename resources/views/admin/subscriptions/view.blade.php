@extends('layouts.admin') 

@section('content-header', tr('subscriptions'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{route('admin.subscriptions.index')}}">{{tr('subscriptions')}}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
        <span>{{tr('view_subscriptions')}}</span>
    </li>
           
@endsection  

@section('content')

<div class="col-12">

	<div class="card card-navy card-outline">

	    <div class="card-header">

	       	<h4 class="text-uppercase">{{tr('view_subscriptions')}} - {{$subscription_details->title}}
	   
		       	<div class="pull-right">

			       @if(Setting::get('admin_delete_control') == YES )

			      		<a href="{{ route('admin.subscriptions.edit', ['subscription_id' => $subscription_details->id] ) }}" class="btn btn-warning" title="{{tr('edit')}}"><b><i class="fa fa-edit"></i></b></a>

			      		<a onclick="return confirm(&quot;{{ tr('subscription_delete_confirmation', $subscription_details->title ) }}&quot;);" href="javascript:;" class="btn btn-danger" title="{{tr('delete')}}"><b><i class="fa fa-trash"></i></b>
			      			</a>

			   		@else
			   			<a href="{{ route('admin.subscriptions.edit' , ['subscription_id' => $subscription_details->id] ) }}" class="btn btn-warning" title="{{tr('edit')}}"><b><i class="fa fa-edit"></i></b></a>	
			      		                			
			      	 	<a onclick="return confirm(&quot;{{ tr('subscription_delete_confirmation', $subscription_details->title ) }}&quot;);" href="{{ route('admin.subscriptions.delete', ['subscription_id' => $subscription_details->id] ) }}" class="btn btn-danger" title="{{tr('delete')}}"><b><i class="fa fa-trash"></i></b>
			      			</a>
			      	@endif

			      	@if($subscription_details->status == APPROVED)

		                <a class="btn btn-danger" title="{{tr('decline')}}" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}" onclick="return confirm(&quot;{{$subscription_details->title}} - {{tr('subscription_decline_confirmation')}}&quot;);" >
		                    <b><i class="fas fa-ban"></i></b>
		                </a>

		            @else
		                
		                <a class="btn btn-success" title="{{tr('approve')}}" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}">
		                    <b><i class="fas fa-check-circle"></i></b> 
		                </a>
		                   
		            @endif

		      	</div>

	      	</h4>

	    </div>
	    
	    <div class="card-body">

	      	<div class="row">

	      		<div class="col-md-3">

	      			<div class="info-box bg-light">
		                <div class="info-box-content">
		                  	<span class="info-box-text text-center text-info"><b>{{tr('no_of_class')}}</b></span>
		                  	<span class="info-box-number text-center text-muted mb-0">{{$subscription_details->no_of_class}}</span>
		                </div>
		           	</div>
		        </div>

		        <div class="col-md-3">

		           	<div class="info-box bg-light">
		                <div class="info-box-content">
		                  	<span class="info-box-text text-center text-info"><b>{{tr('no_of_users_each_class')}}</b></span>
		                  	<span class="info-box-number text-center text-muted mb-0">{{$subscription_details->no_of_users_each_class}}</span>
		                </div>
	              	</div>
	            </div>

	            <div class="col-md-3">

		           	<div class="info-box bg-light">
		                <div class="info-box-content">
		                  	<span class="info-box-text text-center text-info"><b>{{tr('total_subscribers')}}</b></span>
		                  	<span class="info-box-number text-center text-muted mb-0"><a href="{{route('admin.subscription_payments.index',['subscription_id' => $subscription_details->id])}}">{{$subscription_details->subscriptionPayments()->count()}}</a></span>
		                </div>
	              	</div>
	            </div>

	            <div class="col-md-3">

	              	<div class="info-box bg-light">
		                <div class="info-box-content">
		                  	<span class="info-box-text text-center text-info"><b>{{tr('total_amount_spent')}}</b></span>
		                  	<span class="info-box-number text-center text-muted mb-0">{{formatted_amount($subscription_details->amount)}}</span>
		                </div>
	              	</div>

	        	</div>

	        	<div class="col-6">

	        		<div class="card">

		        		<div class="card-header">
		        			<h4>{{tr('subscription_details')}}</h4>
		        		</div>

		        		<div class="card-body">

		        			<ul class="nav flex-column">

		        				<li class="nav-item">
		        					{{tr('title')}}
		        					<span class="float-right"> {{$subscription_details->title}}</span>
		        				</li>
		        				<hr>

		        				<li class="nav-item">
		        					{{tr('plan')}}
		        					<span class="float-right"> {{$subscription_details->plan}}</span>
		        				</li>
		        				<hr>

		        				<li class="nav-item">
		        					{{tr('plan_type')}}
		        					<span class="float-right"> {{$subscription_details->plan_type}}</span>
		        				</li>
		        		
		        			</ul>
		        			
		                </div>

		            </div>

	        	</div>

	        	<div class="col-6">

	        		<div class="card">
	        			
		        		<div class="card-body">

		        			<ul class="nav flex-column">

		        				<li class="nav-item">
		        					{{tr('is_popular')}}
		        					@if($subscription_details->is_popular == YES)
		        						<span class="float-right badge bg-success">{{tr('yes')}}</span>
		        					@else
		        						<span class="float-right badge bg-danger">
		        							{{tr('no')}}
		        						</span>
		        					@endif
		        				</li>
		        				<hr>
		        			
		        				<li class="nav-item">
		        					{{tr('is_free')}}
		        					@if($subscription_details->is_free == YES)
		        						<span class="float-right badge bg-success">{{tr('yes')}}</span>
		        					@else
		        						<span class="float-right badge bg-danger">
		        							{{tr('no')}}
		        						</span>
		        					@endif
		        				</li>
		        				<hr>

		        				<li class="nav-item">
		        					{{tr('status')}}
		        					@if($subscription_details->status == YES)
		        						<span class="float-right badge bg-success">
		        						{{tr('approved')}}
		        						</span>
		        					@else
		        						<span class="float-right badge bg-danger">
		        							{{tr('declined')}}
		        						</span>
		        					@endif
		        				</li>
		        				<hr>

		        				<li class="nav-item">
		        					{{tr('created_at')}}
		        					<span class="float-right"> {{common_date($subscription_details->created_at,Auth::guard('admin')->user()->timezone)}}</span>
		        				</li>
		        				<hr>

		        				<li class="nav-item">
		        					{{tr('updated_at')}}
		        					<span class="float-right"> {{common_date($subscription_details->updated_at,Auth::guard('admin')->user()->timezone)}}</span>
		        				</li>
		        			</ul>
		                	
		                </div>

	                </div>

	        	</div>

	        	<div class="col-12">
	        		<h5 class="text-uppercase">{{tr('description')}}</h5>
                	<hr>
                	
                	<div>

                		<div  class="col-md-8 text-word-wrap pull-left"><a>{{$subscription_details->description}}</a></div>
		             
                	</div>
	        	</div>
	            
	      	</div>

	    </div>

	</div>

</div>

@endsection