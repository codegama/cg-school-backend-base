@extends('layouts.admin') 

@section('content-header', tr('rooms'))

@section('bread-crumb')

    <li class="breadcrumb-item">
    	<a href="{{ route('admin.rooms.index') }}">{{tr('rooms')}}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{tr('add_room')}}</span>
    </li>
           
@endsection

@section('content') 

	@include('admin.rooms._form') 

@endsection