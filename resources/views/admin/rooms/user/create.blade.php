@extends('layouts.admin')

@section('content-header', tr('rooms'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.rooms.index')}}">{{tr('rooms')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('users') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

	<div class="col-md-12">

		<div class="card card-navy card-outline">

			<div class="card-header">

				 <h4 class="text-uppercase">

                   {{tr('users')}}
                   
                </h4>

			</div>

			<div class="card-body">

				<div class="row">

					<div class="col-12 col-sm-6 col-md-3">
				        <div class="info-box">
				            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-home"></i></span>

				            <div class="info-box-content">
				                <span class="info-box-text text-uppercase">{{tr('room_title')}}</span>
				                <span class="info-box-number">
				                   <a href="{{route('admin.rooms.view',['room_id' => $room_details->id])}}">{{$room_details->title}}</a>
				                </span>
				            </div>
				           
				        </div>
				     
					</div>
					   
				    <div class="clearfix hidden-md-up"></div>

				    <div class="col-12 col-sm-6 col-md-3">
				        <div class="info-box mb-3">
				            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

				            <div class="info-box-content">
				                <span class="info-box-text text-uppercase">{{tr('instructor')}}</span>
				                <span class="info-box-number"><a href="{{route('admin.instructors.view',['instructor_id' => $room_details->instructorDetails->id ?? 0])}}">{{$room_details->instructorDetails->name ?? "-"}}</a></span>
				            </div>
				          
				        </div>
				        
				    </div>

				    <div class="clearfix hidden-md-up"></div>

				    <div class="col-12 col-sm-6 col-md-3">
				        <div class="info-box mb-3">
				            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

				            <div class="info-box-content">
				                <span class="info-box-text text-uppercase">{{tr('total_users')}}</span>
				                <span class="info-box-number">{{$room_details->roomUserDetails->count() ?? "-"}}</span>
				            </div>
				          
				        </div>
				        
				    </div>

				    <div class="clearfix hidden-md-up"></div>

				    <div class="col-12 col-sm-6 col-md-3">
				        <div class="info-box mb-3">
				            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-chalkboard-teacher"></i></span>

				            <div class="info-box-content">
				                <span class="info-box-text text-uppercase">{{tr('sub_instructors')}}</span>
				                <span class="info-box-number">{{$room_details->instructorDetails->subInstructorDetails->count() ?? "-"}}</span>
				            </div>
				          
				        </div>
				        
				    </div>

				 </div>

				<div class="row">

					<div class="card col-sm-6">

							<div class="card card-navy">

								<div class="card-header">
									{{tr('add_user_for_meeting')}}
								</div>
								
							</div>

							<form class="forms-sample" action="{{ Setting::get('is_demo_control_enabled') == NO ? route('admin.rooms.user.save') : '#'}}" method="POST" enctype="multipart/form-data" role="form">
			                @csrf

			                	<input type="hidden" name="room_id" value="{{$room_details->id}}">

			                	<input type="hidden" name="instructor_id" value="{{$room_details->instructorDetails->id ?? 0}}">

				                <div class="card-body">

				                    <div class="row">

				                        <div class="form-group col-md-12">

				                            <label for="title">{{ tr('select_users') }} <span class="admin-required">*</span> </label>

				                            <select class="form-control js-example-basic-multiple select2" id="user_id" name="user_ids[]" multiple="multiple">
				                         
				                            @foreach($users as $user_details)
				                                <option value="{{$user_details->id}}"@if($user_details->is_selected == YES) selected @endif>
				                                    {{$user_details->name}}
				                                </option>
				                            @endforeach

				                            </select>

				                        </div>
				                     
				                    </div>

				                </div>

				                <div class="card-footer">

				                    @if(Setting::get('is_demo_control_enabled') == NO )

				                    <button type="submit" class="btn btn-success mr-2">{{ tr('submit') }}</button>

				                    @else

				                    <button type="button" class="btn btn-success mr-2" disabled>{{ tr('submit') }}</button>

				                    @endif
				                </div>
			            	</form>
		            
		            </div>

					<div class="col-md-6">
					   
					    <div class="card">
					        <div class="card-header">
					            <h3 class="card-title">{{tr('latest_room_users')}}</h3>

					        </div>
					        
					        <div class="card-body p-0">

					            <ul class="users-list clearfix">

					            	@if($room_users->isEmpty())

										<div class="text-center m-5">
							          		<h2 class="text-muted">
							          			<i class="fa fa-inbox"></i>
							          		</h2>
							          		<p>{{tr('room_users_not_found')}}</p> 
							          	</div>
								   	@endif

					            	@foreach($room_users as $room_user_details)
						                <li>
						                    <img class="room-user-image" src="{{$room_user_details->userDetails->picture ?? asset('placeholder.jpeg')}}" alt="User Image" />
						                    <a class="users-list-name" href="{{route('admin.users.view',['user_id' => $room_user_details->userDetails->id ?? 0])}}">{{$room_user_details->userDetails->name ?? "-"}}</a>
						                    <a href="{{route('admin.rooms.user.delete' ,['rooms_user_id' => $room_user_details->id])}}" class="btn-xs btn-danger icon-color" onclick="return confirm('are you sure?')"><i class="fas fa-trash"></i></a>
						                </li>
					               @endforeach

					            </ul>
					              <div class="pull-right">{{ $room_users->appends(['room_id' => $search_key ?? ""])->links() }}</div>
					           
					        </div>
					        
					    </div>

					</div>
				</div>

			</div>

		</div>

	</div>

</div>
@endsection