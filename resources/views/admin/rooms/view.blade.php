@extends('layouts.admin') 

@section('content-header', tr('rooms'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.rooms.index')}}">{{tr('rooms')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_rooms') }}</span>
    </li> 
           
@endsection 

@section('content')


<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('view_rooms')}}</h4>

            </div>

            <div class="card-body">

            	<div class="row">

	            	<div class="col-md-6">

					    <div class="card card-widget widget-user-2">

					        <div class="widget-user-header">
					            <div class="widget-user-image">
					                <img class="user-image" src="{{$room_details->picture ?? asset('placeholder.png')}}" alt="User Avatar" />
					            </div>
					        </div>

					        @if(Setting::get('is_demo_control_enabled') == NO)

					        	<div class="row">

	                                <div class="col-md-6 align-left">
				                		<a class="btn btn-block btn-outline-secondary" href="{{ route('admin.rooms.edit', ['room_id' => $room_details->id]) }}">
	                                    {{tr('edit')}}
	                                	</a>
				                	</div>

	                                <div class="col-md-6 align-left">
		                                <a class="btn btn-block btn-outline-danger" href="{{route('admin.rooms.delete', ['room_id' => $room_details->id])}}" 
		                                onclick="return confirm(&quot;{{tr('room_delete_confirmation' , $room_details->title)}}&quot;);">
		                                    {{tr('delete')}}
		                                </a>
		                            </div>

	                            </div>

                            @else
                        		<div class="row">
	                            	<div class="col-md-6 align-left">
				                		<a class="btn btn-block btn-outline-secondary" href="room_details:;">{{tr('edit')}}</a>
				                	</div>
				                	<div class="col-md-6 align-left">
				                    	<a class="btn btn-block btn-outline-danger" href="javascript:;" onclick="return confirm(&quot;{{tr('room_delete_confirmation' , $room_details->title)}}&quot;);">{{tr('delete')}}</a>
				                    </div>
			                    </div>
                                                          
                            @endif

			                <div class="row">

					        	<div class="col-md-6 align-left">

					        		@if($room_details->status == APPROVED)

		                                <a class="btn btn-block btn-outline-danger" href="{{ route('admin.rooms.status', ['room_id' => $room_details->id]) }}" onclick="return confirm(&quot;{{$room_details->title}} - {{tr('room_decline_confirmation')}}&quot;);" >
		                                    {{ tr('decline') }} 
		                                </a>

		                            @else
		                                
		                                <a class="btn btn-block btn-outline-success" href="{{ route('admin.rooms.status', ['room_id' => $room_details->id]) }}">
		                                    {{ tr('approve') }} 
		                                </a>
		                                   
		                            @endif
			                	</div>

			                </div>

			                <br>

					    </div>

					</div>

					<div class="col-md-6">
					    
					    <div class="card card-widget widget-user-2">

					        <div class="card-footer p-0">

					            <ul class="nav flex-column">

					            	<li class="nav-item">
					                    <div  class="nav-link">{{tr('title')}} <span class="float-right text-uppercase">{{$room_details->title}}</span> </div>
					                </li>

					                <li>
					                	<div  class="nav-link">{{tr('instructor')}} 
					                	<span class="float-right text-uppercase">
					                	<a href="{{route('admin.instructors.view' , ['instructor_id' => $room_details->instructor_id])}}"> {{$room_details->instructorDetails->name ?? "-"}}</a></span></div>
					                </li>

					            	<li class="nav-item">
					                    <div class="nav-link">{{tr('unique_id')}}<span class="float-right text-uppercase">{{$room_details->unique_id}}</span> </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('status')}}
					                    	@if($room_details->status == APPROVED)
					                    		<span class="float-right badge bg-success text-uppercase">{{tr('approved')}}</span>
					                    	@else
					                    	 	<span class="float-right badge bg-danger text-uppercase">{{tr('declined')}}</span>
					                    	@endif 
					                    </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('created_at')}}<span class="float-right">{{common_date($room_details->created_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('updated_at')}}<span class="float-right">{{common_date($room_details->updated_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					                <li class="nav-item">
					                   <div class="nav-link">{{tr('description')}}<span class="float-right text-uppercase">{{$room_details->description}}</span> </div>
					                </li>

					            </ul>

					        </div>

					    </div>
					   
					</div>

		        </div>

            </div>
            
        </div>
        
    </div>
    
</div>
@endsection