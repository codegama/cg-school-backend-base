@extends('layouts.admin') 

@section('content-header', tr('rooms'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.rooms.index')}}">{{tr('rooms')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_rooms') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('view_rooms')}}
                    <a class="btn btn-outline-success pull-right" href="{{route('admin.rooms.create')}}">
                        <i class="fa fa-plus"></i> {{tr('add_room')}}
                    </a>
                </h4>

            </div>

            <div class="card-body">

                <form class="col-6  search-button" action="{{route('admin.rooms.index')}}" method="GET" role="search">

                   <!--  {{csrf_field()}} -->

                    <div class="input-group">

                        <input type="text" class="form-control search-input" name="search_key"
                        placeholder="{{tr('rooms_search_placeholder')}}" required> <span class="input-group-btn">
                        &nbsp

                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-search search-color" aria-hidden="true"></i>
                        </button>

                        <a class="btn btn-default search-color" href="{{route('admin.rooms.index')}}"><i class="fa fa-eraser" aria-hidden="true"></i>
                            </a>
                        </span>
                        
                    </div>
                
                </form>

                <table id="table_order" class="table table-bordered table-hover">

                    <thead>

                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('unique_id')}}</th>
                            <th>{{tr('instructor')}}</th>
                            <th>{{tr('title')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($rooms as $i => $room_details)
                              
                            <tr>
                                <td>{{$i+$rooms->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.rooms.view' , ['room_id' => $room_details->id])}}" class="text-uppercase"> {{ $room_details->unique_id }}
                                    </a>
                                </td>

                                <td>
                                    <a href="{{route('admin.instructors.view' , ['instructor_id' => $room_details->instructor_id])}}"> {{$room_details->instructorDetails->name ?? tr('unavailable')}}</a>

                                    <br>

                                    <span class="text-success">

                                        {{tr('mob')}} - {{$room_details->instructorDetails->mobile ?? tr('unavailable')}}

                                    </span>

                                </td>

                                <td> {{ $room_details->title }} </td>

                                <td>

                                    @if($room_details->status == APPROVED)

                                        <span class="badge bg-success">{{ tr('approved') }} </span>

                                    @else

                                        <span class="badge bg-danger">{{ tr('declined') }} </span>

                                    @endif

                                </td>

                                <td>     

                                    <div class="template-demo">

                                        <div class="dropdown">

                                            <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{tr('action')}}
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                              
                                                <a class="dropdown-item" href="{{ route('admin.rooms.view', ['room_id' => $room_details->id]) }}">
                                                    {{tr('view')}}
                                                </a>
                                                
                                                @if(Setting::get('is_demo_control_enabled') == NO)
                                                    <a class="dropdown-item" href="{{ route('admin.rooms.edit', ['room_id' => $room_details->id]) }}">
                                                        {{tr('edit')}}
                                                    </a>
                                                    
                                                    <a class="dropdown-item" href="{{route('admin.rooms.delete', ['room_id' => $room_details->id])}}" 
                                                    onclick="return confirm(&quot;{{tr('room_delete_confirmation' , $room_details->title)}}&quot;);">
                                                        {{tr('delete')}}
                                                    </a>
                                                @else

                                                    <a class="dropdown-item text-muted" href="javascript:;">{{tr('edit')}}</a>
                                                  
                                                    <a class="dropdown-item text-muted" href="javascript:;" onclick="return confirm(&quot;{{tr('room_delete_confirmation' , $room_details->title)}}&quot;);">{{tr('delete')}}</a>                           
                                                @endif

                                                <div class="dropdown-divider"></div>


                                                @if($room_details->status == APPROVED)

                                                    <a class="dropdown-item" href="{{ route('admin.rooms.status', ['room_id' => $room_details->id]) }}" onclick="return confirm(&quot;{{$room_details->title}} - {{tr('room_decline_confirmation')}}&quot;);" >
                                                        {{ tr('decline') }} 
                                                    </a>

                                                @else
                                                    
                                                    <a class="dropdown-item" href="{{ route('admin.rooms.status', ['room_id' => $room_details->id]) }}">
                                                        {{ tr('approve') }} 
                                                    </a>
                                                       
                                                @endif

                                                <a class="dropdown-item" href="{{ route('admin.rooms.user.create', ['room_id' => $room_details->id]) }}">
                                                        {{ tr('assign_users') }} 
                                                    </a>


                                            </div>

                                        </div>

                                    </div>

                                </td>

                            </tr>

                        @endforeach
                        
                    </tbody>
                   
                </table>

                <div class="pull-right">{{ $rooms->appends(request()->input())->links() }}</div>

            </div>
            
        </div>
       
    </div>
   
</div>


@endsection