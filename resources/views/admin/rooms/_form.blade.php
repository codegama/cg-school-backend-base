<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">
                <h4 class="text-uppercase">

                    {{$room_details->id ? tr('edit_room') : tr('add_room')}}

                    <a class="btn btn-outline-success pull-right" href="{{route('admin.rooms.index')}}"> <i class="fa fa-eye"></i> {{tr('view_rooms')}} </a>
                </h4>
            </div>

            <form class="forms-sample" action="{{ Setting::get('is_demo_control_enabled') == NO ? route('admin.rooms.save') : '#'}}" method="POST" enctype="multipart/form-data" role="form">
                @csrf

                <div class="card-body">
                    @if($room_details->id)

                    <input type="hidden" name="room_id" id="room_id" value="{{$room_details->id}}" />

                    @endif

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="title">{{ tr('title') }} <span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="{{ tr('title') }}" value="{{ old('title') ?: $room_details->title}}" required />
                        </div>

                        <div class="form-group col-md-6">
                            <label>{{tr('upload_image')}}</label>

                            <div class="input-group col-xs-12">
                                <input type="file" class="form-control file-upload-info" name="picture" placeholder="{{tr('upload_image')}}" accept="image/*" />

                                <div class="input-group-append">
                                    <button class="btn btn-info" type="button">{{tr('upload')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="form-group col-md-6">

                            <label for="title">{{ tr('select_instructor') }} <span class="admin-required">*</span> </label>
                             <select class="form-control select2" id="instructor_id" name="instructor_id">
                                <option value="">{{tr('select_instructor')}}</option>

                                @foreach($instructors as $instructor_details)
                                    <option value="{{$instructor_details->id}}"@if($instructor_details->is_selected == YES) selected @endif>
                                        {{$instructor_details->name}}
                                    </option>
                                @endforeach

                            </select>

                        </div>
                    </div>

                    <div class="row">

                        <div class="form-group col-md-12">

                            <label for="simpleMde">{{ tr('description') }}</label>

                            <textarea class="form-control" id="description" name="description">{{ old('description') ?: $room_details->description}}</textarea>

                        </div>

                    </div>

                </div>

                <div class="card-footer">
                    <button type="reset" class="btn btn-light">{{ tr('reset')}}</button>

                    @if(Setting::get('is_demo_control_enabled') == NO )

                    <button type="submit" class="btn btn-success mr-2">{{ tr('submit') }}</button>

                    @else

                    <button type="button" class="btn btn-success mr-2" disabled>{{ tr('submit') }}</button>

                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
