@extends('layouts.admin')

@section('content-header')

  {{tr('dashboard')}}

@endsection

@section('bread-crumb')

<li class="breadcrumb-item active" aria-current="page">
    <span>{{ tr('dashboard') }}</span>
</li>

@endsection

@section('content')

<div class="col-lg-12">
	
	<div class="row">

	    <div class="col-lg-3 col-6">
	   
	        <div class="small-box bg-primary">
	            <div class="inner">
	                <h3>{{$data->total_users}}</h3>

	                <p>{{tr('total_users')}}</p>
	            </div>
	            <div class="icon">
	                <i class="ion ion-android-people"></i>
	            </div>
	            <a href="{{route('admin.users.index')}}" class="small-box-footer">{{tr('more_info')}} <i class="fas fa-arrow-circle-right"></i></a>
	        </div>
	    </div>
	    
	    <div class="col-lg-3 col-6">
	        
	        <div class="small-box bg-fuchsia">
	            <div class="inner">
	                <h3>{{$data->total_instructors}}</h3>

	                <p>{{tr('total_instructors')}}</p>
	            </div>
	            <div class="icon">
	                <i class="ion ion-person-add"></i>
	            </div>
	            <a href="{{route('admin.instructors.index')}}" class="small-box-footer">{{tr('more_info')}} <i class="fas fa-arrow-circle-right"></i></a>
	        </div>
	    </div>
	    
	    <div class="col-lg-3 col-6">
	        
	        <div class="small-box bg-success">
	            <div class="inner">
	                <h3>{{formatted_amount($data->total_revenue)}}</h3>

	                <p>{{tr('total_revenue')}}</p>
	            </div>
	            <div class="icon">
	                <i class="ion ion-cash"></i>
	            </div>
	            <a href="{{route('admin.subscription_payments.index')}}" class="small-box-footer">{{tr('more_info')}} <i class="fas fa-arrow-circle-right"></i></a>
	        </div>
	    </div>
	   
	    <div class="col-lg-3 col-6">
	        
	        <div class="small-box bg-warning">
	            <div class="inner">
	                <h3>{{formatted_amount($data->today_revenue)}}</h3>

	                <p>{{tr('today_revenue')}}</p>
	            </div>

	            <div class="icon"><i class="ion ion-cash"></i></div>

	            <a href="{{route('admin.subscription_payments.index',['today_revenue' => YES])}}" class="small-box-footer">{{tr('more_info')}} <i class="fas fa-arrow-circle-right"></i></a>
	        </div>
	    </div>
	    
	</div>

	<div class="row">
		<div class="col-md-12">
			<h4 class="text-muted mb-3 mt-4">{{tr('revenues')}}</h4>
		</div>

		<div class="col-md-6 card card-success card-outline">

		   	<div class="card-header">
            	<h3 class="card-title">{{tr('today_subscriptions')}}</h3>
          	</div>

		   	@if($today_subscriptions->isNotEmpty())

			   	<div class="card-body table-responsive p-0">

			      	<table class="table table-striped table-valign-middle">

				        <thead>
				            <tr>
				               <th>{{tr('subscription')}}</th>
				               <th>{{tr('instructor')}}</th>
				               <th>{{tr('amount')}}</th>
				            </tr>
				        
				        </thead>

				        <tbody>
				         	@foreach($today_subscriptions as $subscription_details)
				            <tr>
				               	<td>{{$subscription_details->subscriptionDetails->title ?? "-"}}</td>
				               	
				               	<td class="text-uppercase">{{$subscription_details->instructorDetails->name ?? "-"}}</td>

				               	<td class="text-success">{{$subscription_details->subscription_amount_formatted}}</td>
				               	
				            </tr>

				            @endforeach
				            
				        </tbody>

				    </table>
			    </div>

			   	<div class="card-footer text-center">
			   		<a href="{{route('admin.subscription_payments.index')}}" class="uppercase">{{tr('view_all')}}</a>
	          	</div>

          	@else

          	<div class="text-center m-5">
          		<h2 class="text-muted">
          			<i class="fa fa-inbox"></i>
          		</h2>
          		<p>No one subscried yet today</p> 
          	</div>

			@endif

		</div>

		<div class="col-md-6">

		    <div class="card card-success card-outline">

		    	<div class="card-header">
	            	<h3 class="card-title">{{tr('overall_subscription')}}</h3>
	          	</div>

		        <div class="card-body">

		            <div class="position-relative mb-4">
		                <canvas id="sales-chart" height="200"></canvas>
		            </div>

		        </div>

		    </div>
		</div>

	</div>

	<div class="row">

		<div class="col-md-12">
			<h4 class="text-muted mb-3 mt-4">{{tr('meetings')}}</h4>
		</div>

	    <div class="col-12 col-sm-6 col-md-3">

	        <div class="info-box dashboard-box-info">
	            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-box"></i></span>

	            <div class="info-box-content">
	                <span class="info-box-text">{{tr('total_meetings')}}</span>
	                <span class="info-box-number">
	                   {{$data->total_meetings}}
	                </span>
	            </div>
	            
	        </div>
	        
	    </div>
	    
	    <div class="col-12 col-sm-6 col-md-3">

	        <div class="info-box dashboard-box-danger mb-3">

	            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-calendar"></i></span>

	            <div class="info-box-content">
	                <span class="info-box-text">{{tr('scheduled_meetings')}}</span>
	                <span class="info-box-number">{{$data->scheduled_meetings}}</span>
	            </div>
	           
	        </div>
	        
	    </div>
	    
	    <div class="clearfix hidden-md-up"></div>

	    <div class="col-12 col-sm-6 col-md-3">

	        <div class="info-box dashboard-box-success mb-3">

	            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-user"></i></span>

	            <div class="info-box-content">
	                <span class="info-box-text">{{tr('todays_meetings')}}</span>
	                <span class="info-box-number">{{$data->todays_meeting}}</span>
	            </div>
	            
	        </div>
	        
	    </div>
	  
	    <div class="col-12 col-sm-6 col-md-3">
	        <div class="info-box dashboard-box-warning mb-3">
	            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-history"></i></span>

	            <div class="info-box-content">
	                <span class="info-box-text">{{tr('completed_meetings')}}</span>
	                <span class="info-box-number">{{$data->completed_meetings}}</span>
	            </div>
	            
	        </div>
	        
	    </div>
	    
	</div>

	<div class="row">

		<div class="col-sm-6">

			<div class="card">

	          	<div class="card-header">
	            	<h3 class="card-title">{{tr('currently_on_live')}}</h3>
	          	</div>
	          
				@if($data->on_live_meetings->isNotEmpty())

		          	<div class="card-body p-0">

			            <ul class="products-list product-list-in-card pl-2 pr-2">

				            @foreach($data->on_live_meetings as $meeting_details)

				              	<li class="item">
					                <div class="product-img">
					                  	<img src="{{$meeting_details->picture}}" alt="Product Image" class="img-size-50">
					                </div>
					                <div class="product-info">
					                  	<a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}" class="product-title">{{$meeting_details->title}}
					                   </a>
					                  	<span class="product-description">
					                  	
					                    {{common_date($meeting_details->start_time,Auth::guard('admin')->user()->timezone)}} - {{common_date($meeting_details->end_time,Auth::guard('admin')->user()->timezone)}}
					                  	</span>
					                </div>
				              	</li>

				           	@endforeach

			            </ul>

			        </div>

		        	<div class="card-footer text-center">
	            		<a href="{{route('admin.meetings.index')}}" class="uppercase">{{tr('view_all')}}</a>
	          		</div>

	          	@else

		            <div class="text-center m-5">
		          		<h2 class="text-muted">
		          			<i class="fa fa-inbox"></i>
		          		</h2>
		          		<p>{{tr('on_live_no_results')}}</p> 
		          	</div>

		        @endif
	        
	        </div>

        </div>

        <div class="col-sm-6">

	        <div class="card">

	          	<div class="card-header">
	            	<h3 class="card-title">{{tr('today_scheduled_meetings')}}</h3>
	          	</div>

	          	@if($data->today_scheduled_meetings->isNotEmpty())
	          
		          	<div class="card-body p-0">

			            <ul class="products-list product-list-in-card pl-2 pr-2">

				            @foreach($data->today_scheduled_meetings as $meeting_details)

				              	<li class="item">

					                <div class="product-img">
					                  <img src="{{$meeting_details->picture}}" alt="Product Image" class="img-size-50">
					                </div>

					                <div class="product-info">
					                  <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}" class="product-title">{{$meeting_details->title}}
					                    </a>
					                  <span class="product-description">
					                  	
					                    {{common_date($meeting_details->start_time,Auth::guard('admin')->user()->timezone)}} - {{common_date($meeting_details->end_time,Auth::guard('admin')->user()->timezone)}}
					                  </span>
					                </div>

				              	</li>

				            @endforeach

			            </ul>

		            </div>
		         
		          	<div class="card-footer text-center">
		            	<a href="{{route('admin.meetings.index')}}" class="uppercase">{{tr('view_all')}}</a>
		          	</div>

	          	@else

		            <div class="text-center m-5">
		          		<h2 class="text-muted"><i class="fa fa-inbox"></i></h2>
		          		<p>{{tr('today_scheduled_meetings_no_results')}}</p> 
		          	</div>

		        @endif
	          
	        </div>

	    </div>

    </div>

</div>

@endsection

@section('scripts')

<script type="text/javascript">

$(function () {
  
	'use strict'

  	var ticksStyle = {
    	fontColor: '#495057',
    	fontStyle: 'bold'
  	}

  	var mode = 'index'; var intersect = true;

  	var $salesChart = $('#sales-chart')
  	
  	var salesChart  = new Chart($salesChart, {
    	type : 'bar',
    	data : {
	      		labels : [<?php foreach ($data->analytics->last_x_days_revenues as $key => $value) 		{
	                    echo '"'.$value->month.'"'.',';
	                } 
	             	?>],
	      		datasets: [
			        {
			          backgroundColor: '#007bff',
			          borderColor    : '#007bff',
			          data           : [<?php 
			                                foreach ($data->analytics->last_x_days_revenues as $value) {
			                                    echo $value->total_earnings.',';
			                                }

			                                ?>]
			        },
			      ]
    		},
    	
    	options: {
      		maintainAspectRatio: false,
	      	tooltips           : {
	        	mode     : mode,
	        	intersect: intersect
	      	},
	      	hover              : {
	        	mode     : mode,
	        	intersect: intersect
	      	},
      		legend             : {
        		display: false
      		},
	      	scales             : {
		        yAxes: [{
		          // display: false,
		          gridLines: {
		            display      : true,
		            lineWidth    : '4px',
		            color        : 'rgba(0, 0, 0, .2)',
		            zeroLineColor: 'transparent'
		          },
		          ticks    : $.extend({
		            beginAtZero: true,

		            // Include a dollar sign in the ticks
		            callback: function (value, index, values) {
		              if (value >= 1000) {
		                value /= 1000
		                value += 'k'
		              }
		              return '$' + value
		            }
		          }, ticksStyle)
		        }],
		        xAxes: [{
		          display  : true,
		          gridLines: {
		            display: false
		          },
		          ticks    : ticksStyle
		        }]
	        }
    	}
  	})
 })

</script>

@endsection

