@extends('layouts.admin') 

@section('content-header', tr('meetings'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.meetings.index')}}">{{tr('meetings')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_meetings') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('view_meetings')}}</h4>

            </div>

            <div class="card-body">

            	<div class="row">

	            	<div class="col-md-6">

					    <div class="card card-widget widget-user-2">

					        <div class="widget-user-header">
					            <div class="widget-user-image">
					                <img class="user-image" src="{{$meeting_details->picture ?? asset('placeholder.png')}}" alt="Meeting Avatar" />
					            </div>
					        </div>

					        @if(Setting::get('is_demo_control_enabled') == NO)

					        	<div class="row">

	                                <div class="col-md-6 align-left">
				                		<a class="btn btn-block btn-outline-secondary" href="{{ route('admin.meetings.edit', ['meeting_id' => $meeting_details->id]) }}">
	                                    {{tr('edit')}}
	                                	</a>
				                	</div>

	                                <div class="col-md-6 align-left">
		                                <a class="btn btn-block btn-outline-danger" href="{{route('admin.meetings.delete', ['meeting_id' => $meeting_details->id])}}" 
		                                onclick="return confirm(&quot;{{tr('meeting_delete_confirmation' , $meeting_details->title)}}&quot;);">
		                                    {{tr('delete')}}
		                                </a>
		                            </div>

	                            </div>

                            @else
                        		<div class="row">
	                            	<div class="col-md-6 align-left">
				                		<a class="btn btn-block btn-outline-secondary" href="javascript:;">{{tr('edit')}}</a>
				                	</div>
				                	<div class="col-md-6 align-left">
				                    	<a class="btn btn-block btn-outline-danger" href="javascript:;" onclick="return confirm(&quot;{{tr('meeting_delete_confirmation' , $meeting_details->title)}}&quot;);">{{tr('delete')}}</a>
				                    </div>
			                    </div>
                                                          
                            @endif

			                <div class="row">

					        	<div class="col-md-6 align-left">

					        		@if($meeting_details->status == APPROVED)

		                                <a class="btn btn-block btn-outline-danger" href="{{ route('admin.meetings.status', ['meeting_id' => $meeting_details->id]) }}" onclick="return confirm(&quot;{{$meeting_details->title}} - {{tr('meeting_decline_confirmation')}}&quot;);" >
		                                    {{ tr('decline') }} 
		                                </a>

		                            @else
		                                
		                                <a class="btn btn-block btn-outline-success" href="{{ route('admin.meetings.status', ['meeting_id' => $meeting_details->id]) }}">
		                                    {{ tr('approve') }} 
		                                </a>
		                                   
		                            @endif
			                	</div>

			                </div>

			                <br>

			                <div class="card-body">

			                	<h5 class="text-uppercase">{{tr('description')}}</h5>

			                	<hr>
			                	
			                	<div>

			                		<div  class="col-md-8 text-word-wrap pull-left"><a>{{$meeting_details->description}}</a></div>
					             
			                	</div>
			                	
			                </div>

					    </div>

					</div>

					<div class="col-md-6">

						<div class="card">

							<div class="card-body">

			                	<h5 class="text-uppercase text-info">{{tr('meeting_timings')}}</h5>
			                	<hr>
			                	<div>
			                		<i class="fas fa-clock"></i>
			                		{{tr('schedule_time')}}<span class="float-right">{{common_date($meeting_details->schedule_time,Auth::guard('admin')->user()->timezone)}}</span>
			                		<hr>
			                		<i class="fas fa-clock"></i>
			                		{{tr('start_time')}}<span class="float-right">{{common_date($meeting_details->start_time,Auth::guard('admin')->user()->timezone)}}</span>
			                		<hr>
			                		<i class="fas fa-clock"></i>
			                		{{tr('end_time')}}<span class="float-right">{{common_date($meeting_details->end_time,Auth::guard('admin')->user()->timezone)}}</span>
			                	</div>
			                	
			                </div>

		                </div>
					    
					    <div class="card card-widget widget-user-2">

					        <div class="card-header p-0">

					            <ul class="nav flex-column">

					            	<li class="nav-item">
					                    <div class="nav-link">{{tr('unique_id')}}<span class="float-right text-uppercase">{{$meeting_details->unique_id}}</span> </div>
					                </li>

					            	<li class="nav-item">
					                    <div  class="nav-link">{{tr('title')}} <span class="float-right text-uppercase">{{$meeting_details->title}}</span> </div>
					                </li>

					                <li class="nav-item">
					                    <div  class="nav-link">{{tr('instructor')}} <span class="float-right text-uppercase"><a href="{{route('admin.instructors.view' , ['instructor_id' => $meeting_details->instructor_id])}}"> {{$meeting_details->instructorDetails->name ?? "-"}}</a></span> </div>
					                </li>

					                <li class="nav-item">
					                    <div  class="nav-link">{{tr('room')}} <span class="float-right text-uppercase"><a href="{{route('admin.rooms.view' , ['room_id' => $meeting_details->room_id])}}"> {{$meeting_details->roomDetails->title ?? "-"}}</a></span> </div>
					                </li>

					                <li class="nav-item">
					                   <div class="nav-link">{{tr('created_by')}}<span class="float-right text-uppercase">{{$meeting_details->created_by}}</span> </div>
					                </li>

					                <li class="nav-item">
					                   <div class="nav-link">{{tr('is_cancelled')}}
					                   	@if($meeting_details->is_cancelled == YES)
					                   		<span class="float-right badge bg-success text-uppercase">{{tr('yes')}}</span>
					                   	@else
					                   	 	<span class="float-right badge bg-info text-uppercase">{{tr('no')}}</span>
					                   	@endif
					                   </div>
					                </li>

					                @if($meeting_details->is_cancelled == YES)
						                <li class="nav-item">
						                   <div class="nav-link">{{tr('cancelled_reason')}}<span class="float-right text-uppercase">{{$meeting_details->cancelled_reason}}</span> </div>
						                </li>
					                @endif

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('status')}}
					                    	@if($meeting_details->status == APPROVED)
					                    		<span class="float-right badge bg-success text-uppercase">{{tr('approved')}}</span>
					                    	@else
					                    	 	<span class="float-right badge bg-danger text-uppercase">{{tr('declined')}}</span>
					                    	@endif 
					                    </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('created_at')}}<span class="float-right">{{common_date($meeting_details->created_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>
					                
					                <li class="nav-item">
					                    <div class="nav-link">{{tr('updated_at')}}<span class="float-right">{{common_date($meeting_details->updated_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					            </ul>

					        </div>

					    </div>
					   
					</div>

		        </div>

            </div>
            
        </div>
        
    </div>
    
</div>
@endsection