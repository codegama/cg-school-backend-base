@extends('layouts.admin') 

@section('content-header', tr('meetings'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.meetings.index')}}">{{tr('meetings')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_meetings') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('view_meetings')}}
                    <a class="btn btn-outline-success pull-right" href="{{route('admin.meetings.create')}}">
                        <i class="fa fa-plus"></i> {{tr('add_meeting')}}
                    </a>
                </h4>

            </div>

            <div class="card-body">
                
                <form class="col-6  search-button" action="{{route('admin.meetings.index')}}" method="GET" role="search">

                   <!--  {{csrf_field()}} -->

                    <div class="input-group">

                        <input type="text" class="form-control search-input" name="search_key"
                        placeholder="{{tr('meetings_search_placeholder')}}" required> <span class="input-group-btn">
                        &nbsp

                        <button type="submit" class="btn btn-default">
                            <i class="fa fa-search search-color" aria-hidden="true"></i>
                        </button>

                        <a class="btn btn-default search-color" href="{{route('admin.meetings.index')}}"><i class="fa fa-eraser" aria-hidden="true"></i>
                            </a>
                        </span>
                        
                    </div>
                
                </form>

                <table id="table_order" class="table table-bordered table-hover">

                    <thead>

                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('unique_id')}}</th>
                            <th>{{tr('title')}}</th>
                            <th>{{tr('instructor')}}</th>
                            <th>{{tr('room')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($meetings as $i => $meeting_details)
                             
                            <tr>
                                <td>{{$i+$meetings->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}"> {{ $meeting_details->unique_id }}
                                    </a>
                                </td>
                               
                                <td> {{ $meeting_details->title }} </td>

                                <td>
                                    <a href="{{route('admin.instructors.view' , ['instructor_id' => $meeting_details->instructor_id])}}"> {{$meeting_details->instructorDetails->name ?? tr('unavailable')}}</a>

                                    <br>

                                    <span class="text-success">
                                        {{tr('mob')}}:{{$meeting_details->instructorDetails->mobile ?? tr('unavailable')}}
                                    </span>

                                </td>

                                <td>
                                    <a href="{{route('admin.rooms.view' , ['room_id' => $meeting_details->room_id])}}"> {{$meeting_details->roomDetails->title ?? tr('unavailable')}}</a>
                                </td>

                                <td>

                                    @if($meeting_details->status == APPROVED)

                                        <span class="badge bg-success">{{ tr('approved') }} </span>

                                    @else

                                        <span class="badge bg-danger">{{ tr('declined') }} </span>

                                    @endif

                                </td>

                                <td>     

                                    <div class="template-demo">

                                        <div class="dropdown">

                                            <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{tr('action')}}
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                              
                                                <a class="dropdown-item" href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id]) }}">
                                                    {{tr('view')}}
                                                </a>
                                                
                                                @if(Setting::get('is_demo_control_enabled') == NO)
                                                    <a class="dropdown-item" href="{{ route('admin.meetings.edit', ['meeting_id' => $meeting_details->id]) }}">
                                                        {{tr('edit')}}
                                                    </a>
                                                    
                                                    <a class="dropdown-item" href="{{route('admin.meetings.delete', ['meeting_id' => $meeting_details->id])}}" 
                                                    onclick="return confirm(&quot;{{tr('meeting_delete_confirmation' , $meeting_details->title)}}&quot;);">
                                                        {{tr('delete')}}
                                                    </a>
                                                @else

                                                    <a class="dropdown-item text-muted" href="javascript:;">{{tr('edit')}}</a>
                                                  
                                                    <a class="dropdown-item text-muted" href="javascript:;" onclick="return confirm(&quot;{{tr('meeting_delete_confirmation' , $meeting_details->title)}}&quot;);">{{tr('delete')}}</a>                           
                                                @endif

                                                <div class="dropdown-divider"></div>


                                                @if($meeting_details->status == APPROVED)

                                                    <a class="dropdown-item" href="{{ route('admin.meetings.status', ['meeting_id' => $meeting_details->id]) }}" onclick="return confirm(&quot;{{$meeting_details->title}} - {{tr('meeting_decline_confirmation')}}&quot;);" >
                                                        {{ tr('decline') }} 
                                                    </a>

                                                @else
                                                    
                                                    <a class="dropdown-item" href="{{ route('admin.meetings.status', ['meeting_id' => $meeting_details->id]) }}">
                                                        {{ tr('approve') }} 
                                                    </a>
                                                       
                                                @endif


                                            </div>

                                        </div>

                                    </div>

                                </td>

                            </tr>

                        @endforeach
                        
                    </tbody>
                   
                </table>

                <div class="pull-right">{{ $meetings->appends(request()->input())->links() }}</div>
                
            </div>
            
        </div>
       
    </div>
   
</div>


@endsection