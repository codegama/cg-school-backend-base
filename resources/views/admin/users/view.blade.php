@extends('layouts.admin') 

@section('content-header', tr('users'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.users.index')}}">{{tr('users')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_users') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('view_users')}}
                </h4>

            </div>

            <div class="card-body">

            	<div class="row">

	            	<div class="col-md-6">

					    <div class="card card-widget widget-user-2">

					        <div class="widget-user-header">
					            <div class="widget-user-image">
					                <img class="user-image" src="{{$user_details->picture ?: asset('placeholder.jpeg')}}" alt="User Avatar" />
					            </div>
					        </div>

					        @if(Setting::get('is_demo_control_enabled') == NO)

					        	<div class="row">

	                                <div class="col-md-6 align-left">
				                		<a class="btn btn-block btn-outline-secondary" href="{{ route('admin.users.edit', ['user_id' => $user_details->id]) }}">
	                                    {{tr('edit')}}
	                                	</a>
				                	</div>

	                                <div class="col-md-6 align-left">
		                                <a class="btn btn-block btn-outline-danger" href="{{route('admin.users.delete', ['user_id' => $user_details->id])}}" 
		                                onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">
		                                    {{tr('delete')}}
		                                </a>
		                            </div>

	                            </div>

                            @else
                        		<div class="row">
	                            	<div class="col-md-6 align-left">
				                		<a class="btn btn-block btn-outline-secondary" href="user_details:;">{{tr('edit')}}</a>
				                	</div>
				                	<div class="col-md-6 align-left">
				                    	<a class="btn btn-block btn-outline-danger" href="javascript:;" onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">{{tr('delete')}}</a>
				                    </div>
			                    </div>
                                                          
                            @endif

			                <div class="row">

					        	<div class="col-md-6 align-left">

					        		@if($user_details->status == APPROVED)

		                                <a class="btn btn-block btn-outline-danger" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}" onclick="return confirm(&quot;{{$user_details->name}} - {{tr('user_decline_confirmation')}}&quot;);" >
		                                    {{ tr('decline') }} 
		                                </a>

		                            @else
		                                
		                                <a class="btn btn-block btn-outline-success" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}">
		                                    {{ tr('approve') }} 
		                                </a>
		                                   
		                            @endif
			                	</div>

			                	<div class="col-md-6 align-left">
			                		 @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED) 

                                        <a class="dropdown-item" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> {{ tr('verify') }} 
                                        </a>

                                    @endif 
			                    </div>

			                </div>

			                <br>

					    </div>

					</div>

					<div class="col-md-6">
					    
					    <div class="card card-widget widget-user-2">

					        <div class="card-footer p-0">

					            <ul class="nav flex-column">

					            	<li class="nav-item">
					                    <div href="#" class="nav-link">{{tr('name')}} <span class="float-right text-uppercase">{{$user_details->name}}</span> </div>
					                </li>

					            	<li class="nav-item">
					                    <div href="#" class="nav-link">{{tr('email')}}<span class="float-right text-uppercase">{{$user_details->email}}</span> </div>
					                </li>

					                <li class="nav-item">
					                   <div class="nav-link">{{tr('mobile')}}<span class="float-right text-uppercase">{{$user_details->mobile ?: tr('unavailable')}}</span> </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link"> {{tr('payment_mode')}} <span class="float-right badge bg-secondary text-uppercase">{{$user_details->payment_mode}}</span></div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('is_verified')}} 
					                    	@if($user_details->is_verified == USER_EMAIL_VERIFIED)
					                    		<span class="float-right badge bg-primary text-uppercase">{{tr('yes')}}</span>
					                    	@else
					                    	 	<span class="float-right badge bg-danger text-uppercase">{{tr('no')}}</span>
					                    	@endif
					                    </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('status')}}
					                    	@if($user_details->status == APPROVED)
					                    		<span class="float-right badge bg-success text-uppercase">{{tr('approved')}}</span>
					                    	@else
					                    	 	<span class="float-right badge bg-danger text-uppercase">{{tr('declined')}}</span>
					                    	@endif 
					                    </div>
					                </li>

					                <li class="nav-item">
					                    <div href="#" class="nav-link">{{tr('device_type')}} <span class="float-right badge bg-info text-uppercase">{{$user_details->device_type}}</span> </div>
					                </li>

					                <li class="nav-item">
					                   <div class="nav-link">{{tr('login_by')}}<span class="float-right text-uppercase">{{$user_details->login_by}}</span></div>
					                </li>


					                <li class="nav-item">
					                    <div class="nav-link">{{tr('created_at')}}<span class="float-right">{{common_date($user_details->created_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('updated_at')}}<span class="float-right">{{common_date($user_details->updated_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					            </ul>

					        </div>

					    </div>
					   
					</div>

		        </div>

            </div>
            
        </div>
        
    </div>
    
</div>
@endsection