@extends('layouts.admin') 

@section('content-header', tr('users'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.users.index')}}">{{tr('users')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_users') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-header">

                <h4 class="text-uppercase">{{tr('view_users')}}

                    <a class="btn btn-outline-success pull-right" href="{{route('admin.users.create')}}">
                        <i class="fa fa-plus"></i> {{tr('add_user')}}
                    </a>
              
                </h4>

            </div>

            <div class="card-body">

                <form  action="{{route('admin.users.index')}}" method="GET" role="search">

                    <div class="row">

                        <div class="col-3">
                            @if(Request::has('search_key'))
                                <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
                            @endif
                        </div>
                        
                        <div class="col-3">

                            <select class="form-control select2" name="status">

                                <option  class="select-color" value="">{{tr('select_status')}}</option>

                                <option  class="select-color" value="{{SORT_BY_APPROVED}}">{{tr('approved')}}</option>

                                <option  class="select-color" value="{{SORT_BY_DECLINED}}">{{tr('declined')}}</option>

                                <option  class="select-color" value="{{SORT_BY_EMAIL_VERIFIED}}">{{tr('verified')}}</option>

                                <option  class="select-color" value="{{SORT_BY_EMAIL_NOT_VERIFIED}}">{{tr('un_verified')}}</option>

                            </select>

                        </div>

                        <div class="col-6">

                            <div class="input-group">
                                <input type="text" class="form-control" name="search_key"
                                    placeholder="{{tr('users_search_placeholder')}}"> <span class="input-group-btn">
                                    &nbsp

                                    <button type="submit" class="btn btn-default">
                                       <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>

                                    <a class="btn btn-default" href="{{route('admin.users.index')}}"><i class="fa fa-eraser" aria-hidden="true"></i>
                                    </a>
                                </span>
                            </div>

                        </div>

                    </div>

                </form>
                <table id="table_order" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('name')}}</th>
                            <th>{{tr('email')}}</th>
                            <th>{{tr('mobile')}}</th>   
                            <th>{{tr('status')}}</th>
                            <th>{{tr('verify')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $i => $user_details)
                              
                            <tr>
                                <td>{{$i+$users->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.users.view' , ['user_id' => $user_details->id])}}"> {{ $user_details->name }}
                                    </a>
                                </td>

                                <td> {{ $user_details->email }} </td>

                                <td> {{ $user_details->mobile ?: tr('unavailable')}} </td>

                                <td>

                                    @if($user_details->status == USER_APPROVED)

                                        <span class="badge bg-success">{{ tr('approved') }} </span>

                                    @else

                                        <span class="badge bg-danger">{{ tr('declined') }} </span>

                                    @endif

                                </td>

                                <td>   

                                    @if($user_details->is_verified == USER_EMAIL_VERIFIED) 

                                        <span class="badge bg-success">{{ tr('verified') }} </span>

                                    @else

                                        <a class="badge bg-info" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> 
                                            {{ tr('verify') }} 
                                        </a>

                                    @endif  
                                                                  
                                </td>

                                <td>     

                                    <div class="template-demo">

                                        <div class="dropdown">

                                            <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{tr('action')}}
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                              
                                                <a class="dropdown-item" href="{{ route('admin.users.view', ['user_id' => $user_details->id]) }}">
                                                    {{tr('view')}}
                                                </a>
                                                
                                                @if(Setting::get('is_demo_control_enabled') == NO)
                                                    <a class="dropdown-item" href="{{ route('admin.users.edit', ['user_id' => $user_details->id]) }}">
                                                        {{tr('edit')}}
                                                    </a>
                                                    
                                                    <a class="dropdown-item" href="{{route('admin.users.delete', ['user_id' => $user_details->id])}}" 
                                                    onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">
                                                        {{tr('delete')}}
                                                    </a>
                                                @else

                                                    <a class="dropdown-item text-muted" href="javascript:;">{{tr('edit')}}</a>
                                                  
                                                    <a class="dropdown-item text-muted" href="javascript:;">{{tr('delete')}}</a>                           
                                                @endif

                                                <div class="dropdown-divider"></div>

                                                @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED) 

                                                    <a class="dropdown-item" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> {{ tr('verify') }} 
                                                    </a>

                                                @endif 

                                                @if($user_details->status == USER_APPROVED)

                                                    <a class="dropdown-item" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}" onclick="return confirm(&quot;{{$user_details->first_name}} - {{tr('user_decline_confirmation')}}&quot;);" >
                                                        {{ tr('decline') }} 
                                                    </a>

                                                @else
                                                    
                                                    <a class="dropdown-item" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}">
                                                        {{ tr('approve') }} 
                                                    </a>
                                                       
                                                @endif


                                            </div>

                                        </div>

                                    </div>

                                </td>

                            </tr>

                        @endforeach
                    </tbody>
                   
                </table>

                <div class="pull-right" id="paglink">{{ $users->appends(request()->input())->links() }}</div>

            </div>
            
        </div>
       
    </div>
   
</div>


@endsection