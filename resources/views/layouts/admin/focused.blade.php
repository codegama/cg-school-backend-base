<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{Setting::get('favicon')}}">

    <title>{{Setting::get('site_name')}}</title>

    <link rel="canonical" href="https://www.wrappixel.com/templates/adminpro/" />

    <!-- Bootstrap Core CSS -->
    <link href="https://www.wrappixel.com/demos/admin-templates/admin-pro/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- page css -->
    <link href="{{asset('admin-assets/css/pages/login-register-lock.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('admin-assets/css/style.css')}}" rel="stylesheet">
   
</head>

<body class="card-no-border">
   
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">{{Setting::get('site_name')}}</p>
        </div>
    </div>

    @yield('content')
    
    @include('layouts.admin.scripts')
    
</body>

</html>