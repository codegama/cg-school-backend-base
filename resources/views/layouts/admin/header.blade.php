<nav class="main-header navbar navbar-expand navbar-dark navbar-navy">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <img  class="user-image-circle" src="{{ Auth::guard('admin')->user() ? Auth::guard('admin')->user()->picture : asset('placeholder.png') }}" />
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div>
                     <a href="{{route('admin.profile')}}" class="dropdown-item pull-left">{{tr('profile')}}</a>
                </div>
                <div class="dropdown-divider"></div>
                <div>
                     <a class="dropdown-item pull-left" data-toggle="modal" data-target="#logoutModel"> {{tr('logout')}}</a>
                </div>
            </div>
        </li>
    </ul>
</nav>
