
<aside class="main-sidebar elevation-4 sidebar-light-navy">

    <a href="{{route('admin.dashboard')}}" class="brand-link navbar-navy">
        <span class="brand-text font-weight-light text-white">{{Setting::get('site_name')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{Auth::guard('admin')->user()->picture ?: asset('placeholder.jpeg')}}" class="img-circle elevation-2" alt="User Image" />
            </div>
            <div class="info">
                <a href="" class="d-block">{{Auth::guard('admin')->user()->name ?? "-"}}</a>
            </div>
        </div>
    
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{route('admin.dashboard')}}" class="nav-link" id="dashboard">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{tr('dashboard')}}
                        </p>
                    </a>
                </li>

                <li class="nav-header sidebar-header">{{tr('account_management')}}</li>
                
                <li class="nav-item has-treeview" id="users-crud">
                    <a href="#" class="nav-link" id="users">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            {{tr('users')}}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" >
                        <li class="nav-item">
                            <a href="{{route('admin.users.create')}}" class="nav-link" id="users-create">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('add_user')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.users.index')}}" class="nav-link" id="users-view">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('view_users')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview" id="instructors-crud">
                    <a href="#" class="nav-link" id="instructors">
                        <i class="fas fa-chalkboard-teacher"></i>
                        <p>
                            {{tr('instructors')}}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" >
                        <li class="nav-item">
                            <a href="{{route('admin.instructors.create')}}" class="nav-link" id="instructors-create">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('add_instructor')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.instructors.index')}}" class="nav-link" id="instructors-view">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('view_instructors')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                
                <li class="nav-header sidebar-header text-uppercase">Meeting Management</li>

                <!-- Room CRUD -->

                <li class="nav-item has-treeview" id="rooms-crud">
                    <a href="#" class="nav-link" id="rooms">
                        <i class="fas fa-home"></i>
                        <p>
                            {{tr('rooms')}}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" >
                        <li class="nav-item">
                            <a href="{{route('admin.rooms.create')}}" class="nav-link" id="rooms-create">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('add_room')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.rooms.index')}}" class="nav-link" id="rooms-view">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('view_rooms')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview" id="meetings-crud">
                    <a href="#" class="nav-link" id="meetings">
                       <i class="fas fa-handshake"></i>
                        <p>
                            {{tr('meetings')}}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" >
                        <li class="nav-item">
                            <a href="{{route('admin.meetings.create')}}" class="nav-link" id="meetings-create">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('add_meeting')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.meetings.index')}}" class="nav-link" id="meetings-view">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('view_meetings')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                
                <li class="nav-header sidebar-header">{{tr('revenue_management')}}</li>

                <li class="nav-item has-treeview" id="subscriptions-crud">
                    <a href="#" class="nav-link" id="subscriptions">
                      <i class="fas fa-receipt"></i>
                        <p>
                            {{tr('subscriptions')}}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" >
                        <li class="nav-item">
                            <a href="{{route('admin.subscriptions.create')}}" class="nav-link" id="subscriptions-create">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('add_subscription')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.subscriptions.index')}}" class="nav-link" id="subscriptions-view">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('view_subscriptions')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview" id="payments-crud">
                    <a href="#" class="nav-link" id="payments">
                      <i class="fab fa-paypal"></i>
                        <p>
                            {{tr('payments')}}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" >
                        <li class="nav-item">
                            <a href="{{route('admin.revenue.dashboard')}}" class="nav-link" id="revenues-dashboard">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('revenue_dashboard')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.subscription_payments.index')}}" class="nav-link" id="subscription-payments">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{tr('subscription_payments')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header sidebar-header">{{tr('setting_management')}}</li>

                <li class="nav-item ">
                    <a href="{{route('admin.settings')}}" class="nav-link" id= "settings">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>
                            {{tr('settings')}}
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.profile')}}" class="nav-link"  id="profile">
                        <i class="nav-icon fas fa-user-cog"></i>
                        <p>
                            {{tr('profile')}}
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#logoutModel"> 
                        <i class="fa fa-share" aria-hidden="true"></i>
                        <p>
                            {{tr('logout')}}
                        </p>
                    </a>
                </li>
                
            </ul>
        </nav>
        
    </div>
   
</aside>
