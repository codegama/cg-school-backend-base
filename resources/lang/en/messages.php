<?php
return [
	'dashboard' =>'Dashboard',
	'login_success' => 'You have successfully login!!',
	'email' => 'Email',
	'password' => 'Password',
	'sign_in' =>'sign in',
	'enter_email' => 'Enter email',
	'enter_password' => 'Enter Password',

	//users messages
	'users' => 'Users',
	'add_user' => 'Add User',
	'view_users' => 'View Users',
	'edit_user' => 'Edit User',
	'all_right_reserved' => 'All rights are reserved',

	'settings' =>'Settings',
	'profile' => 'Profile',
	'is_verified' => 'Is Verified',
	/*********** FORM FIELDS ************/

	'submit' => 'Submit',
	'reset' => 'Reset',
	'close' => 'Close',

	'active' => 'Active',
	'inactive' => 'Inactive',
	'activate' => 'Activate',
	'inactivate' => 'In Activate',

	'id'	=>	'ID',
	'email' => 'Email',
	'email_address' => 'Email Address',
	'password' =>  'Password',
	'confirm_password' => 'Confirm Password',
	'new_password' => 'New Password',
	'old_password' => 'Old Password',

	'first_name' => 'First Name',
	'last_name' => 'Last Name',
	'username' => 'User Name',

	'mobile' => 'Mobile',
	'picture' => 'Picture',
	'about' => 'About Me',
	'address' => 'Address',
	'description' => 'Description',
	'name' => 'Name',
	'title' => 'Title',
	'type' => 'Type',
	'gender' =>'Gender',
	'remove' => 'Remove',
	'amount'	=> 	'Amount',
	'frontend_url' => 'Frontend Url',
	'currency' 	=>	'Currency',
	'total_amount_spent' => 'Total Amount Spent',
	/*********** FORM FIELDS ************/
	

	/*********** COMMON CONTENT *******/

	'select_picture' => 'Selecte Picture',
	'member_since' => "Since ",

	'version' => 'Version',
	'copyright' => 'Copyright',
	'all_right_reserved' => 'All right reserved',
	'analytics' => 'Analytics',
	'core' => 'Core',

	'approve'	=>	'Approve',
	'decline'	=>	'Decline',
	'approved'	=>	'Approved',
	'pending'	=>	'Pending',
	'added'	=> 	'Added',

	'YES' => 'Yes',
	'NO' => 'No',

	'user' => 'User',
	'admin' => 'Admin',
	'user_welcome_title' => 'Welcome to ',
	'login_content' => 'Login to your account',
	'images_not_found' => 'Images Not Available. You can',

	'provider_welcome_title' => 'Welcome to ',

	'forgot_email_title' => 'Your New password',

	'login_type' => 'Login Type',

	'action' => 'Actions',

	'edit' => 'Edit',

	'status' => 'Status',

	'joined' => 'Joined At',

	's_no' => 'S No',

	'delete' => 'Delete',

	'approve' => 'Approve',

	'decline' => 'Decline',

	'approved' => 'Approved',

	'pending' => 'Pending',

	'declined' => 'Declined',

	'verify' => 'Verify',

	'verified' => 'Verified',

	'verified_status' => 'Verified Status',

	'unverify' => 'Unverify',

	'help' => 'Help',
	'faq' => 'FAQ',

	'settings' => 'Settings',
	'profile' => 'Profile',
	'dashboard' => 'Dashboard',
	'revenue' => 'Revenue',
	'revenues' => 'Revenues',
	'login' => 'Login',
	'signup' => 'Signup',
	'account' => 'Account',
	'edit_profile' => 'Edit Profile',
	'change_password' => 'Change Password',
	'delete_account' => 'Delete Account',
	'logout' => 'Logout',
	'hello_text' => 'Hello! let\'s get started',
	'provider_redeems' => 'Provider Redeems',
	'user_refunds' => 'User Refunds',

	'device_type' => 'Device Type',
	'login_by' => 'Login By',
	'register_type' => 'Register Type',
	'payment_mode' => 'Payment Mode',
	'timezone' => 'Timezone',
	'latitude' => 'Latitude',
	'longitude' => 'Longitude',
	'push_notification' => 'Mobile Notification',
	'email_notification' => 'Email Notification',
	'is_email_verified' => 'Is Email Verified?',
	'notification_settings' => 'Notification Settings',

	'properties' => 'Properties',
	'amenties_and_bookings' => 'Amenities & Booking Settings',
	'history' => 'History',
	'host_reviews' =>'Host Reviews',
	'booking_preference'=> 'Booking Preference',
	'check_in_time' =>'Check In',
	'check_out_time' =>'Check Out',

	'home' => 'Home',

	'on' => 'ON',
	'off' => 'OFF',
	'no' => 'NO',
	'yes' => 'YES',

	'no_reviews_found' => 'No Reviews Found!!',
	'add_images' => 'Add Gallery Images.',

	'created' => 'Created',
	'updated_on' => 'Updated On',

	'created_at' => 'Created Time',
	'updated_at' => 'Updated Time',

	'file' =>'File',

	'update_profile' => 'Update Profile',
	'change_password' => 'Change Password',

	'view' => 'View',
	'view_all' => 'View All',
	'success' => 'Success!!',

	'total_amount' => 'Total Amount',
	'revenues' => 'Revenues',
	'added_on' => 'Added On',

    'admin_not_error'	=>	'Something Went Wrong, Try Again!',

    'paid' => 'Paid',
    'subscription_amount' => 'Subscription Amount',
	'paid_amount' => 'Paid Amount',
	'admin_amount' => 'Admin Amount',
	'provider_amount' => 'Provider Amount',
	'paid_date' => 'Paid At',
	'sub_total' => 'Sub Total',
	'actual_total' => 'Actual Total',
	'coupon_amount' => 'Coupon Discount',
	'tax_price' =>'Tax Price',
	'details' => 'Details',
	'invoice' => 'Invoice',
	'no_of_months' => 'No Of Months',
	'no_of_class' => 'No Of Class',
	'no_of_users_each_class' => 'No Of Users Each Class',
	'no_of_user' => 'No Of User',
	'is_free' => 'Is Free?',
	'is_popular' => 'Is Popular?',

	'location_details' => 'Location Details',
	'space_details_text' => 'Space Details',
	'access_method_key' => 'Key',
	'secret_code' => 'Secret code',
	'images' => 'Image',

	'remaining_amount' => 'Remaining Amount',
	'remaining' => 'Remaining',

	'upload' => 'Upload',
	'upload_image' => 'Upload Image',
	'mobile_note' => 'Note : The mobile must be between 6 and 13 digits.',
	'png_image_note' => 'Please enter .png images only.',
	'description' => 'Description',

	'invalid_request_input' => 'Invalid request details',

	'paypal_email' => 'Paypal Email',
	'account_name' => 'Account Name',
	'account_no' => 'Account Number',
	'route_no' => 'Route Number',

	/*********** COMMON CONTENT *******/

	
	// Settings 

	'settings_update_success' => 'Settings Updated successfully!!',
	
	'booking_settings' => 'Booking Settings',

	'settings' => 'Settings',
	'site_settings' => 'Site Settings',
	'email_settings' => 'Email Settings',
	'payment_settings' => 'Payment Settings',
	'mobile_settings' => 'Mobile Settings',
	'social_settings' => 'Social Settings',
	'social_login'=>'Social Login',
	'social_and_app_settings' => 'Social & App Settings',
	'social_settings' => 'Social Links',
	'other_settings' => 'Others',
	'push_notification_settings' => 'Push Notification Settings',
	'mail_push_settings' => 'Email & Push Settings',
	
	'payment_settings' => 'Payment Settings',
	

	'stripe_settings' => 'Stripe Settings',

	'site_name' => 'Site Name',
	'site_logo' => 'Site Logo',
	'site_icon' => 'Favicon',
	'tag_name' => 'Tag Name',

	'MAIL_DRIVER' => 'MAIL DRIVER',
	'MAIL_HOST' => 'MAIL HOST',
	'MAIL_PORT' => 'MAIL PORT',
	'MAIL_USERNAME' => 'MAIL USERNAME',
	'MAIL_PASSWORD' => 'MAIL PASSWORD',
	'MAIL_ENCRYPTION' => 'MAIL ENCRYPTION',
	'MAIL_FROM_ADDRESS' => 'MAIL FORM ADDRESS',
	'MAIL_FROM_NAME' => 'MAIL FORM NAME',

	'mail_driver_note' => 'Supported - "smtp", "mailgun"',
	'mail_host_note' => 'Ex- "smtp.gmail.com", "smtp.mailgun.org"',
	'mail_port_note' => 'Ex- 587,445',
	'mail_username_note' => 'Ex- "abcd.gmail.com"',
	'mail_password_note' => '',
	'mail_encryption_note' => 'Ex- "tls"',
	'MAIL_FROM_ADDRESS_note' => 'Ex- "no-reply@gmail.com"',
	'MAIL_FROM_NAME_note' => 'Ex- "Site Name"',

	'FCM_SERVER_KEY' => 'FCM SERVER KEY',
	'FCM_SENDER_ID' => 'FCM SENDER ID',
	'FCM_PROTOCOL' => 'FCM PROTOCOL',

	'stripe_publishable_key' => 'Publishable Key',
	'stripe_secret_key' => 'Secret Key',
	'stripe_mode' => 'Mode',
	'live' => 'Live',
	'sandbox' => 'Sandbox',
	'payment' => 'Payment',


	'icon_for_provider' => 'Provider Icon',
	'icon_for_provider_note' => 'Provider Icon',
	'icon_for_user' => 'User Icon',
	'icon_for_user_note' => 'User Icon',
	'icon_for_destination' => 'Destination Icon',
	'icon_for_destination_note' => 'Destination Icon',

	'booking_admin_commission' => 'Admin Commission in %',
	'booking_admin_commission_note' => 'Commission in % like 10, 21, etc',
	'tax_percentage' => 'Tax in % (18%)',
	'tax_percentage_note' => 'Tax in %',

	'tax_percentage_note' => '',
	'base_price_distance_limit' => 'Distance limit for base price',

	'base_price' => 'Base Price',
	'per_base_price' => 'Per Base Price',
	'settings_per_price' => 'Per Price',
	'settings_per_price_note' => 'Per Price By Distance. (Ex. 1KM = $0.5)',

	'base_price_note' => 'Base Price. (Ex.Min:1)',
	'base_price_distance_limit_note' => 'Set distance for base price.(upto 5km - $10)',

	'provider_select_timeout' => 'Provider Request Timeout',
	'provider_select_timeout_note' => 'Provider Timeout for new request in secs',


	'google_api_key' => 'Google API KEY',
	'push_notification_browser_key' => 'Browser key for push Notification',
	'socket_url' => 'Chat Socket url',

	'socket_url' => 'Chat Socket url',
	
	'apps_settings' => 'Apps Settings',
	'playstore_user' => 'User - PlayStore',
	'appstore_user' => 'User - Appstore',
	'playstore_provider' => 'Provider - PlayStore',
	'appstore_provider' => 'Provider - Appstore',

	'facebook_link' => 'Facebook Link',
	'linkedin_link' => 'LinkedIn Link',
	'twitter_link' => 'Twitter Link',
	'google_plus_link' => 'Google Plus Link',
	'pinterest_link' => 'Pinterest Link',

	'google_analytics' => 'Google Analytics',
	'body_scripts' => 'Body Scripts' ,
	'header_scripts' => 'Header Scripts',

	// SEO Settings Part

	'seo_settings' => 'SEO Settings',
	'meta_title' => 'Meta Title',
	'meta_keywords' => 'Meta Keywords',
	'meta_author' => 'Meta Author',
	'meta_description' => 'Meta Description',

	'social_settings' => 'Social Settings',
	'fb_settings' => 'FB Settings',
	'twitter_settings' => 'Twitter Settings',
	'google_settings' => 'Google Settings',

	'FB_CLIENT_ID' => 'FB Client Id',
	'FB_CLIENT_SECRET' => 'FB Client Secret',
	'FB_CALL_BACK' => 'FB CallBack',

	'TWITTER_CLIENT_ID' => 'Twitter Client Id',
	'TWITTER_CLIENT_SECRET' => 'Twitter Client Secret',
	'TWITTER_CALL_BACK' => 'Twitter CallBack',

	'GOOGLE_CLIENT_ID' => 'Google Client Id',
	'GOOGLE_CLIENT_SECRET' => 'Google Client Secret',
	'GOOGLE_CALL_BACK' => 'Google CallBack',

	'PAYPAL_ID' => 'Paypal Id',
	'PAYPAL_SECRET' => 'Paypal Secret',
	'PAYPAL_MODE' => 'Paypal Mode',

	'instructors' => 'Instructors',
	'view_instructors' => 'View Instructors',
	'add_instructor' => 'Add Instructor',
	'edit_instructor' => 'Edit Instructor',

	'subscriptions' => 'Subscriptions',
	'view_subscriptions' => 'View Subscriptions',
	'add_subscription' => 'Add Subscription',
	'edit_subscription' => 'Edit Subscription',


	'account_management' => 'ACCOUNT MANAGEMENT',
	'revenue_management' => 'REVENUE MANAGEMENT',
	'setting_management' => 'SETTING MANAGEMENT',

	//users
	'delete_user'	=>	'Delete User',
	
	'user_created_success' => 'The user created successfully!!',
	'user_updated_success' => 'The user updated successfully!!',
	'user_save_failed' => 'The user details updating failed!!',
	'user_not_found' => 'The selected user details not found.',

	'user_deleted_success' => 'The user deleted successfully!!',
	'user_delete_failed' => 'The user deletion failed!!',
	'user_delete_confirmation' => 'Once you\'ve deleted the record , the user (:other_key) will no longer be able to log in to the site or apps. This action cannot be undo.',

	'user_approve_success' => 'The user approved successfully..!!',
	'user_decline_success' => 'The user declined successfully..!!',
	'user_decline_confirmation' => 'Do you want decline this user?',
	'user_status_change_failed' => 'The user status updating failed..!!',

	'user_verify_success' => 'The user email verification completed!',
	'user_unverify_success' => 'The user email verification removed!',
	'user_verify_change_failed' => 'Updating user email verification status failed..!!',

	'user_email_confirmation' => 'Do you want change this user as email verified user?',
	'amenities_updated_success' => 'Updated Amenities for the selected Host.',
	'user_account_name_not_avail'=>'User account name is not available',
	'user_account_no_not_avail'=>'User account number is not available',
	'user_route_no_not_avail'=>'User route number is not available',
	'user_paypal_email_not_avail'=>'User paypal email is not available',

	//instructors

	'delete_instructor'	=>	'Delete User',
	
	'instructor_created_success' => 'The instructor created successfully!!',
	'instructor_updated_success' => 'The instructor updated successfully!!',
	'instructor_save_failed' => 'The instructor details updating failed!!',
	'instructor_not_found' => 'The selected instructor details not found.',

	'instructor_deleted_success' => 'The instructor deleted successfully!!',
	'instructor_delete_failed' => 'The instructor deletion failed!!',
	'instructor_delete_confirmation' => 'Once you\'ve deleted the record , the instructor (:other_key) will no longer be able to log in to the site or apps. This action cannot be undo.',

	'instructor_approve_success' => 'The instructor approved successfully..!!',
	'instructor_decline_success' => 'The instructor declined successfully..!!',
	'instructor_decline_confirmation' => 'Do you want decline this instructor?',
	'instructor_status_change_failed' => 'The instructor status updating failed..!!',

	'instructor_verify_success' => 'The instructor email verification completed!',
	'instructor_unverify_success' => 'The instructor email verification removed!',
	'instructor_verify_change_failed' => 'Updating instructor email verification status failed..!!',

	'instructor_email_confirmation' => 'Do you want change this instructor as email verified instructor?',

	//subscription 

	'subscription_create_success' => 'The subscription created successfully!!',
	'subscription_update_success' => 'The subscription updated successfully!!',
	'subscription_save_failed' => 'The subscription details updating failed!!',
	'subscription_not_found' => 'The selected subscription details not found.',

	'subscription_deleted_success' => 'The subscription deleted successfully!!',
	'subscription_delete_failed' => 'The subscription deletion failed!!',
	'subscription_delete_confirmation' => 'Once you\'ve deleted the record , the subscription (:other_key) will no longer be able to log in to the site or apps. This action cannot be undo.',

	'subscription_approve_success' => 'The subscription approved successfully..!!',
	'subscription_decline_success' => 'The subscription declined successfully..!!',
	'subscription_decline_confirmation' => 'Do you want decline this subscription?',
	'subscription_status_change_failed' => 'The subscription status updating failed..!!',
	'subscription_details' => 'Subscription details',
	'admin_profile_success' => 'Profile updated successfully',
	'subscription_payments' => 'Subscription Payments',
	'revenue_dashboard' => 'Revenue Dashboard',

	'rooms' => 'Rooms',
	'add_room' => 'Add Room',
	'edit_room' => 'Edit Room',
	'view_rooms' => 'View Rooms',
	'room_create_success' => 'The room created successfully!!',
	'room_update_success' => 'The room updated successfully!!',
	'room_save_failed' => 'The room details updating failed!!',
	'room_not_found' => 'The selected room details not found.',

	'room_deleted_success' => 'The room deleted successfully!!',
	'room_delete_failed' => 'The room deletion failed!!',
	'room_delete_confirmation' => 'Once you\'ve deleted the record , the room (:other_key) will no longer be able to log in to the site or apps. This action cannot be undo.',

	'room_approve_success' => 'The room approved successfully..!!',
	'room_decline_success' => 'The room declined successfully..!!',
	'room_decline_confirmation' => 'Do you want decline this room?',
	'room_status_change_failed' => 'The room status updating failed..!!',

	'meetings' =>'Meetings',
	'payments' => 'Payments',
	'total_earnings' => 'Total Earnings',
	'today_earnings' => 'Today Earnings',
	'total_subscribers' => 'Total Subscribers',
	'today_subscribers' => 'Today Subscribers',
	'more_info' => 'More Info',
	'subscription' => 'Subscription',
	'payment_id' => 'Payment Id',
	'subscription_payment_not_found' => 'subscription payment details not found',
	'expiry_date' => 'Expiry Date',
	'is_current_subscription' => 'Is Current Subscription',
	'is_cancelled' => 'Is Cancelled',
	'cancel_reason' => 'Cancel Reason',
	'unique_id' => 'Unique Id', 
	'last_week_revenues' => 'Last Week Revenues',
	'select_instructor' => 'Select Instructor',
	'instructor' => 'Instructor',

	'add_meeting' => 'Add Meeting',
	'view_meetings' => 'View Meetings',
	'edit_meetings' => 'Edit Meetings',	

	'meeting_create_success' => 'The meeting created successfully!!',
	'meeting_update_success' => 'The meeting updated successfully!!',
	'meeting_save_failed' => 'The meeting details updating failed!!',
	'meeting_not_found' => 'The selected meeting details not found.',

	'meeting_deleted_success' => 'The meeting deleted successfully!!',
	'meeting_delete_failed' => 'The meeting deletion failed!!',
	'meeting_delete_confirmation' => 'Once you\'ve deleted the record , the meeting (:other_key) will no longer be able to log in to the site or apps. This action cannot be undo.',

	'meeting_approve_success' => 'The meeting approved successfully..!!',
	'meeting_decline_success' => 'The meeting declined successfully..!!',
	'meeting_decline_confirmation' => 'Do you want decline this meeting?',
	'meeting_status_change_failed' => 'The meeting status updating failed..!!',

	'start_time' => 'Start Time',
	'end_time' => 'End Time',
	'schedule_time' => 'Schedule Time',
	'select_room' => 'Select Room',
	'meeting_timings' => 'Meeting Timings',
	'room' => 'Room',
	'created_by' =>'Created By',
	'fcm_sender_id' => 'Fcm Sender Id',
	'fcm_server_key' => 'Fcm Server Key',
	'total_users' => 'Total Users',
	'total_instructors' => 'Total Instructor',
	'total_revenue' => 'Total Revenue',
	'today_revenue' => 'Today Revenue',

	'subscribers' => 'Subscribers',
	'select_plan_type' => 'Select Plan Type',
	'plan_type' => 'Plan Type',	
	'plan' => 'Plan',
	'add_sub_instructor' => 'Add Sub Instructor',
	'sub_instructors' => 'Sub Instructors',
	'assign_subinstructor' =>'Assign Sub Instructor',
	'select_subinstructor' => 'Select Sub instructor',
	'sub_instructor_already_present' =>'Sub instructor already present',
	'instructor_assigned_successfully' => 'Instructor assigned successfully',
	'room_title' => 'Room title',
	'add_user_for_meeting' =>'Add users for meeting',
	'assign_users' => 'Assign Users',
	'select_users' => 'Select users',
	'room_user_assigned_successfully' => 'Room user assigned successfully',
	'latest_members' => 'Latest Members',
	'sub_instructor_not_found' => 'Sub instructor not found',
	'sub_instructor_deleted_success' => 'Sub instructor deleted successfully!!',
	'sub_instructor_delete_failed' => 'Sub instructor deleted failed',
	'rooms_user_details_not_found' => 'Rooms user details not found',
	'rooms_user_deleted_success' => 'Rooms user deleted successfully!!',
	'rooms_user_delete_failed' => 'Rooms user failed to delete',
	'subscribe' => 'Subscribe',
	'you_are_already_member_of_this_subscription' => 'You are already member of this subscription',
	'subscription_payment_success' => 'Subscription payment completed successfully',
	'subscription_payment_failed' => 'Subscription payment failed try again!!',
	'subscription_history' => 'Subscription history',
	'subscription_history_delete_confirmation' =>'Do you want to delete subscription history?',
	'subscription_payment_details_not_found' => 'Subscription payment details not found',
	'subscription_payment_deleted_success' => 'Subscription payment deleted successfully!!',
	'subscription_payment_failed_to_delete' => 'Failed to delete subscription payment history',
	'today_subscriptions' => 'Today Subscriptions',
	'currently_on_live' => 'Currently On Live',
	'completed_meetings' => 'Completed Meetings',
	'todays_meetings' => 'Todays Meetings',
	'scheduled_meetings' => 'Scheduled Meetings',
	'total_meetings' => 'Total Meetings',
	'today_scheduled_meetings' => 'Today Scheduled Meetings',
	'overall_subscription' => 'Overall Subscription',
	'settings_key_not_found' => 'Settings key not found',
	'settings_save_error' => 'Setting details failed to save try again!!',
	'months' => 'Months',
	'month' => 'Month',
	'days' => 'Days',
	'day' => 'Day',
	'weeks' => 'Weeks',
	'logout_success' => 'You have successfully logout!!',
	'username_password_not_match' => 'Sorry, the username or password you entered do not match. Please try again',
	
	'week' => 'Week',
	'class' => 'Class',
	'on_live_no_results' => 'No live classes now',
	'today_scheduled_meetings_no_results' => 'No meetings scheduled today',
	'sub_instructor_not_found' => 'No sub instructor available',
	'subscription_payments_search_placeholder' => 'Search by Username,Payment Id',
	'meetings_search_placeholder' => 'Search by Title,Instructor Name,Mobile',
	'users_search_placeholder' => 'Search by  Username,Email Id,Mobile',
	'rooms_search_placeholder'  => 'Search by Title,Instructor Name,Mobile',
	'instructors_search_placeholder' => 'Search by Username,Mobile,Email Id',
	'edit_meeting' => 'Edit Meeting',
	'confirm_logout' => 'Confirm Logout',
	'logout_note' => 'Do you want logout from this session?',
	'mob' => 'Mob',
	'password_change_success' => 'Password has been changed successfully, Login with new password',
	'subscriber_name' => 'Subscriber Name',
	'verify_success' => 'You have successfully verified an email address',
	'welcome_title' => 'Welcome To',
	'instructor_welcome_description' => 'We re really happy to have you! Click the link below to verify your account:',
	'welcome_to' => 'Welcome to',
	'hello' => 'Hello',
	'thanks_for_signup' => 'Thank you for signing up for',
	'verify_now' => 'Verify Now',
	'forgot_password_description' => 'It seems that you’ve forgotten your password.',
	'oops' => 'Oops!',
	'your_new_password' => 'Your new password',
	'ignore_forgot_password' => 'If you did not make this request, just ignore this email.',
	'instructor_forgot_email_title' => 'Your new password',
	'password_changed_discription' => 'You have succesfully changed your password, visit website for more info',
	'visit_website' => 'Visit Website',
	'instructor_change_password_title' => 'Password change',
	'change_password_email_title' =>'Change Password',
	'visit_website_description' => 'Remember',
	'un_verified' => 'Unverified',
	'latest_room_users' => 'Latest room users',
	'room_users_not_found' => 'Room users are not added',
	'select_status' => 'Select Status',
	'unavailable' => 'N/A',
	'do_you_want_to_subscribe_this_plan' => 'Do you want to subscribe this plan?',

] 

?>