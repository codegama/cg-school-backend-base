<?php

return [

    101 => 'Successfully loggedin!!',

    102 => 'Mail sent successfully',

    103 => 'Your account deleted.',

    104 => 'Your password changed.',

    106 => 'Logged out',

    105 => 'The card added to your account.',

    108 => 'The card changed to default.',

    109 => 'The card deleted',

    110 => 'The card marked as default.',

    111 => 'The profile updated',

    112 => 'The billing account added',

    113 => 'The billing account deleted',

    114 => 'The document uploaded and waiting for approval',

    115 => 'The uploaded document deleted',

    116 => 'The uploaded documents are deleted',

    117 => 'The sub instructor added',

    118 => 'The sub instructor removed',

    119 => 'The room added',

    120 => 'The room deleted',

    121 => 'The user added to rooms',
    
    122 => 'The user removed from rooms',

    123 => 'The payment done successfully',

];
