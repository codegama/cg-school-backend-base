<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});

Route::get('/email/verify', 'ApplicationController@email_verify')->name('email.verify');

Route::group(['prefix' => 'instructor' , 'middleware' => 'cors'], function() {

    Route::any('pages_list' , 'ApplicationController@static_pages_api');

    Route::get('get_settings_json', function () {

        if(\File::isDirectory(public_path(SETTINGS_JSON))){

        } else {

            \File::makeDirectory(public_path('default-json'), 0777, true, true);

            \App\Helpers\Helper::settings_generate_json();
        }

        $jsonString = file_get_contents(public_path(SETTINGS_JSON));

        $data = json_decode($jsonString, true);

        return $data;
    
    });

	/***
	 *
	 * User Account releated routs
	 *
	 */

    Route::post('register','InstructorApi\AccountApiController@register');
    
    Route::post('login','InstructorApi\AccountApiController@login');

    Route::post('forgot_password', 'InstructorApi\AccountApiController@forgot_password');

    Route::group(['middleware' => 'InstructorApiVal'] , function() {

        Route::post('profile','InstructorApi\AccountApiController@profile'); // 1

        Route::post('update_profile', 'InstructorApi\AccountApiController@update_profile'); // 2

        Route::post('change_password', 'InstructorApi\AccountApiController@change_password'); // 3

        Route::post('delete_account', 'InstructorApi\AccountApiController@delete_account'); // 4

        Route::post('logout', 'InstructorApi\AccountApiController@logout'); // 7

        Route::post('push_notification_update', 'InstructorApi\AccountApiController@push_notification_status_change');  // 5

        Route::post('email_notification_update', 'InstructorApi\AccountApiController@email_notification_status_change'); // 6

    });

    // Route::post('project/configurations', 'UserApiController@configurations'); 

    Route::group(['middleware' => 'InstructorApiVal'] , function() {

        Route::post('kyc_documents_list', 'InstructorApi\VerificationApiController@kyc_documents_list');

        Route::post('kyc_documents_save','InstructorApi\VerificationApiController@kyc_documents_save');

        Route::post('kyc_documents_delete','InstructorApi\VerificationApiController@kyc_documents_delete');

        Route::post('kyc_documents_delete_all','InstructorApi\VerificationApiController@kyc_documents_delete_all');

        Route::post('kyc_status_user','InstructorApi\VerificationApiController@kyc_status_user');

        Route::post('users_accounts_list','InstructorApi\VerificationApiController@users_accounts_list');

        Route::post('users_accounts_save','InstructorApi\VerificationApiController@users_accounts_save');

        Route::post('users_accounts_delete','InstructorApi\VerificationApiController@users_accounts_delete');

    });

    // Cards management start

    Route::group(['middleware' => 'InstructorApiVal'] , function() {

        Route::post('cards_add', 'InstructorApi\AccountApiController@cards_add'); // 15

        Route::post('cards_list', 'InstructorApi\AccountApiController@cards_list'); // 16

        Route::post('cards_delete', 'InstructorApi\AccountApiController@cards_delete'); // 17

        Route::post('cards_default', 'InstructorApi\AccountApiController@cards_default'); // 18

        Route::post('payment_mode_default', 'InstructorApi\AccountApiController@payment_mode_default');

    });

    // Subscriptions management start

    Route::group(['middleware' => 'InstructorApiVal'] , function() {

        Route::post('subscriptions_index', 'InstructorApi\AccountApiController@subscriptions_index');

        Route::post('subscriptions_view', 'InstructorApi\AccountApiController@subscriptions_view');

        Route::post('subscriptions_payment_by_stripe', 'InstructorApi\AccountApiController@subscriptions_payment_by_stripe');

        Route::post('subscriptions_history', 'InstructorApi\AccountApiController@subscriptions_history');

        Route::post('subscriptions_current_plan', 'InstructorApi\AccountApiController@subscriptions_current_plan');

    });

    // Subscriptions management end

    // sub_instructors management start

    Route::group(['middleware' => 'InstructorApiVal'] , function() {

        Route::post('sub_instructors_index', 'InstructorApi\SubInstructorApiController@sub_instructors_index');

        Route::post('sub_instructors_view', 'InstructorApi\SubInstructorApiController@sub_instructors_view');

        Route::post('sub_instructors_search', 'InstructorApi\SubInstructorApiController@sub_instructors_search');

        Route::post('sub_instructors_add', 'InstructorApi\SubInstructorApiController@sub_instructors_add');

        Route::post('sub_instructors_remove', 'InstructorApi\SubInstructorApiController@sub_instructors_remove');

    });

    // sub_instructors management end

    // rooms management start

    Route::group(['middleware' => 'InstructorApiVal'] , function() {

        Route::post('rooms_index', 'InstructorApi\MeetingApiController@rooms_index');

        Route::post('rooms_view', 'InstructorApi\MeetingApiController@rooms_view');

        Route::post('rooms_search', 'InstructorApi\MeetingApiController@rooms_search');

        Route::post('rooms_save', 'InstructorApi\MeetingApiController@rooms_save');

        Route::post('rooms_delete', 'InstructorApi\MeetingApiController@rooms_delete');

        Route::post('room_users_index', 'InstructorApi\MeetingApiController@room_users_index');

        Route::post('room_users_search', 'InstructorApi\MeetingApiController@room_users_search');

        Route::post('room_users_add', 'InstructorApi\MeetingApiController@room_users_add');

        Route::post('room_users_remove', 'InstructorApi\MeetingApiController@room_users_remove');

    });

    // rooms management end

    // rooms management start

    Route::group(['middleware' => 'InstructorApiVal'] , function() {

        Route::post('meetings_index', 'InstructorApi\MeetingApiController@meetings_index');

        Route::post('meetings_view', 'InstructorApi\MeetingApiController@meetings_view');

        Route::post('meetings_search', 'InstructorApi\MeetingApiController@meetings_search');

        Route::post('meetings_save', 'InstructorApi\MeetingApiController@meetings_save');

        Route::post('meetings_delete', 'InstructorApi\MeetingApiController@meetings_delete');

        Route::post('meetings_assign_instructor', 'InstructorApi\MeetingApiController@meetings_assign_instructor');

        Route::post('meetings_vod_video_save', 'InstructorApi\MeetingApiController@meetings_vod_video_save');

        Route::post('meetings_live_start', 'InstructorApi\MeetingApiController@meetings_live_start');

        Route::post('meetings_live_end', 'InstructorApi\MeetingApiController@meetings_live_end');

        Route::post('meetings_join', 'InstructorApi\MeetingApiController@meetings_join');
        
        Route::post('meetings_end', 'InstructorApi\MeetingApiController@meetings_end');

        Route::post('meeting_users_index', 'InstructorApi\MeetingApiController@meeting_users_index');

        Route::post('meeting_users_search', 'InstructorApi\MeetingApiController@meeting_users_search');
    });

    // rooms management end

});

