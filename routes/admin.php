<?php

Route::group(['middleware' => 'web'], function() {

    Route::group(['as' => 'admin.', 'prefix' => 'admin'], function(){

        Route::get('important/constants', 'ApplicationController@list_of_constants');

        Route::get('clear-cache', function() {

            $exitCode = Artisan::call('config:cache');

            return back();

        })->name('clear-cache');

        Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('login');

        Route::post('login', 'Auth\AdminLoginController@login')->name('login.post');

        Route::get('logout', 'Auth\AdminLoginController@logout')->name('logout');

        /***
         *
         * Admin Account releated routes
         *
         */

        Route::get('profile', 'AdminController@profile')->name('profile');

        Route::post('profile/save', 'AdminController@profile_save')->name('profile.save');

        Route::post('change/password', 'AdminController@change_password')->name('change.password');

        Route::get('/', 'AdminController@index')->name('dashboard');
        
        Route::get('users', 'AdminController@users_index')->name('users.index');

        Route::get('users/create', 'AdminController@users_create')->name('users.create');

        Route::get('users/edit', 'AdminController@users_edit')->name('users.edit');

        Route::post('users/save', 'AdminController@users_save')->name('users.save');

        Route::get('users/view', 'AdminController@users_view')->name('users.view');

        Route::get('users/delete', 'AdminController@users_delete')->name('users.delete');

        Route::get('users/status', 'AdminController@users_status')->name('users.status');

        Route::get('users/verify', 'AdminController@users_verify_status')->name('users.verify');

        //instructions

        Route::get('instructors', 'AdminController@instructors_index')->name('instructors.index');

        Route::get('instructors/create', 'AdminController@instructors_create')->name('instructors.create');

        Route::get('instructors/edit', 'AdminController@instructors_edit')->name('instructors.edit');

        Route::post('instructors/save', 'AdminController@instructors_save')->name('instructors.save');

        Route::get('instructors/view', 'AdminController@instructors_view')->name('instructors.view');

        Route::get('instructors/delete', 'AdminController@instructors_delete')->name('instructors.delete');

        Route::get('instructors/status', 'AdminController@instructors_status')->name('instructors.status');

        Route::get('instructors/verify', 'AdminController@instructors_verify_status')->name('instructors.verify');

        //sub instructor routes
        Route::get('sub_instructors/create','AdminController@sub_instructors_create')->name('sub_instructors.create');

        Route::post('sub_instructors/save','AdminController@sub_instructors_save')->name('sub_instructors.save');

        Route::get('sub_instructors/delete','AdminController@sub_instructors_delete')->name('sub_instructors.delete');

        //sub instructor subscription
        Route::get('instructor/subscriptions','AdminController@instructors_subscriptions_index')->name('instructors.subscriptions_index');

        Route::post('instructor/subscription_payments','AdminController@instructors_subscriptions_payments')->name('instructors.subscriptions_payments');

        Route::get('instructor/subscriptions_payments/delete','AdminController@instructors_subscriptions_payments_delete')->name('instructors.subscriptions_payments_delete');

        //subscriptions
        Route::get('subscriptions', 'AdminController@subscriptions_index')->name('subscriptions.index');

        Route::get('subscriptions/create', 'AdminController@subscriptions_create')->name('subscriptions.create');

        Route::get('subscriptions/edit', 'AdminController@subscriptions_edit')->name('subscriptions.edit');

        Route::post('subscriptions/save', 'AdminController@subscriptions_save')->name('subscriptions.save');

        Route::get('subscriptions/view', 'AdminController@subscriptions_view')->name('subscriptions.view');

        Route::get('subscriptions/delete', 'AdminController@subscriptions_delete')->name('subscriptions.delete');

        Route::get('subscriptions/status', 'AdminController@subscriptions_status')->name('subscriptions.status');

        //Subscription Payments
        Route::get('subscription_payments/index','AdminController@subscription_payments_index')->name('subscription_payments.index');

        Route::get('subscription_payments/view','AdminController@subscription_payments_view')->name('subscription_payments.view');

        Route::get('revenue/dashboard','AdminController@revenue_dashboard')->name('revenue.dashboard');

        // rooms crud
        Route::get('rooms', 'AdminController@rooms_index')->name('rooms.index');

        Route::get('rooms/create', 'AdminController@rooms_create')->name('rooms.create');

        Route::get('rooms/edit', 'AdminController@rooms_edit')->name('rooms.edit');

        Route::post('rooms/save', 'AdminController@rooms_save')->name('rooms.save');

        Route::get('rooms/view', 'AdminController@rooms_view')->name('rooms.view');

        Route::get('rooms/delete', 'AdminController@rooms_delete')->name('rooms.delete');

        Route::get('rooms/status', 'AdminController@rooms_status')->name('rooms.status');

        //rooms users create
        Route::get('rooms/user/create','AdminController@rooms_user_create')->name('rooms.user.create');

        Route::post('rooms/users/save','AdminController@rooms_user_save')->name('rooms.user.save');

        Route::get('rooms/users/delete','AdminController@rooms_user_delete')->name('rooms.user.delete');

        //meetings crud
        Route::get('meetings', 'AdminController@meetings_index')->name('meetings.index');

        Route::get('meetings/create', 'AdminController@meetings_create')->name('meetings.create');

        Route::get('meetings/edit', 'AdminController@meetings_edit')->name('meetings.edit');

        Route::post('meetings/save', 'AdminController@meetings_save')->name('meetings.save');

        Route::get('meetings/view', 'AdminController@meetings_view')->name('meetings.view');

        Route::get('meetings/delete', 'AdminController@meetings_delete')->name('meetings.delete');

        Route::get('meetings/status', 'AdminController@meetings_status')->name('meetings.status');


        // settings

        Route::get('/admin-control', 'AdminController@admin_control')->name('control');

        Route::get('/ios-control', 'AdminController@ios_control')->name('ios-control'); 

        Route::get('settings', 'AdminController@settings')->name('settings'); 

        Route::post('settings/save', 'AdminController@settings_save')->name('settings.save'); 

        Route::post('env_settings','AdminController@env_settings_save')->name('env-settings.save');

        // STATIC PAGES

        Route::get('static_pages' , 'AdminController@static_pages_index')->name('static_pages.index');

        Route::get('static_pages/create', 'AdminController@static_pages_create')->name('static_pages.create');

        Route::get('static_pages/edit', 'AdminController@static_pages_edit')->name('static_pages.edit');

        Route::post('static_pages/save', 'AdminController@static_pages_save')->name('static_pages.save');

        Route::get('static_pages/delete', 'AdminController@static_pages_delete')->name('static_pages.delete');

        Route::get('static_pages/view', 'AdminController@static_pages_view')->name('static_pages.view');

        Route::get('static_pages/status', 'AdminController@static_pages_status_change')->name('static_pages.status');
    });

});